Instalacja HEXTL 
---

###Wymagane oprogramowanie:

- Java JDK min 1.8
- Maven
- Git
- Node min 10.19.0
- Baza MySql

###Przygotowanie


1 . Przygotowanie Bazy danych:
 - Utworzenie nowego uzytkownika bazy MySql.
 - Tworzymy schemat bazy danych za pomoca pliku `schema.sql` znajdującego sie w init.
 - Dodajemy role w aplikacje i pierwszego uztkownika za pomoca `init.sql` znajdującego sie w init.
 
2 . Przygotowanie properties:
 - Uzupelnienie database setings, adres do bazy, użytkownika i hasło
 - ssl - jeżeli ma być używane odkomentować, przygotować pliki jks i uzupelnić danymi
 - app settings wg uznania i wymagan biznesowych
 - uzupelnic dane do mail settings w celu komunikacji poprzez email i powiadomień
 
3 . Wejść do głównego katalogu i zbudować projekt za pomoca polecenia `mvn install`

4 . Kolejny krok to z katalogu target przenieś aplikacje pod nazwą hextl-`numer_wersji`.jar do wybranego miejsca.  
 Nastepnie wykonać otworzyć konsole i wpisać:   
  `java -jar  hextl-numer_wersji.jar`

5 . Pobrać react-java z repozytorium i uruchomic za pomoca npm `run build`
- uwaga może być konieczne poprzedzenie komendy npm run build komendą NODE_OPTIONS="--max-old-space-size=ilość-pamięci-do-wykorzystania",
 gdzie w miejsce ilość-pamięci-do-wykorzystania wpisujemy ilość pamięci ram do zbudowania aplikacji
 
 
 **v.01**

