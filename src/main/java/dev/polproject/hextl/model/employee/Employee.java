package dev.polproject.hextl.model.employee;

import dev.polproject.hextl.model.ActiveFilter;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.time.OffsetDateTime;
import java.util.Objects;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "employees")
public class Employee implements ActiveFilter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String name;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String lastName;

    @NotNull
    private Boolean isActive;
    private String information;

    @NotNull
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private List<ProductionReport> employeeProductionReport;




    @Override
    public Boolean getActive() {
        return isActive;
    }

    @Override
    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public OffsetDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(OffsetDateTime createAt) {
        this.createAt = createAt;
    }

    public OffsetDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(OffsetDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public List<ProductionReport> getEmployeeProductionReport() {
        return employeeProductionReport;
    }

    public void setEmployeeProductionReport(List<ProductionReport> employeeProductionReport) {
        this.employeeProductionReport = employeeProductionReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(name, employee.name) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(isActive, employee.isActive) &&
                Objects.equals(information, employee.information) &&
                Objects.equals(createAt, employee.createAt) &&
                Objects.equals(updateAt, employee.updateAt) &&
                Objects.equals(employeeProductionReport, employee.employeeProductionReport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName);
    }
}
