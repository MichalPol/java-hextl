package dev.polproject.hextl.model;

public interface ActiveFilter {
    Boolean getActive();
    void setActive(Boolean active);
}
