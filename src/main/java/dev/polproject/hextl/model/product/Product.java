package dev.polproject.hextl.model.product;

import dev.polproject.hextl.model.ActiveFilter;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "products")
public class Product implements ActiveFilter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private Boolean isSerialized;

    @NotNull
    private Integer itemsPerCycle;

    @NotNull
    @Column(unique = true)
    private String instructionId;

    private String description;

    @NotNull
    private Boolean isActive;

    @NotNull
    private OffsetDateTime createAt;

    @NotNull
    private OffsetDateTime updateAt;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<ProductionReport> productProductionReports;

    @Override
    public Boolean getActive() {
        return isActive;
    }

    @Override
    public void setActive(Boolean active) {
        isActive = active;
    }
}
