package dev.polproject.hextl.model.user;

import dev.polproject.hextl.model.ActiveFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "users")
public class User implements ActiveFilter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String name;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String surname;

    @Column(unique = true)
    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String login;

    @NotNull(message="nie może być puste")
    private String password;

    @NotNull
    private Boolean isActive;
    @NotNull
    private String pathAvatar;

    @NotBlank(message = "nie może być puste")
    @Email(message = "email nieprawidłowy")
    @Column(unique = true)
    private String email;

    @NotNull(message="nie może być puste")
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;

    @NotNull(message="nie może być puste")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Role role;

    @Override
    public Boolean getActive() {
        return isActive;
    }

    @Override
    public void setActive(Boolean active) {
        isActive = active;
    }
}
