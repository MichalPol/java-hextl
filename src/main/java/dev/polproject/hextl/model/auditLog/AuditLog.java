package dev.polproject.hextl.model.auditLog;

import dev.polproject.hextl.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "logs")
public class AuditLog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ControlCode controlCode;
    @NotNull
    private String actionMessage;
    @OneToOne
    private User user;
    @NotNull
    private OffsetDateTime createAt;
}
