package dev.polproject.hextl.model.timetable;

import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "work_plans")
public class WorkPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @NotNull(message="nie może być puste")
    private LocalDate startDay;

    @NotNull(message="nie może być puste")
    private LocalDate endDay;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<WorkShift> workShifts;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Employee> holidaysEmployee;
    private String infoHolidays;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Employee> absenceEmployee;
    private String infoAbsence;

    @NotNull(message="nie może być puste")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private User createBy;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private User updateBy;

    @NotNull(message="nie może być puste")
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;


}
