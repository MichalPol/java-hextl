package dev.polproject.hextl.model.ranking;

import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.line.Line;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.time.Year;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "employee_ranking")
public class EmployeeRanking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private Integer year;
    @NotNull
    private Integer rankingTypeCounter;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Line line;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private RankingType rankingType;
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Employee employee;
    @NotNull
    private Double averagePerHour;
    @NotNull
    private Double percentage;
    @NotNull
    private Double averageSpeed;
    @NotNull
    private Integer totalProduced;
    @NotNull
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;

}
