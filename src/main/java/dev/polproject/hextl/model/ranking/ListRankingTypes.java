package dev.polproject.hextl.model.ranking;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListRankingTypes {
    private RankingType general;
    private RankingType firstWorkplace;
    private RankingType secondWorkplace;
    private RankingType thirdWorkplace;
}
