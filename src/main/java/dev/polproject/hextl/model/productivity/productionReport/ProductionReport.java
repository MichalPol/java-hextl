package dev.polproject.hextl.model.productivity.productionReport;

import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder

@Entity(name = "production_reports")
public class ProductionReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Line line;

    @NotNull
    private OffsetDateTime productionStart;//rozpoczecie produkcji

    @NotNull
    private OffsetDateTime productionEnd;//zakonczenie produkcji


    @NotNull
    private Double productionTimeToHour;//czas produkcji w godzinach

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;//produkt wyprodukowany

    @NotNull
    @Size(min = 3, max = 10)
    private String series;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee firstWorkplace;//blistrzarka

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee secondWorkplace;//kartoniarka

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee thirdWorkplace;//fasowaczka

    @NotNull
    private Integer speedMachinePerCycle; // predkosc maszyny

    @NotNull
    private Integer totalQuantityProduced;//wielkosc/ilosc  produktu wyprodukowanego


    //product.itemPerCycle * speedMachinePerCycle * productionTimeAtMinute = maxPossibleItems
    //produkt ilosc na cykl * spredkosc maszyny * czas produkcji = maksymalna mozliwa ilosc do wyprodukowania
    @NotNull
    private Integer maxPossibleItems;//maksymalna ilosc jaką możba było wyprodukować


    //ilosc wkonana / realny czas produkcji * 60
    @NotNull
    private Double performancePerHour;//wydajność z przeliczeniem na produktów/godzina

    //algorytm do implementacji
    //ilosc wykonana w danym czasie/maksymalna ilosc wykonana  w danym czasie * 100
    @NotNull
    private Double percentagePerformance;

    private String description;

    @NotNull
    private OffsetDateTime createAt;
    @NotNull
    private OffsetDateTime updateAt;

    @NotNull
    @ManyToOne
    private User createdByUser;
    @NotNull
    @ManyToOne
    private User updatedByUser;
}
