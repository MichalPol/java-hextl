package dev.polproject.hextl.model.productivity.productionReport;

import lombok.Getter;

@Getter
public enum StatisticalOptionType {
    PRODUCT("product"),
    LINE("line"),
    FIRST_WORKPLACE("firstWorkplace"),
    SECOND_WORKPLACE("secondWorkplace"),
    THIRD_WORKPLACE("thirdWorkplace"),
    SERIES("series"),
    AVENGER_SPEED("averageSpeed"),
    AVENGER_PER_HOUR("averagePerHour"),
    PERCENTAGE("percentage"),
    TOTAL_PRODUCED("totalProduced");

    private String type;

    StatisticalOptionType(String type) {
        this.type = type;
    }
}
