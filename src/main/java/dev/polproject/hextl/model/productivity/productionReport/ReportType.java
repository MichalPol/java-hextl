package dev.polproject.hextl.model.productivity.productionReport;

import lombok.Getter;

@Getter
public enum ReportType {
    LINE,
    EMPLOYEE,
    PRODUCT;

    ReportType() {
    }
}
