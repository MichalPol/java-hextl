package dev.polproject.hextl.repository.user;

import dev.polproject.hextl.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Override
    @Query("select distinct u from users u join fetch u.role ")
    List<User> findAll();

    @Query("select distinct u from users u join fetch u.role where u.email = ?1")
    Optional<User> findByEmail(@NotBlank(message = "nie może być puste") @Email(message = "email nieprawidłowy") String email);

    @Query("select distinct  u from users  u join fetch u.role where u.login = ?1")
    Optional<User> findByLogin(@NotNull(message = "nie może być puste") @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20") String login);
}
