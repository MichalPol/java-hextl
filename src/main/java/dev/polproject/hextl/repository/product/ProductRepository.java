package dev.polproject.hextl.repository.product;

import dev.polproject.hextl.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository  extends JpaRepository<Product, Long> {
    Optional<Product> findByInstructionId(@NotNull String instructionId);
    Optional<Product> findByName(@NotNull String name);
    List<Product> findAllByIsActive(@NotNull Boolean isActive);
}
