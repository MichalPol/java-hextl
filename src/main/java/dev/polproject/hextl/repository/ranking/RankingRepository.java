package dev.polproject.hextl.repository.ranking;

import dev.polproject.hextl.model.ranking.EmployeeRanking;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.service.line.LineService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.Year;
import java.util.List;
import java.util.Optional;

@Repository
public interface RankingRepository extends JpaRepository<EmployeeRanking, Long> {

    @Query(value = "select * from  employee_ranking where `year` = ?1 and ranking_type_counter = ?2  and ranking_type = ?3", nativeQuery = true)
    List<EmployeeRanking> findAllByYearAndRankingTypeCounterAndRankingType(@NotNull Integer year,
                                                                           @NotNull Integer rankingTypeCounter,
                                                                           @NotNull String rankingType);

    Optional<EmployeeRanking> findFirstByYearAndRankingTypeCounterAndRankingType(@NotNull Integer year,
                                                                                 @NotNull Integer rankingTypeCounter,
                                                                                 @NotNull RankingType rankingType);


}
