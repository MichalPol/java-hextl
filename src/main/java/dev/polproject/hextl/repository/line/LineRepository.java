package dev.polproject.hextl.repository.line;

import dev.polproject.hextl.model.line.Line;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface LineRepository extends JpaRepository<Line, Long>{

    Optional<Line> findByNumberLine(@NotNull Integer numberLine);
    Optional<Line> findByName(@NotNull String name);

}
