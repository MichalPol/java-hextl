package dev.polproject.hextl.repository.app;

import dev.polproject.hextl.service.exeption.NotFound;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

@Repository
public class AppRepository {

    private final String filePath = "src/main/resources/application.properties";

    public Map<String, String> readAllSettingsApp() {

        Map<String, String> map = new HashMap<>();
        try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath))) {
            Iterator<String> it = fileReader.lines().iterator();
            while (it.hasNext()) {
                String lines = it.next();
                String[] split = lines.split("=");
                for (int i = 0; i < split.length; i++) {
                    if (split.length == 2) {
                        map.put(split[0], split[1]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public Map<String, String> readSettingsAppByKey(String key) throws NotFound {

        Map<String, String> map = new HashMap<>();

        try (BufferedReader fileReader = new BufferedReader(new FileReader(filePath))) {
            Iterator<String> it = fileReader.lines().iterator();
            while (it.hasNext()) {
                String lines = it.next();

                String[] split = lines.split("=");
                for (int i = 0; i < split.length; i++) {
                    if (split.length == 2) {
                        if (split[0].equals(key))
                            map.put(split[0], split[1]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (map.size() == 0){
            throw new NotFound("Not found key");
        }
        return map;
    }

    public Map<String, String> writeKeyValue(String key, String value) throws NotFound {
        Map<String, String> map = new HashMap<>();
        Properties properties = new Properties();
        //check the existence of the key
        readSettingsAppByKey(key);
        try {
            //read properties
            properties.load(new FileInputStream(filePath));
            //set new value
            properties.setProperty(key, value);
            //save value
            properties.store(new FileWriter(filePath),null);
            map.put(key,value);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
