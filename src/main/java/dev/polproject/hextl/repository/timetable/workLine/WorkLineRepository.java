package dev.polproject.hextl.repository.timetable.workLine;

import dev.polproject.hextl.model.timetable.WorkLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkLineRepository extends JpaRepository<WorkLine, Integer> {
}
