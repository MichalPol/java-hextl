package dev.polproject.hextl.repository.timetable.workPlan;

import dev.polproject.hextl.model.timetable.WorkPlan;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.time.LocalDate;

@Repository
public interface WorkPlanRepository extends JpaRepository<WorkPlan, Integer> {

    List<WorkPlan> findAllByStartDayBetween(@NotNull LocalDate startDay, @NotNull LocalDate startDay2);


}
