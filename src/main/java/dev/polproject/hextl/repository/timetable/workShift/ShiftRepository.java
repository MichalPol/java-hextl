package dev.polproject.hextl.repository.timetable.workShift;

import dev.polproject.hextl.model.timetable.WorkShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShiftRepository extends JpaRepository<WorkShift, Integer> {
}
