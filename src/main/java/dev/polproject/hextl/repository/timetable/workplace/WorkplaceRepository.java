package dev.polproject.hextl.repository.timetable.workplace;

import dev.polproject.hextl.model.timetable.Workplace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkplaceRepository extends JpaRepository<Workplace, Integer> {
}
