package dev.polproject.hextl.repository.productivity;

import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface ProductionReportRepository extends JpaRepository<ProductionReport, Long> {

    List<ProductionReport> findAllByProductionEndBetween(@NotNull OffsetDateTime productionStart, @NotNull OffsetDateTime productionEnd);

    @Query(value = "SELECT distinct  pr FROM production_reports pr join fetch  pr.line join fetch pr.createdByUser u join fetch u.role WHERE  pr.line = ?1 and pr.productionStart >= ?2 and  pr.productionEnd <= ?3 ")
    List<ProductionReport> findAllByLineAndProductionStartAfterAndProductionEndBefore(@NotNull Line line, @NotNull OffsetDateTime productionStart, @NotNull OffsetDateTime productionEnd);

}
