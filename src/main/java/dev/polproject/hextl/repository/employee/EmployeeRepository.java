package dev.polproject.hextl.repository.employee;

import dev.polproject.hextl.model.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

//    @Query(value = "SELECT DISTINCT u FROM users u JOIN u.role where u.id = ?1")
//    @Override
//    Optional<Employee> findById(Long aLong);
}
