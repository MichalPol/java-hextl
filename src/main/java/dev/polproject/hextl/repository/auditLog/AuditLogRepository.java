package dev.polproject.hextl.repository.auditLog;

import dev.polproject.hextl.model.auditLog.AuditLog;
import dev.polproject.hextl.model.auditLog.ControlCode;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {

    @Override
    @Query(value = "SELECT distinct log FROM logs log join fetch log.user u join fetch u.role")
    List<AuditLog> findAll();

    @Query(value = "SELECT distinct log FROM logs  log join fetch log.user u join fetch u.role where  log.createAt  >= ?1 and  log.createAt <= ?2")
    List<AuditLog> findAllByCreateAtBetween(@NotNull OffsetDateTime start, @NotNull OffsetDateTime end);

    @Query(value = "SELECT distinct al FROM logs al join fetch al.user  u join fetch u.role where al.controlCode = ?1 and al.createAt >=  ?2 and al.createAt <= ?3")
    List<AuditLog> findAllByCreateAtBetweenAndControlCode(@NotNull ControlCode controlCode, OffsetDateTime start, OffsetDateTime end);

    List<AuditLog> findAllByControlCode(@NotNull ControlCode controlCode, Pageable pageable);
}
