package dev.polproject.hextl.controller.restApi.employee;

import dev.polproject.hextl.controller.dto.employee.CreateUpdateEmployeeDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeProductionReportsDto;
import dev.polproject.hextl.service.app.PaginationService;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RequiredArgsConstructor
@RestController
@Slf4j
@RequestMapping("/api/v1/employee")
public class EmployeeApi {
    private final EmployeeService employeeService;
    private final PaginationService paginationService;

    @GetMapping
    public List<EmployeeDto> getAllEmployees(@RequestParam (required = false) Boolean filterIsActive) {
        log.info("all employees have been downloaded from API ");
        return employeeService.getAllEmployeeDto(paginationService.filterIsActiveValidation(filterIsActive));
    }

    @GetMapping("/{idEmployee}")
    public EmployeeDto getEmployeeById(@PathVariable Long idEmployee, @RequestParam (required = false) Boolean filterIsActive) throws NotFound {
        if (null == filterIsActive){
            return employeeService.getEmployeeDtoByIdNoFilterActive(idEmployee);
        }
        log.info("Employee downloaded from APi with id " + idEmployee);
        return employeeService.getEmployeeDtoByIdFilterActive(idEmployee, filterIsActive);
    }

    @PostMapping
    public EmployeeDto createEmployee(@Valid @RequestBody CreateUpdateEmployeeDto createUpdateEmployeeDto) throws Conflict {
        log.info("Launching the REST employee creation site:  " + createUpdateEmployeeDto.getName() + " " + createUpdateEmployeeDto.getLastName());
        return employeeService.createEmployee(createUpdateEmployeeDto);
    }

    @PutMapping("/{idEmployee}")
    public EmployeeDto updateEmployee(@PathVariable Long idEmployee, @Valid  @RequestBody CreateUpdateEmployeeDto createUpdateEmployeeDto ) throws Exception {
        log.info("Running the REST employee editing site with ID: " + idEmployee);
        return employeeService.updateEmployee(idEmployee,createUpdateEmployeeDto);
    }

    @DeleteMapping("/{idEmployee}")
    public EmployeeDto deleteEmployeeById(@PathVariable Long idEmployee) throws Exception {
        log.info("Employee deleted with ID: " + idEmployee);
        return employeeService.removeEmployee(idEmployee);
    }

    @GetMapping("/{idEmployee}/productionReports")
    public EmployeeProductionReportsDto getAllProductionReportsFromEmployee(@PathVariable Long idEmployee) throws NotFound {
        log.info("Employee downloaded Production Reports from APi with id " + idEmployee);
        return employeeService.getAllProductionReportsFromEmployee(idEmployee);
    }
}
