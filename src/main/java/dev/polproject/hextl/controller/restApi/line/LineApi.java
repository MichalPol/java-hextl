package dev.polproject.hextl.controller.restApi.line;

import dev.polproject.hextl.controller.dto.line.CreateLineDto;
import dev.polproject.hextl.controller.dto.line.LineDto;
import dev.polproject.hextl.controller.dto.line.LineProductionReportsDto;
import dev.polproject.hextl.controller.dto.line.UpdateLineDto;
import dev.polproject.hextl.service.app.PaginationService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.line.LineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/line")
@RequiredArgsConstructor
public class LineApi {

    private final LineService lineService;
    private final PaginationService paginationService;

    @GetMapping
    public List<LineDto> getAllLine(@RequestParam (required = false) Boolean filterIsActive){
        log.info("Download all Line");
        return lineService.getAllLine(paginationService.filterIsActiveValidation(filterIsActive));
    }

    @GetMapping("/{idLine}")
    public LineDto getLineById(@PathVariable Long idLine,@RequestParam(required = false) Boolean filterIsActive) throws NotFound {
        log.info("Download line id " + idLine);
        return lineService.getLineDto(idLine,paginationService.filterIsActiveValidation(filterIsActive));
    }

    @PostMapping
    public LineDto creatLine(@RequestBody @Valid CreateLineDto createLineDto) throws Conflict {
        log.info("Create new line " + createLineDto.getName());
        return lineService.createLine(createLineDto);
    }

    @PutMapping("/{idLine}")
    public LineDto updateLine(@PathVariable Long idLine, @RequestBody @Valid UpdateLineDto UpdateLineDto) throws NotFound, Conflict {
        log.info("Update line id " + idLine);
        return lineService.updateLine(idLine, UpdateLineDto);
    }

    @DeleteMapping("/{idLine}")
    public LineDto deleteLine(@PathVariable Long idLine) throws NotFound {
        log.info("Delete line id " + idLine);
        return lineService.deleteLine(idLine);
    }

    @GetMapping("/{idLine}/productionReports")
    public LineProductionReportsDto getAllProductionReportsFromLine(@PathVariable Long idLine) throws NotFound {
        log.info("get all Production Report from Line");
        return lineService.getAllProductionReportsFromLine(idLine);
    }
}
