package dev.polproject.hextl.controller.restApi.auditLog;


import dev.polproject.hextl.controller.dto.auditLog.AuditLogDto;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.exeption.DataInvalid;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/log")
public class AuditLogApi {

    private final AuditLogService auditLogService;

    @GetMapping("/all")
    public List<AuditLogDto> getAllLog()  {
        return auditLogService.getAllLog();
    }

    @GetMapping("/logCode")
    public ControlCode[] getAllCode() {
        return ControlCode.values();
    }

    @GetMapping()
    public List<AuditLogDto> getLogBetweenTime(@RequestParam @ApiParam(defaultValue = "2020-06-01-00:00") String startDate,
                                               @RequestParam @ApiParam(defaultValue = "2020-06-10-23:59") String endDate,
                                               @RequestParam(required = false) ControlCode code) throws DataInvalid {
        if (null == code) {
            code = ControlCode.ALL;
        }
        return auditLogService.getLogBetweenTime(startDate, endDate, code);
    }

    @GetMapping("/page/")
    public List<AuditLogDto> getLogByCode(@RequestParam ControlCode code, @RequestParam int page ){
        return auditLogService.getLogByCode(code,page);
    }
}
