package dev.polproject.hextl.controller.restApi.user;

import dev.polproject.hextl.controller.dto.mail.ResetPassword;
import dev.polproject.hextl.controller.dto.user.ChangePasswordDto;
import dev.polproject.hextl.controller.dto.user.CreateUserDto;
import dev.polproject.hextl.controller.dto.user.UpdateUserDto;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.service.app.PaginationService;
import dev.polproject.hextl.service.exeption.*;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserApi {

    private final UserService userService;
    private final PaginationService paginationService;
    

    @GetMapping
    public List<UserDto> getAllUser(@RequestParam (required = false) Boolean filterIsActive){
        log.info("all users have been downloaded from APi");
        return userService.getAllUser(paginationService.filterIsActiveValidation(filterIsActive));
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable Integer id, @RequestParam (required = false) Boolean filterIsActive) throws NotFound {
        log.info("user downloaded from APi with id:" + id);
        return userService.getUserById(id, paginationService.filterIsActiveValidation(filterIsActive));
    }

    @PostMapping("/{idUser}/role/{idRole}")
    public UserDto addUserToRole(@PathVariable Integer idUser, @PathVariable Integer idRole) throws NotFound{
        log.info("added " + idRole+ " roles to user with id " + idUser);
        return userService.addUserToRole(idUser, idRole);
    }


    @PostMapping
    public UserDto createUser(@Valid @RequestBody CreateUserDto createUserDto) throws NotFound, DataInvalid, Conflict {
        log.info("Create new User with login: " + createUserDto.getLogin() );
        return userService.createUser(createUserDto);
    }

    @PutMapping("/{id}")
    public UserDto updateUser(@PathVariable Integer id, @Valid @RequestBody UpdateUserDto updateUserDto) throws NotFound, DataInvalid {
        log.info("Editing user with login: " + updateUserDto.getLogin() );
        return userService.updateUser(id, updateUserDto);
    }

    @DeleteMapping("/{id}")
    public UserDto deleteUserById(@PathVariable Integer id ) throws NotFound {
        log.info("Removed user with id: " + id);
        return userService.deleteUserById(id);
    }

    @PatchMapping("/resetPassword")
    public HttpStatus sendNewPassword(@RequestBody ResetPassword resetPassword, Authentication authentication) throws NotFound, MessagingException, DataInvalid, Unauthorized {
        userService.resetPasswordByAdmin(resetPassword, authentication );
        return HttpStatus.OK;
    }

    @PatchMapping("/changePassword")
    public UserDto changePassword(@NotNull Authentication authentication,@RequestBody ChangePasswordDto changePasswordDto) throws NotFound, IncorrectPassword {
        return userService.changePasswordUser(authentication, changePasswordDto);
    }

}
