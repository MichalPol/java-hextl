package dev.polproject.hextl.controller.restApi.ranking;

import dev.polproject.hextl.controller.dto.ranking.CreateRankingDto;
import dev.polproject.hextl.controller.dto.ranking.RankingDto;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.ranking.RankingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/ranking/")
@RequiredArgsConstructor
public class RankingApi {

    private final RankingService rankingService;

    @GetMapping("year/{year}")
    public RankingDto getRankingYear(@Valid  @PathVariable Integer year) throws DataInvalid, OutRange, NotFound {
        return rankingService.getRankingYear(year);
    }

    @GetMapping("year/{year}/week/{week}")
    public RankingDto getRankingWeek(@PathVariable Integer year, @PathVariable Integer week) throws DataInvalid, OutRange, NotFound {
        return rankingService.getRankingWeek(year, week);
    }

    @GetMapping("year/{year}/month/{month}")
    public RankingDto getRankingMonth(@PathVariable Integer year, @PathVariable Integer month ) throws DataInvalid, OutRange, NotFound {
        return rankingService.getRankingMonth(year, month);
    }

    @GetMapping("year/{year}/quarter/{numberQuarter}")
    public RankingDto getRankingQuarter(@PathVariable Integer year,@PathVariable Integer numberQuarter) throws DataInvalid, OutRange, NotFound {
        return rankingService.getRankingQuarter(year,numberQuarter);
    }

    @GetMapping("year/{year}/halfYear/{numberHalfYear}")
    public RankingDto getRankingHalfYear(@PathVariable Integer year, @PathVariable Integer numberHalfYear) throws DataInvalid, OutRange, NotFound {
        return rankingService.getRankingHalfYear(year, numberHalfYear);
    }

    @PostMapping("/week")
    public RankingDto createRankingWeek(@RequestBody CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        return rankingService.createRankingWeek(createRankingDto);
    }

    @PostMapping("/month")
    public RankingDto createRankingMonth(@RequestBody CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        return rankingService.createRankingMonth(createRankingDto);
    }

    @PostMapping("/quarter")
    public RankingDto createRankingQuarter(@RequestBody CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        return rankingService.createRankingQuarter(createRankingDto);
    }

    @PostMapping("/halfYear")
    public RankingDto createRankingHalfYear(@RequestBody CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        return rankingService.createRankingHalfYear(createRankingDto);
    }

    @PostMapping("/year")
    public RankingDto createRankingYear(@RequestBody CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        return rankingService.createRankingYear(createRankingDto);
    }

    @PostMapping("/isExistsRanking")
    public HttpStatus existsRanking(RankingType rankingType,
                                    Integer year,
                                    Integer rankingTypeCounter){
        boolean byFirstRankingByType = rankingService.findByFirstRankingByType(rankingType, year, rankingTypeCounter);

        if (byFirstRankingByType){
            return HttpStatus.OK;
        }
        return HttpStatus.NOT_FOUND;
    }

    @GetMapping("/rankingType")
    public RankingType[] getRankingType(){
        return RankingType.values();
    }

    @DeleteMapping("/delete")
    public void deleteRanking(RankingType rankingType, Integer year, Integer count) throws OutRange, InterruptedException {
        rankingService.removeRanking(rankingType,year, count);
    }

}
