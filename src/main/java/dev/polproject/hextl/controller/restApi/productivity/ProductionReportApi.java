package dev.polproject.hextl.controller.restApi.productivity;

import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.controller.dto.productionReport.CreateProductionReportDto;
import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.controller.dto.productionReport.UpdateProductionReportDto;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.service.app.DateAppFormatter;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportService;
import dev.polproject.hextl.service.user.UserService;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/production-report")
@RequiredArgsConstructor
public class ProductionReportApi {

    private final ProductionReportService productionReportService;
    private final UserService userService;

    @GetMapping
    public List<ProductionReportDto> getAllProductionReport() {
        return productionReportService.getAllProductReportDto();
    }

    @GetMapping("{idProductionReport}")
    public ProductionReportDto getProductionReportById(@PathVariable long idProductionReport) throws NotFound {
        return productionReportService.getProductionReportDto(idProductionReport);
    }

    @GetMapping("/{startDate}/{endDate}")
    public List<ProductionReportDto> getProductionReportBetweenDateTime(@PathVariable String startDate,
                                                                        @PathVariable String endDate) throws DataInvalid {
        OffsetDateTime start = DateAppFormatter.parseStringToOffsetDateTimeFormat(startDate);
        OffsetDateTime end = DateAppFormatter.parseStringToOffsetDateTimeFormat(endDate);
        return productionReportService.getProductionReportDtoByBetweenTime(start, end);
    }

    @PostMapping()
    public ProductionReportDto createProductionReport
            (@RequestBody @Valid CreateProductionReportDto createProductionReportDto, Authentication authentication )
            throws NotFound, DataInvalid, OutRange, Conflict {
        String login = authentication.getName();
        User user = userService.getUserModelByLogin(login);
        return productionReportService.createProductionReport(createProductionReportDto, user);
    }

    @PutMapping("{idProductionReport}")
    public ProductionReportDto updateProductionReport(@PathVariable Long idProductionReport,
                                                      @RequestBody @Valid UpdateProductionReportDto update,
                                                      Authentication authentication) throws NotFound, DataInvalid {
        String login = authentication.getName();
        User user = userService.getUserModelByLogin(login);
        return productionReportService.updateProductionReport(idProductionReport, update , user);
    }

    @DeleteMapping("/{idProductionReport}")
    public ProductionReportDto deleteProductionReport(@PathVariable Long idProductionReport) throws NotFound {
        return productionReportService.deleteProductionReport(idProductionReport);
    }

    @GetMapping("/products/{start}/{end}")
    public Set<ProductIdNameSerializedDto> getProductsFromProductionReport(
            @PathVariable @ApiParam(value = "2020-06-01-00:00") String start,
            @PathVariable @ApiParam(value = "2020-06-05-23:59") String end) throws DataInvalid, NotFound {

        OffsetDateTime startDate = DateAppFormatter.parseStringToOffsetDateTimeFormat(start);
        OffsetDateTime endDate = DateAppFormatter.parseStringToOffsetDateTimeFormat(end);
        return productionReportService.getProductFromProductionReport(startDate, endDate);
    }

}
