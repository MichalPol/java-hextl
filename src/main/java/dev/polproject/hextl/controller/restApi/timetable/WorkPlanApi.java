package dev.polproject.hextl.controller.restApi.timetable;

import dev.polproject.hextl.controller.dto.timetable.workPlan.CreateWorkPlanDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.UpdateWorkPlanDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.WorkPlanDto;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.timetable.WorkPlanService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/work-plan")
@RequiredArgsConstructor
public class WorkPlanApi {

    private final WorkPlanService workPlanService;


    @GetMapping
    public List<WorkPlanDto> getAllWeeklyWorkPlan(){
        log.info("get all work plan");
        return workPlanService.getAllWorkPlan();
    }

    @GetMapping("/{id}")
    public WorkPlanDto getWeeklyWorkPlanById( @PathVariable Integer id) throws NotFound {
        log.info("Work plan with id retrieved: " + id);
        return workPlanService.getWorkPlanById(id);
    }

    @GetMapping("/param")
    public WorkPlanDto getFirstWorkPlanBetweenDateByParam(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                                          @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end) throws NotFound, DataInvalid {
        log.info("work plan downloaded from the range: " + start + " - " + end);
        return workPlanService.getFirstWorkPlanBetweenDate(start,end);
    }

    @GetMapping("/start/{start}/end/{end}")
    public WorkPlanDto getFirstWorkPlanBetweenDate(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                                   @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end) throws NotFound, DataInvalid {
        log.info("work plan downloaded from the range: " + start + " - " + end);
        return workPlanService.getFirstWorkPlanBetweenDate(start,end);

    }

    @PostMapping
    public WorkPlanDto createWorkPlan(@RequestBody  @Valid  CreateWorkPlanDto createWorkPlan) throws NotFound, Conflict, DataInvalid {
//        log.info("created work plan from the range: " + createWorkPlan.getStartDay() + " - " + createWorkPlan.getEndDay());
        return workPlanService.createWorkPlan(createWorkPlan);
    }

    @PutMapping("/{idWorkPlan}/user/{idUser}")
    public WorkPlanDto updateWorkPlan( @PathVariable Integer idUser, @PathVariable Integer idWorkPlan,@RequestBody @Valid UpdateWorkPlanDto updateWorkPlanDto) throws NotFound, DataInvalid {
        log.info("Editing work plan with ID: " + idWorkPlan);

        return workPlanService.updateWorkPlan(idUser,idWorkPlan,updateWorkPlanDto);
    }


}
