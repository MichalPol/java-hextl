package dev.polproject.hextl.controller.restApi.mail;

import dev.polproject.hextl.controller.dto.mail.RequestChangePasswordByEmail;
import dev.polproject.hextl.controller.dto.mail.SendMessageContactEmail;
import dev.polproject.hextl.controller.dto.mail.SendMessageEmailToUser;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mail.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/mail")
@RequiredArgsConstructor
public class MailApi {

    private final MailService mailService;
    @PostMapping("/sendMessageContact")
    public HttpStatus sendMessage( @Valid @RequestBody SendMessageContactEmail sendMessageContactEmail) throws MessagingException {
        mailService.sendMessageEmailContact(sendMessageContactEmail);
        return HttpStatus.OK;
    }

    @PostMapping("/sendChangePass")
    public HttpStatus sendChangePassword(@RequestBody RequestChangePasswordByEmail requestChangePasswordByEmail) throws NotFound, MessagingException {
        mailService.sendRequestPasswordChange(requestChangePasswordByEmail.getEmail());
        return HttpStatus.OK;
    }

    @PostMapping("/sendMessageToUser")
    public HttpStatus sendMessageToUser(@RequestBody SendMessageEmailToUser message) throws NotFound, MessagingException {
        mailService.sendMessageEmailToUser(message);
        return HttpStatus.OK;
    }

}
