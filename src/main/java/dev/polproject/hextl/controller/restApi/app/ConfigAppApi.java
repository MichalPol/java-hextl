package dev.polproject.hextl.controller.restApi.app;

import dev.polproject.hextl.service.app.AppConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api/config")
public class ConfigAppApi {

    private final AppConfigService appConfigService;

    @GetMapping("/appName")
    public String getAppNameFromProperties() {
        return appConfigService.getAppName();
    }

    @GetMapping("/addressServer")
    public String getAppAddressServerFromProperties() {
        return appConfigService.getAddressServer();
    }

    @GetMapping("/version")
    public String getAppVersionFromProperties() {
        return appConfigService.getAppVersion();
    }

    @GetMapping("/numberJobShifts")
    public Integer getAppNumberJobShiftsFromProperties() {
        return appConfigService.getNumberJobShifts();
    }

    @GetMapping("/workPlanNumbersLine")
    public Integer getAppWorkPlanNumbersLineFromProperties() {
        return appConfigService.getWorkPlanNumbersLine();
    }

    @GetMapping("/workPlanNumberWorkplaces")
    public Integer getAppWorkPlanNumberWorkplacesFromProperties() {
        return appConfigService.getWorkPlanNumberWorkplaces();
    }


}
