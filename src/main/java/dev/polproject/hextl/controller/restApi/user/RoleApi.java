package dev.polproject.hextl.controller.restApi.user;

import dev.polproject.hextl.controller.dto.user.RoleDto;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.user.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/role")
@RequiredArgsConstructor
public class RoleApi {
    private final RoleService service;

    @GetMapping
    public List<RoleDto> getAllRole(){
        log.info("get all role");
        return service.getAllRole();
    }

    @GetMapping("/{idRole}")
    public RoleDto getRoleById(@PathVariable Integer idRole) throws NotFound {
        log.info("get role by id " + idRole );
        return service.getRoleById(idRole);
    }
}
