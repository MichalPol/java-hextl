package dev.polproject.hextl.controller.restApi.login;

import dev.polproject.hextl.controller.dto.user.SessionUser;
import dev.polproject.hextl.controller.dto.user.UserLoginPasswordDto;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.Unauthorized;
import dev.polproject.hextl.service.user.UserService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class LoginApi {
    private final UserService userService;

    @PostMapping("/login")
    public void login(@RequestBody UserLoginPasswordDto userLoginPasswordDto){
    }

    @PostMapping("/logout")
    public HttpStatus logout(HttpSession httpSession){
        httpSession.setAttribute("JSESSIONID", " ");
        return HttpStatus.OK;
    }

    @GetMapping("/authentication")
    public SessionUser getUserAuthentication(Authentication authentication) throws NotFound, Unauthorized {
        if (authentication == null){
            throw  new Unauthorized("you must log in to access");
        }
        log.info("Downloaded who is logged in: " + authentication.getName());
        return userService.getUserAuthentication(authentication.getName());
    }
}
