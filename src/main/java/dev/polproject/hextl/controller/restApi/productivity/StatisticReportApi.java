package dev.polproject.hextl.controller.restApi.productivity;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.controller.dto.statisticRaports.CreateReportEmployee;
import dev.polproject.hextl.controller.dto.statisticRaports.StatisticReport;
import dev.polproject.hextl.controller.dto.statisticRaports.StatisticReportCircleChart;
import dev.polproject.hextl.model.productivity.productionReport.ReportType;
import dev.polproject.hextl.model.productivity.productionReport.StatisticalOptionType;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.productivity.statisticGenerator.StatisticReportGeneratorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/statistic-report")
@RequiredArgsConstructor
public class StatisticReportApi {
    private final StatisticReportGeneratorService statisticReportGeneratorService;


    @PostMapping()
    public StatisticReport statisticReportGenerator(@Valid @RequestBody CreateNewReport createNewReport) throws NotFound, DataInvalid, OutRange {
        return statisticReportGeneratorService.getReportGenerator(createNewReport);
    }

    @PostMapping("/circleChart")
    public StatisticReportCircleChart statisticReportGeneratorCircleChart(@Valid @RequestBody CreateNewReport createNewReport) throws NotFound, DataInvalid, OutRange {
        return statisticReportGeneratorService.getReportGeneratorOnlyStatistic(createNewReport);
    }

    @PostMapping("/products")
    public Set<ProductIdNameSerializedDto> getProductsOfReportProduction(@Valid @RequestBody CreateNewReport createNewReport) throws OutRange, DataInvalid, NotFound {
        return statisticReportGeneratorService.getProductsOfReportGenerator(createNewReport);
    }

    @PostMapping("/employee")
    public Set<EmployeeIdNameSurnameDto> getEmployeeOfReportProduction(
            @Valid @RequestBody CreateReportEmployee createReportEmployee
            ) throws OutRange, DataInvalid, NotFound {

        return statisticReportGeneratorService.getEmployeeOfReportProduction(createReportEmployee);
    }

    @GetMapping("/typeReport")
    public ReportType[] getAllReportType() {
        return ReportType.values();
    }

    @GetMapping("/options")
    public List<String> getAllTypeStatisticOptions() {
        return Arrays.stream(StatisticalOptionType.values())
                .map(StatisticalOptionType::getType)
                .collect(Collectors.toList());
    }



}
