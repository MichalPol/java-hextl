package dev.polproject.hextl.controller.restApi.product;

import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.controller.dto.product.CreateUpdateProductDto;
import dev.polproject.hextl.controller.dto.product.ProductDto;
import dev.polproject.hextl.service.app.PaginationService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductApi {
    private final ProductService productService;
    private final PaginationService paginationService;

    @GetMapping
    public List<ProductDto> getAllProduct(
            @RequestParam(required = false) Boolean filterIsActive){
        return productService.getAllProductDto(
                paginationService.filterIsActiveValidation(filterIsActive));
    }

    @GetMapping("/{idProduct}")
    public ProductDto getProductDto(@PathVariable Long idProduct,
                                    @RequestParam(required = false) Boolean filterIsActive) throws NotFound {
        return productService
                .getProductDto(idProduct, paginationService.filterIsActiveValidation(filterIsActive));
    }

    @PostMapping
    public ProductDto createProduct(@Valid @RequestBody CreateUpdateProductDto creatProduct) throws Conflict {
        return productService.createProduct(creatProduct);
    }

    @PutMapping("/{idProduct}")
    public ProductDto updateProduct(@PathVariable Long idProduct, @Valid @RequestBody CreateUpdateProductDto updateProduct) throws NotFound, Conflict {
        return productService.updateProduct(idProduct, updateProduct);
    }

    @DeleteMapping("/{idProduct}")
    public ProductDto deleteProduct(@PathVariable Long idProduct) throws NotFound {
        return productService.deleteProduct(idProduct);
    }

    @GetMapping("/{idProduct}/productionReports")
    public List<ProductionReportDto> getAllProductionReportsFromProduct(@PathVariable Long idProduct) throws NotFound {
        return productService.getAllProductionReportsFromProduct(idProduct);
    }
}
