package dev.polproject.hextl.controller.config.exeption;

import dev.polproject.hextl.service.exeption.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ControllerRestConfig {
    @ExceptionHandler(NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFound(NotFound ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(DataInvalid.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleDataInvalid(DataInvalid ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(OutRange.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleOutRange(OutRange ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(Conflict.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleAlreadyExists(Conflict ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(Unauthorized.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public String handleUnauthorized(Unauthorized ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(IncorrectPassword.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleIncorrectPassword(IncorrectPassword msg) {return msg.getMessage();
    }

}
