package dev.polproject.hextl.controller.config.security;

import dev.polproject.hextl.controller.config.authenticationManager.RestAuthenticationFailureHandler;
import dev.polproject.hextl.controller.config.authenticationManager.RestAuthenticationSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)

public class SecurityConfig extends WebSecurityConfigurerAdapter {


    private final UserDetailsService userDetailsService;
    private final RestAuthenticationSuccessHandler authenticationSuccessHandler;
    private final RestAuthenticationFailureHandler authenticationFailureHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors()
                .and()
                .httpBasic()
                .and()
                .authorizeRequests()
                //options permitALL
                .antMatchers(HttpMethod.OPTIONS,"/**").permitAll()


                //auditLog api
                .antMatchers("/api/v1/log/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //app api
                .antMatchers("/api/config/**").permitAll()

                //employee api
                .antMatchers("/api/v1/employee/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //line api
                .antMatchers("/api/v1/line/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //login api

                //mail api
                .antMatchers(HttpMethod.POST,"/api/v1/mail/sendMessageContact").permitAll()
                .antMatchers(HttpMethod.POST,"/api/v1/mail/sendChangePass").permitAll()
                .antMatchers(HttpMethod.POST,"/api/v1/mail/sendMessageToUser").hasAnyRole("DEV","ADMIN","MANAGER","USER")

                //product api
                .antMatchers("/api/v1/product/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //production-report api
                .antMatchers("/api/v1/production-report/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //ranking api
                .antMatchers("/api/v1/ranking/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //role api
                .antMatchers("/api/v1/role/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //statistic api
                .antMatchers("/api/v1/statistic-report/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //user api
                .antMatchers(HttpMethod.GET,"/api/v1/user/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")
                .antMatchers(HttpMethod.POST,"/api/v1/user/**").hasAnyRole("DEV","ADMIN","MANAGER","USER")
                .antMatchers(HttpMethod.PUT,"/api/v1/user/**").hasAnyRole("DEV","ADMIN")
                .antMatchers(HttpMethod.PATCH,"api/v1/user/**").hasAnyRole("DEV","ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/v1/user/**").hasAnyRole("DEV","ADMIN")

                //work-plan api
                .antMatchers("/api/v1/work-plan/**").hasAnyRole("DEV","ADMIN","MANAGER","USER","DEMO")

                //swagger
                .antMatchers("/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()

                .antMatchers("/api/v1/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and()
                .addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        auth.authenticationProvider(daoAuthenticationProvider);
    }

    @Bean
    public JsonObjectAuthenticationFilter authenticationFilter() throws Exception {
        JsonObjectAuthenticationFilter filter = new JsonObjectAuthenticationFilter();
        filter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        filter.setAuthenticationFailureHandler(authenticationFailureHandler);
        filter.setAuthenticationManager(super.authenticationManagerBean());
        return filter;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new
                UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
