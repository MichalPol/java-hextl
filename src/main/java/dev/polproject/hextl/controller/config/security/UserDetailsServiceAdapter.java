package dev.polproject.hextl.controller.config.security;

import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
@RequiredArgsConstructor
public class UserDetailsServiceAdapter implements UserDetailsService {

    private final UserService userService;
    private final AuditLogService auditLogService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = null;
        try {
            user = userService.getUserModelByLogin(login);
            auditLogService.action(ControlCode.USER_LOGIN,"Login user " + user.getLogin() + " ", user.getId());
        } catch (NotFound notFound) {
            try {
                auditLogService.action(ControlCode.ERROR_USER_NOT_LOGIN_LOGIN_OR_PASSWORD_INVALID, " Not found user",1);
            } catch (NotFound found) {
                found.printStackTrace();
            }
             new NotFound("Login or password error");
        }

            return org.springframework.security.core.userdetails.User
                    .withUsername(user.getLogin())
                    .password(user.getPassword())
                    .roles(user.getRole().getNameRole())
                    .build();
    }
}
