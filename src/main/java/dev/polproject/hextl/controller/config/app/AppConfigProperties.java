package dev.polproject.hextl.controller.config.app;


import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotEmpty;
import java.io.File;


@Setter
@Getter
@ConfigurationProperties(prefix="app")
@Configuration
public class AppConfigProperties {

    @NotEmpty
    private String appName;

    @NotEmpty
    private String appVersion;

    @NotEmpty
    private String addressServer;

    @NotEmpty
    private Integer numberJobShifts;

    @NotEmpty
    private Integer workPlanNumbersLine;

    @NotEmpty
    private Integer workPlanNumberWorkplaces;

    @NotEmpty
    private Integer rankingWeekStartDay;

    @NotEmpty
    private String rankingWeekStartHour;

    @NotEmpty
    private Integer rankingWeekEndDay;

    @NotEmpty
    private String rankingWeekEndHour;

    @NotEmpty
    private String rankingMonthStartHour;

    @NotEmpty
    private String rankingMonthEndHour;

    @NotEmpty
    private String adminEmail;

    @NotEmpty
    private Integer pageSize;

}
