package dev.polproject.hextl.controller.dto.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResetPassword {

    private Integer resetPasswordWithUserId;
    private String passwordUserAdmin;
}
