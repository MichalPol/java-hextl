package dev.polproject.hextl.controller.dto.productionReport;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductionReportDto {

    @NotNull
    @Min(value = 0)
    private Long lineId;

    @NotNull
    private String productionStart;//rozpoczecie produkcji

    private String productionEnd;//zakonczenie produkcji

    @NotNull
    @Min(value = 0)
    private Integer productionHours;//czas produkcji w godzinach

    @NotNull
    @Min(value = 0)
    private Integer productionMinutes;//czas produkcji w minutach

    @NotNull
    @Min(value = 0)
    private Long productId;//produkt wyprodukowany

    @NotNull
    @Size(min = 3, max = 10)
    private String series;

    @NotNull
    @Min(value = 0)
    private Long firstWorkplaceIdEmployee;//blistrzarka

    @NotNull
    @Min(value = 0)
    private Long secondWorkplaceIdEmployee;//kartoniarka

    @NotNull
    @Min(value = 0)
    private Long thirdWorkplaceIdEmployee;//fasowaczka

    @NotNull
    @Min(value = 5)
    @Max(value = 50)
    private Integer speedMachinePerMinute; // predkosc maszyny na minute

    @NotNull
    @Min(value = 0)
    private Integer totalQuantityProduced;//wielkosc produktu wyprodukowanego

    private String description;

}
