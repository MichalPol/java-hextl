package dev.polproject.hextl.controller.dto.ranking;

import dev.polproject.hextl.model.ranking.RankingType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.time.Year;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RankingDto {
    private Year year;
    private RankingType rankingName;
    private Integer counterForRankingType;
    private String startRanking;
    private String endRanking;
    private List<EmployeeGeneralRankingDto> generalRanking;
    private List<EmployeeRankingForWorkplaceDto> firstWorkplaceRanking;
    private List<EmployeeRankingForWorkplaceDto> secondWorkplaceRanking;
    private List<EmployeeRankingForWorkplaceDto> thirdWorkplaceRanking;
}
