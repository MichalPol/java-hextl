package dev.polproject.hextl.controller.dto.line;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateLineDto {

    @Size(min = 4, max = 30)
    @NotNull
    private String name;

    @NotNull
    private Integer numberLine;
}
