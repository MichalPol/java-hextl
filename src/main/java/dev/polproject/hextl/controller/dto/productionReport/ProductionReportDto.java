package dev.polproject.hextl.controller.dto.productionReport;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.product.ProductDto;
import dev.polproject.hextl.controller.dto.user.UserIdNameSurnameDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductionReportDto {

    private Long id;

    private Long lineId;

    private OffsetDateTime productionStart;

    private OffsetDateTime productionEnd;

    private Double productionTimeToHour;

    private ProductDto product;

    private String series;

    private EmployeeIdNameSurnameDto firstWorkplace;

    private EmployeeIdNameSurnameDto secondWorkplace;

    private EmployeeIdNameSurnameDto thirdWorkplace;

    private Integer speedMachinePerCycle;

    private Integer totalQuantityProduced;

    private Integer maxPossibleItems;

    private Double performancePerHour;

    private Double percentagePerformance;

    private String description;

    private OffsetDateTime createAt;

    private OffsetDateTime updateAt;

    private UserIdNameSurnameDto createdByUser;

    private UserIdNameSurnameDto updatedByUser;
}
