package dev.polproject.hextl.controller.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private Long id;

    private String name;

    private Boolean isSerialized;

    private Boolean isActive;

    private Integer itemsPerCycle;

    private String instructionId;

    private String description;

    private OffsetDateTime createAt;

    private OffsetDateTime updateAt;
}
