package dev.polproject.hextl.controller.dto.auditLog;

import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.controller.dto.user.UserIdNameSurnameDto;
import dev.polproject.hextl.model.auditLog.ControlCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class AuditLogDto {

    private Long id;
    private ControlCode logControlCode;
    private String eventMessage;
    private UserIdNameSurnameDto createdByUser;
    private OffsetDateTime createAt;

}
