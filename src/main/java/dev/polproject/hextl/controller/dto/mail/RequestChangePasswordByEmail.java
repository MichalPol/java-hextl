package dev.polproject.hextl.controller.dto.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestChangePasswordByEmail {
    @Email
    private String email;
}
