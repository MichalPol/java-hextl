package dev.polproject.hextl.controller.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateUserDto {
    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String name;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String surname;

    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String login;

    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String password;

    @NotBlank(message = "nie może być puste")
    @Email(message = "email nieprawidłowy")
    private String email;

    private String pathAvatar;

    @NotNull(message="nie może być puste")
    private Integer role;
}
