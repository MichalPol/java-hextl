package dev.polproject.hextl.controller.dto.timetable.workPlan;


import dev.polproject.hextl.controller.dto.timetable.workShift.UpdateWorkShiftDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UpdateWorkPlanDto {

    @NotNull(message="nie może być puste")
    private LocalDate startDay;

    @NotNull(message="nie może być puste")
    private LocalDate endDay;

    private List<UpdateWorkShiftDto> workShifts;

    private List<Long> holidaysEmployees;
    private String infoHolidays;

    private List<Long> absenceEmployees;
    private String infoAbsence;

}
