package dev.polproject.hextl.controller.dto.timetable.workLine;

import dev.polproject.hextl.controller.dto.timetable.workPlace.UpdateWorkplaceDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UpdateWorkLineDto {
    @NotNull(message="nie może być puste")
    private Integer id;
    @NotNull(message="nie może być puste")
    private Integer lineNumber;

    private List<UpdateWorkplaceDto> workplaces;
}
