package dev.polproject.hextl.controller.dto.line;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineDto {

    private Long id;
    private String name;
    private Integer numberLine;
    private Boolean isActive;
}
