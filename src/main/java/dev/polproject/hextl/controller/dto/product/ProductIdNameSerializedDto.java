package dev.polproject.hextl.controller.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductIdNameSerializedDto {
    private Long id;
    private String name;
    private Boolean isSerialized;
    private Boolean isActive;

}
