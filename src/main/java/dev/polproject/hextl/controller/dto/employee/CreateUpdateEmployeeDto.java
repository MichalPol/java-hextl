package dev.polproject.hextl.controller.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;


@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateEmployeeDto {

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String name;

    @NotNull(message="nie może być puste")
    @Size(min = 2, max = 20, message = "musi być dłuższe niż 2 i krótsze niż 20")
    private String lastName;

    private String information;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateUpdateEmployeeDto that = (CreateUpdateEmployeeDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName);
    }
}
