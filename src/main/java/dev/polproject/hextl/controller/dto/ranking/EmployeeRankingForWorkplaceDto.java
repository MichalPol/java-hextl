package dev.polproject.hextl.controller.dto.ranking;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.line.LineIdNameNumberDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class EmployeeRankingForWorkplaceDto {

    private LineIdNameNumberDto line;
    private EmployeeIdNameSurnameDto employee;
    private Double averagePerHour;
    private Double percentage;
    private Double averageSpeed;
    private Integer totalProduced;
}
