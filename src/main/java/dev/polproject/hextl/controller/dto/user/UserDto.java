package dev.polproject.hextl.controller.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserDto {
    private Integer id;
    private String name;
    private String surname;
    private String login;
    private String pathAvatar;
    private boolean isActive;
    private String email;
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;
    private Integer idRole;
}
