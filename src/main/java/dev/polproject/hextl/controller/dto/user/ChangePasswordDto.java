package dev.polproject.hextl.controller.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ChangePasswordDto {
    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String oldPassword;
    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String newPassword;
    @NotNull(message="nie może być puste")
    @Size(min = 5, max = 20, message = "musi być dłuższe niż 5 i krótsze niż 20")
    private String checkPassword;
}
