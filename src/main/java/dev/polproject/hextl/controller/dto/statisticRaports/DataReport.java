package dev.polproject.hextl.controller.dto.statisticRaports;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.line.LineIdNameNumberDto;
import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataReport {
    private Long idProductionReport;
    private OffsetDateTime startProduction;
    private OffsetDateTime endProduction;
    private LineIdNameNumberDto line;
    private Double productionTimeToHour;
    private ProductIdNameSerializedDto product;
    private String series;
    private EmployeeIdNameSurnameDto firstWorkplace;
    private EmployeeIdNameSurnameDto secondWorkplace;
    private EmployeeIdNameSurnameDto thirdWorkplace;
    private Integer speedMachinePerCycle;
    private Integer totalQuantityProduced;
    private Integer maxPossibleItems;
    private Double performancePerHour;
    private Double percentagePerformance;

}
