package dev.polproject.hextl.controller.dto.timetable.workShift;


import dev.polproject.hextl.controller.dto.timetable.workLine.UpdateWorkLineDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UpdateWorkShiftDto {

    @NotNull(message="nie może być puste")
    private Integer id;

    @NotNull(message="nie może być puste")
    private Integer shiftNumber;

    private List<UpdateWorkLineDto> lines;

    private List<Long> shiftsLeader;

    private List<Long> unskilledWorker;

    private List<Long> supervision;

    private List<Long> other;


    private String comments;
}
