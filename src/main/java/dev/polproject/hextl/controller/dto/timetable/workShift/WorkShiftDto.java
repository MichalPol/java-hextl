package dev.polproject.hextl.controller.dto.timetable.workShift;


import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.timetable.workLine.WorkLineDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class WorkShiftDto {
    private Integer id;

    private Integer shiftNumber;

    private List<WorkLineDto> lines;

    private List<EmployeeIdNameSurnameDto> shiftsLeader;

    private List<EmployeeIdNameSurnameDto> unskilledWorker;

    private List<EmployeeIdNameSurnameDto> supervision;

    private List<EmployeeIdNameSurnameDto> other;


    private String comments;
}
