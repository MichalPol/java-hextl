package dev.polproject.hextl.controller.dto.timetable.workLine;

import dev.polproject.hextl.controller.dto.timetable.workPlace.WorkplaceDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class WorkLineDto {
    private Integer id;
    private Integer lineNumber;
    private List<WorkplaceDto> workplaces;
}
