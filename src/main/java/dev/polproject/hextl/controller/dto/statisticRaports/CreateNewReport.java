package dev.polproject.hextl.controller.dto.statisticRaports;

import dev.polproject.hextl.model.productivity.productionReport.ReportType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateNewReport {
    @NotNull
    private ReportType type;
    @ApiModelProperty(value = "Table id of one of the types Employee, Product, Line")
    @NotNull
    private List<Long> idItems;
    @NotNull
    @ApiModelProperty(value = "format date yyyy-MM-dd-HH-mm", example = "2020-05-01-12:00")
    private String start;
    @NotNull
    @ApiModelProperty(value = "format date yyyy-MM-dd-HH-mm", example = "2020-06-30-12:00")
    private String end;
    @ApiModelProperty(value =  "Map <Key, Value> ")
    private Map<String, Object> options;
}
