package dev.polproject.hextl.controller.dto.statisticRaports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticReport {

    private String name;
    private OffsetDateTime starRange;
    private OffsetDateTime endRange;
    private List<DataReport> dataReport;
    private Map<String, Object> options;
}
