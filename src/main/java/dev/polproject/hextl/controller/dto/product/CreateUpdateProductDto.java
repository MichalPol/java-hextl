package dev.polproject.hextl.controller.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateProductDto {


    @Size(max = 70)
    @NotNull
    private String name;

    @NotNull
    private Boolean isSerialized;

    @Min(value = 2)
    @Max(value = 16)
    @NotNull
    private Integer itemsPerCycle;

    @NotNull
    private String instructionId;

    @Size(max = 200)
    private String description;

}
