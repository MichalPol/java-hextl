package dev.polproject.hextl.controller.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private Long id;
    private String name;
    private String lastName;
    private Boolean isActive;
    private String information;
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;
}
