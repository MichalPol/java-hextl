package dev.polproject.hextl.controller.dto.statisticRaports;

import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParseReport {

    private Map<String,Object> options;
    private List<ProductionReport> productionReports;
}
