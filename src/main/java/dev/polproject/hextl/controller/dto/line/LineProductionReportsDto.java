package dev.polproject.hextl.controller.dto.line;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineProductionReportsDto {

    private Long id;
    private String name;
    private Integer numberLine;
    private Boolean isActive;
    private List<Long> idProductionLineReports;
}
