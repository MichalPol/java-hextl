package dev.polproject.hextl.controller.dto.timetable.workPlan;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateWorkPlanDto {

    @NotNull
    private LocalDate startDay;

    @NotNull
    private LocalDate endDay;

    @NotNull
    private Integer createByIdUser;

}
