package dev.polproject.hextl.controller.dto.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendMessageContactEmail {
    @Email
    private String addressEmail;
    @NotNull
    private String titleMessage;
    @NotNull
    private String nameSender;
    @NotNull
    private String content;
}
