package dev.polproject.hextl.controller.dto.ranking;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateRankingDto {

    @NotNull
    private Integer year;
    @NotNull
    private Integer dateRangeNumber;

}
