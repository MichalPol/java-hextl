package dev.polproject.hextl.controller.dto.productionReport;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProductionReportDto {

        @NotNull
        private Long lineId;

        @ApiModelProperty(value = "format date yyyy-MM-dd-HH-mm", example = "2020-05-01-12:00")
        @NotNull
        private String productionStart;

        @ApiModelProperty(value = "format date yyyy-MM-dd-HH-mm", example = "2020-05-01-13:00")
        @NotNull
        private String productionEnd;

        @NotNull
        private Double productionTimeToHour;

        @NotNull
        private Long productId;

        @NotBlank
        private String series;

        @NotNull
        private Long firstWorkplaceIdEmployee;

        @NotNull
        private Long secondWorkplaceIdEmployee;

        @NotNull
        private Long thirdWorkplaceIdEmployee;

        @NotNull
        private Integer speedMachinePerCycle;

        @NotNull
        private Integer totalQuantityProduced;

        @NotNull
        private Double performancePerHour;

        @NotNull
        private Double percentagePerformance;

        private String description;

}
