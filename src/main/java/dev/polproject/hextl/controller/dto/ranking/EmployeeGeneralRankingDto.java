package dev.polproject.hextl.controller.dto.ranking;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class EmployeeGeneralRankingDto {
    private EmployeeIdNameSurnameDto employee;
    private Double averagePerHour;
    private Double percentage;
    private Double averageSpeed;
    private Integer totalProduced;
}
