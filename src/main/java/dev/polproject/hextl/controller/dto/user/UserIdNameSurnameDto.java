package dev.polproject.hextl.controller.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserIdNameSurnameDto {
    private Integer id;
    private String name;
    private String surname;

}
