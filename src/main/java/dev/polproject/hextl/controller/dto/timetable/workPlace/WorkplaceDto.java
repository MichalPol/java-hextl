package dev.polproject.hextl.controller.dto.timetable.workPlace;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class WorkplaceDto {
    private Integer id;
    private String nameWorkplace;
    private List<EmployeeIdNameSurnameDto> employeeListWorkplaces;
}
