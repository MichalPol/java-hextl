package dev.polproject.hextl.controller.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeIdNameSurnameDto {
    private Long id;
    private String name;
    private String lastName;

}
