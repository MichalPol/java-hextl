package dev.polproject.hextl.controller.dto.employee;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeProductionReportsDto {
    private Long id;
    private String name;
    private String lastName;
    private Boolean isActive;
    private String information;
    private OffsetDateTime createAt;
    private OffsetDateTime updateAt;
    private List<Long> idProductionReports;
}
