package dev.polproject.hextl.controller.dto.timetable.workPlan;


import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.user.UserIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.timetable.workShift.WorkShiftDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class WorkPlanDto {
    private Integer id;

    private LocalDate startDay;

    private LocalDate endDay;

    private List<WorkShiftDto> workShifts;

    private List<EmployeeIdNameSurnameDto> holidaysEmployees;
    private String infoHolidays;

    private List<EmployeeIdNameSurnameDto> absenceEmployees;
    private String infoAbsence;

    private UserIdNameSurnameDto createByIdUser;
    private UserIdNameSurnameDto updateByIdUser;


    private OffsetDateTime updateAt;
}
