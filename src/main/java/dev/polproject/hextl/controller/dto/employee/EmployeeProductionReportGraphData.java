package dev.polproject.hextl.controller.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeProductionReportGraphData {
    private Long id;
    private String name;
    private String lastName;
    private Map<LocalDate, Integer> reports;

}
