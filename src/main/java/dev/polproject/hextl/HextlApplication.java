package dev.polproject.hextl;

import dev.polproject.hextl.service.app.AppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableScheduling
@EnableSwagger2
@SpringBootApplication
public class HextlApplication extends SpringBootServletInitializer {

    @Autowired
    private AppConfigService appConfigService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HextlApplication.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(HextlApplication.class, args);
    }

    @Bean
    public Docket get() {
        //swagger config
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(createApiInfo());
    }

    private ApiInfo createApiInfo() {
        //swagger config
        return new ApiInfo(appConfigService.getAppName(),
                appConfigService.getAppName(),
                appConfigService.getAppVersion(),
                appConfigService.getAddressServer(),
                new Contact("Team", appConfigService.getAddressServer(), appConfigService.getAppAdminEmail()),
                "My own licence",
                appConfigService.getAddressServer(),
                Collections.emptyList());
    }
}
