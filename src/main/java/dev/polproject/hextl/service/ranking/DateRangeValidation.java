package dev.polproject.hextl.service.ranking;

import dev.polproject.hextl.service.exeption.OutRange;
import org.springframework.stereotype.Component;

@Component
public class DateRangeValidation {

    public Integer weekValidator(Integer week) throws OutRange {
        if (week < 1 || week > 53){
            throw new OutRange("Number week is out range");
        }
        return week;
    }

    public Integer monthValidator(Integer month) throws OutRange {
        if (month < 1 || month > 12){
            throw new OutRange("Number month is out range");
        }
        return month;
    }

    public Integer quarterValidator(Integer numberQuarter) throws OutRange {
        if (numberQuarter < 1 || numberQuarter > 4){
            throw new OutRange("Number quarter is out range");
        }
        return numberQuarter;
    }

    public Integer halfYearValidator(Integer halfYearNumber) throws OutRange {
        if (halfYearNumber < 1 || halfYearNumber > 2){
            throw new OutRange("Half Year is out range");
        }
        return halfYearNumber;
    }

    public Integer yearValidator(Integer year) throws OutRange {
        if (year < 2019 && year != 1 ){
            throw new OutRange("Out Range year");
        }
        return year;
    }
}
