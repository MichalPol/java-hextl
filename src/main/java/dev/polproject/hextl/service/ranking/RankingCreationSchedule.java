package dev.polproject.hextl.service.ranking;

import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.service.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Calendar;

@Slf4j
@Configuration
@EnableScheduling
public class RankingCreationSchedule {
    @Autowired
    private RankingService rankingService;
    @Autowired
    private MailService mailService;

    private final String generateRankingWeekCron = "0 0 5 ? * * ";
    private final String generateRankingMonthCron = "0 0 12 ? * Sun ";
    private final String generateRankingQuarterCron = "0 5 12 ? * Sun ";
    private final String generateRankingHalfYearCron = "0 10 12 ? * Sun ";
    private final String generateRankingYearCron = "0 15 12 ? * Sun ";


    private final int weekNumber = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
    private final int monthNumber = LocalDate.now().getMonthValue();
    private final int yearNumber = 1;
    private final int currentYear = Year.now().getValue();

    @Scheduled(cron = generateRankingWeekCron)
    @Transactional
    public void creatingWeeklyRankingAccordingSchedule() throws InterruptedException {
        deleteRankingSchedule(RankingType.WEEK, weekNumber);
        createRankingSchedule(RankingType.WEEK, weekNumber);
    }

    @Scheduled(cron = generateRankingMonthCron)
    @Transactional
    public void creatingMonthRankingAccordingSchedule() throws InterruptedException {
        deleteRankingSchedule(RankingType.MONTH, monthNumber);
        createRankingSchedule(RankingType.MONTH, monthNumber);
    }

    @Scheduled(cron = generateRankingQuarterCron)
    @Transactional
    public void creatingQuarterRankingAccordingSchedule() throws InterruptedException {
        int quarterNumber = 0;
        if (monthNumber < 4) {
            quarterNumber = 1;
        } else if (monthNumber  < 7) {
            quarterNumber = 2;
        } else if (monthNumber < 10) {
            quarterNumber = 3;
        } else if (monthNumber < 13) {
            quarterNumber = 4;
        }

        deleteRankingSchedule(RankingType.QUARTER, quarterNumber);
        createRankingSchedule(RankingType.QUARTER, quarterNumber);
    }

    @Scheduled(cron = generateRankingHalfYearCron)
    @Transactional
    public void creatingHalfYearRankingAccordingSchedule() throws InterruptedException {
        int halfYearNumber;
        if (monthNumber <= 6) {
            halfYearNumber = 1;
        } else {
            halfYearNumber = 2;
        }

        deleteRankingSchedule(RankingType.HALF_YEAR, halfYearNumber);
        createRankingSchedule(RankingType.HALF_YEAR, halfYearNumber);
    }

    @Scheduled(cron = generateRankingYearCron)
    @Transactional
    public void creatingYearRankingAccordingSchedule() throws InterruptedException {
        deleteRankingSchedule(RankingType.YEAR, yearNumber);
        createRankingSchedule(RankingType.YEAR, yearNumber);
    }

    private void deleteRankingSchedule(RankingType rankingType, Integer rankingTypeCounter) throws InterruptedException {
        Thread threadDelete = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    rankingService.removeAllRankingType(rankingType, currentYear, rankingTypeCounter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log.info("Removed" + rankingType.name() + "Ranking type on schedule");
            }
        };
        threadDelete.start();
        threadDelete.join();
    }

    private void createRankingSchedule(RankingType rankingType, Integer rankingTypeCounter) {

        try {
            rankingService.createRanking(rankingType, currentYear, rankingTypeCounter);
            mailService.sendMessageEmailGenerateAutomaticEmployeeRanking("Ranking " + rankingType.name() + " - Complete " + OffsetDateTime.now());
            log.info("creating " + rankingType.name() + " a ranking on schedule");
        } catch (Exception e) {
            log.error("There was an error creating the scheduled ranking of the " + rankingType.name());
            try {
                mailService.sendMessageEmailGenerateAutomaticEmployeeRanking("Ranking " + rankingType.name() + " - BAD GENERATE There was an error creating the scheduled ranking of the " + rankingType.name()
                        + OffsetDateTime.now() + " " + e.toString());
            } catch (MessagingException messagingException) {
                messagingException.printStackTrace();
            }
        }
    }
}
