package dev.polproject.hextl.service.ranking.factory;

import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.model.ranking.ListRankingTypes;
import dev.polproject.hextl.service.exeption.DataInvalid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RankingTypeFactory {

    public ListRankingTypes setRankingType(RankingType rankingType) throws DataInvalid {
        ListRankingTypes rankingTypeList = new ListRankingTypes();

        switch (rankingType) {
            case WEEK: {
                rankingTypeList.setGeneral(RankingType.GENERAL_WEEK);
                rankingTypeList.setFirstWorkplace(RankingType.WORKPLACE_FIRST_WEEK);
                rankingTypeList.setSecondWorkplace(RankingType.WORKPLACE_SECOND_WEEK);
                rankingTypeList.setThirdWorkplace(RankingType.WORKPLACE_THIRD_WEEK);
                return rankingTypeList;
            }
            case MONTH: {
                rankingTypeList.setGeneral(RankingType.GENERAL_MONTH);
                rankingTypeList.setFirstWorkplace(RankingType.WORKPLACE_FIRST_MONTH);
                rankingTypeList.setSecondWorkplace(RankingType.WORKPLACE_SECOND_MONTH);
                rankingTypeList.setThirdWorkplace(RankingType.WORKPLACE_THIRD_MONTH);
                return rankingTypeList;

            }
            case QUARTER: {
                rankingTypeList.setGeneral(RankingType.GENERAL_QUARTER);
                rankingTypeList.setFirstWorkplace(RankingType.WORKPLACE_FIRST_QUARTER);
                rankingTypeList.setSecondWorkplace(RankingType.WORKPLACE_SECOND_QUARTER);
                rankingTypeList.setThirdWorkplace(RankingType.WORKPLACE_THIRD_QUARTER);
                return rankingTypeList;
            }
            case HALF_YEAR: {
                rankingTypeList.setGeneral(RankingType.GENERAL_HALF_YEAR);
                rankingTypeList.setFirstWorkplace(RankingType.WORKPLACE_FIRST_HALF_YEAR);
                rankingTypeList.setSecondWorkplace(RankingType.WORKPLACE_SECOND_HALF_YEAR);
                rankingTypeList.setThirdWorkplace(RankingType.WORKPLACE_THIRD_HALF_YEAR);
                return rankingTypeList;
            }
            case YEAR: {
                rankingTypeList.setGeneral(RankingType.GENERAL_YEAR);
                rankingTypeList.setFirstWorkplace(RankingType.WORKPLACE_FIRST_YEAR);
                rankingTypeList.setSecondWorkplace(RankingType.WORKPLACE_SECOND_YEAR);
                rankingTypeList.setThirdWorkplace(RankingType.WORKPLACE_THIRD_YEAR);
                return rankingTypeList;
            }
            default:
                throw new DataInvalid("unknown type");
        }
    }

}
