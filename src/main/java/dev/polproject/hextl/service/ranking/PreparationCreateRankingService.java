package dev.polproject.hextl.service.ranking;

import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.controller.dto.statisticRaports.StatisticReportCircleChart;
import dev.polproject.hextl.model.auditLog.AuditLog;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.productivity.productionReport.ReportType;
import dev.polproject.hextl.model.ranking.EmployeeRanking;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.productivity.statisticGenerator.StatisticReportGeneratorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PreparationCreateRankingService {

    private final EmployeeService employeeService;
    private final StatisticReportGeneratorService statisticService;
    private final LineService lineService;

    public List<EmployeeRanking> generalRanking(String start, String end, Integer year, Integer week, RankingType typ) throws OutRange, DataInvalid, NotFound {
        List<EmployeeRanking> result = new ArrayList<>();
        List<Employee> employees = employeeService.getAllEmployeeModel()
                .stream()
                .filter(employee -> employee.getEmployeeProductionReport().size() > 0)
                .collect(Collectors.toList());

        Map<String, Object> optionsForGeneral = new HashMap<>();
        optionsForGeneral.put("averagePerHour", true);
        optionsForGeneral.put("percentage", true);
        optionsForGeneral.put("averageSpeed", true);
        optionsForGeneral.put("totalProduced", true);

        for (Employee employee : employees) {
            CreateNewReport general = CreateNewReport.builder()
                    .idItems(Arrays.asList(employee.getId()))
                    .type(ReportType.EMPLOYEE)
                    .start(start)
                    .end(end)
                    .options(optionsForGeneral)
                    .build();
            StatisticReportCircleChart report = statisticService.getReportGeneratorOnlyStatistic(general);
            if ((Integer) report.getOptions().get("totalProduced") != 0) {
                EmployeeRanking employeeGeneralRanking = EmployeeRanking.builder()
                        .id(null)
                        .year(year)
                        .rankingTypeCounter(week)
                        .line(null)
                        .rankingType(typ)
                        .employee(employee)
                        .averagePerHour((double) report.getOptions().get("averagePerHour"))
                        .percentage((double) report.getOptions().get("percentage"))
                        .averageSpeed((double) report.getOptions().get("averageSpeed"))
                        .totalProduced((Integer) report.getOptions().get("totalProduced"))
                        .createAt(OffsetDateTime.now())
                        .build();
                result.add(employeeGeneralRanking);
            }
        }
        return result;
    }

    public List<EmployeeRanking> workplaceRanking(String start,
                                                  String end,
                                                  Integer year,
                                                  Integer rankingTypeCounter,
                                                  RankingType type,
                                                  String optionsWorkplaceKey)
            throws OutRange, DataInvalid, NotFound {

        List<EmployeeRanking> result = new ArrayList<>();
        List<Employee> employees = employeeService.getAllEmployeeModel()
                .stream()
                .filter(employee -> employee.getEmployeeProductionReport().size() > 0)
                .collect(Collectors.toList());

        Map<String, Object> optionsWorkplace = new HashMap<>();
        optionsWorkplace.put("averagePerHour", true);
        optionsWorkplace.put("percentage", true);
        optionsWorkplace.put("averageSpeed", true);
        optionsWorkplace.put("totalProduced", true);
        List<Line> allLine = lineService.getAllLineModel();

        for (Employee employee : employees) {

            for (Line line : allLine) {
                optionsWorkplace.put("line", line.getId().intValue());
                optionsWorkplace.put(optionsWorkplaceKey, employee.getId().intValue());

                CreateNewReport workplace = CreateNewReport.builder()
                        .idItems(Arrays.asList(employee.getId()))
                        .type(ReportType.EMPLOYEE)
                        .start(start)
                        .end(end)
                        .options(optionsWorkplace)
                        .build();
                StatisticReportCircleChart reportWorkplace = statisticService.getReportGeneratorOnlyStatistic(workplace);
                if ((Integer) reportWorkplace.getOptions().get("totalProduced") != 0) {

                    EmployeeRanking employeeWorkplaceRanking = EmployeeRanking.builder()
                            .id(null)
                            .year(year)
                            .rankingTypeCounter(rankingTypeCounter)
                            .line(line)
                            .rankingType(type)
                            .employee(employee)
                            .averagePerHour((double) reportWorkplace.getOptions().get("averagePerHour"))
                            .percentage((double) reportWorkplace.getOptions().get("percentage"))
                            .averageSpeed((double) reportWorkplace.getOptions().get("averageSpeed"))
                            .totalProduced((Integer) reportWorkplace.getOptions().get("totalProduced"))
                            .createAt(OffsetDateTime.now())
                            .build();
                    result.add(employeeWorkplaceRanking);
                }
            }
        }
        return result;
    }
}
