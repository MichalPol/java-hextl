package dev.polproject.hextl.service.ranking.factory;

import dev.polproject.hextl.model.ranking.RankingDateRange;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.service.app.AppConfigService;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.WeekFields;
import java.util.Locale;

@Component
@RequiredArgsConstructor
public class DateRangeRankingFactory {

    private final AppConfigService appConfigService;

    public RankingDateRange setDateRangeRanking(RankingType rankingType, Integer rankingTypeCounter, Integer year) throws NotFound, OutRange, DataInvalid {

        RankingDateRange dateRange = new RankingDateRange();
        String startRanking = null;
        String endRanking = null;

        switch (rankingType) {
            case WEEK: {
                Integer startDay = appConfigService.getRankingWeekStartDay();
                Integer endDay = appConfigService.getRankingWeekEndDay();
                String weekStartHour = appConfigService.getRankingWeekStartHour();
                String weekEndHour = appConfigService.getRankingWeekEndHour();

                WeekFields weekFields = WeekFields.of(Locale.US);
                LocalDate startDate = LocalDate.now()
                        .withYear(year)
                        .with(weekFields.weekOfYear(), rankingTypeCounter)
                        .with(weekFields.dayOfWeek(), startDay);

                LocalDate endDate = LocalDate.now()
                        .withYear(year)
                        .with(weekFields.weekOfYear(), rankingTypeCounter)
                        .with(weekFields.dayOfWeek(), endDay);


                startRanking = startDate + "-" + weekStartHour;
                endRanking = endDate + "-" + weekEndHour;
                dateRange.setStartRanking(startRanking);
                dateRange.setEndRanking(endRanking);
                return dateRange;

            }
            case MONTH: {
                LocalDate startOfMonth = LocalDate.of(year, rankingTypeCounter, 1);
                LocalDate endOfMonth = YearMonth.of(year, rankingTypeCounter).atEndOfMonth();

                startRanking = startOfMonth + "-" + appConfigService.getRankingMonthStartHour();
                endRanking = endOfMonth + "-" + appConfigService.getRankingMonthEndHour();
                dateRange.setStartRanking(startRanking);
                dateRange.setEndRanking(endRanking);
                return dateRange;

            }
            case QUARTER:{

                switch (rankingTypeCounter) {
                    case 1: {
                        startRanking = year + "-01-01-00:00";
                        endRanking = year + "-03-31-23:59";
                        break;
                    }
                    case 2: {
                        startRanking = year + "-03-01-00:00";
                        endRanking = year + "-06-30-23:59";
                        break;
                    }
                    case 3: {
                        startRanking = year + "-07-01-00:00";
                        endRanking = year + "-09-30-23:59";
                        break;
                    }
                    case 4: {
                        startRanking = year + "-10-01-00:00";
                        endRanking = year + "-12-31-23:59";
                        break;
                    }
                    default:{
                        throw new OutRange("choose the correct quarter");

                    }
                }
                dateRange.setStartRanking(startRanking);
                dateRange.setEndRanking(endRanking);
                return dateRange;
            }
            case HALF_YEAR:{
                if (rankingTypeCounter == 1) {
                    startRanking = year + "-01-01-00:00";
                    endRanking = year + "-06-30-23:59";
                } else if (rankingTypeCounter == 2) {
                    startRanking = year + "-07-01-00:00";
                    endRanking = year + "-12-31-23:59";
                } else {
                    throw new OutRange("choose the correct half-year");
                }
                dateRange.setStartRanking(startRanking);
                dateRange.setEndRanking(endRanking);
                return dateRange;
            }

            case YEAR:{
                startRanking = year + "-01-01-00:00";
                endRanking = year + "-12-31-23:59";

                System.out.println(startRanking);
                System.out.println(endRanking);
                dateRange.setStartRanking(startRanking);
                dateRange.setEndRanking(endRanking);
                return dateRange;
            }
            default: {
                throw new DataInvalid("unknown type");
            }
        }

    }
}
