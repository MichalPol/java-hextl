package dev.polproject.hextl.service.ranking;

import dev.polproject.hextl.controller.dto.ranking.CreateRankingDto;
import dev.polproject.hextl.controller.dto.ranking.EmployeeGeneralRankingDto;
import dev.polproject.hextl.controller.dto.ranking.EmployeeRankingForWorkplaceDto;
import dev.polproject.hextl.controller.dto.ranking.RankingDto;
import dev.polproject.hextl.model.ranking.EmployeeRanking;
import dev.polproject.hextl.model.ranking.ListRankingTypes;
import dev.polproject.hextl.model.ranking.RankingDateRange;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.repository.ranking.RankingRepository;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.mapper.ranking.RankingMapper;
import dev.polproject.hextl.service.ranking.factory.DateRangeRankingFactory;
import dev.polproject.hextl.service.ranking.factory.RankingTypeFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RankingService {

    @Autowired
    private RankingRepository rankingRepository;
    @Autowired
    private PreparationCreateRankingService preparationRankingService;
    @Autowired
    private RankingMapper rankingMapper;
    @Autowired
    private DateRangeValidation dateRangeValidation;
    @Autowired
    private DateRangeRankingFactory dateRangeRankingFactory;
    @Autowired
    private RankingTypeFactory rankingTypeFactory;

    //create week
    @Transactional
    public RankingDto createRankingWeek(CreateRankingDto createRankingDto) throws DataInvalid, NotFound, OutRange, InterruptedException {
        Integer year = dateRangeValidation.yearValidator(createRankingDto.getYear());
        Integer week = dateRangeValidation.weekValidator(createRankingDto.getDateRangeNumber());

        Thread thread = new Thread() {
            @SneakyThrows
            @Override
            public void run() {
                if (findByFirstRankingByType(RankingType.GENERAL_WEEK, year, week)) {
                    removeAllRankingType(RankingType.WEEK, year, week);
                }
            }
        };
        thread.start();
        thread.join();
        return createRanking(RankingType.WEEK, year, week);
    }

    //create month
    @Transactional
    public RankingDto createRankingMonth(CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        Integer year = dateRangeValidation.yearValidator(createRankingDto.getYear());
        Integer numberMonth = dateRangeValidation.monthValidator(createRankingDto.getDateRangeNumber());

        Thread thread = new Thread() {
            @SneakyThrows
            @Override
            public void run() {
                if (findByFirstRankingByType(RankingType.GENERAL_MONTH, year, numberMonth)) {
                    removeAllRankingType(RankingType.MONTH, year, numberMonth);
                }
            }
        };
        thread.start();
        thread.join();
        return createRanking(RankingType.MONTH, year, numberMonth);
    }

    //create 3 months
    @Transactional
    public RankingDto createRankingQuarter(CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        Integer year = dateRangeValidation.yearValidator(createRankingDto.getYear());
        Integer numberQuarter = dateRangeValidation.quarterValidator(createRankingDto.getDateRangeNumber());

        Thread thread = new Thread() {
            @SneakyThrows
            @Override
            public void run() {
                if (findByFirstRankingByType(RankingType.GENERAL_QUARTER, year, numberQuarter)) {
                    removeAllRankingType(RankingType.QUARTER, year, numberQuarter);
                }
            }
        };
        thread.start();
        thread.join();
        return createRanking(RankingType.QUARTER, year, numberQuarter);
    }

    //create half a year
    @Transactional
    public RankingDto createRankingHalfYear(CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        Integer year = dateRangeValidation.yearValidator(createRankingDto.getYear());
        Integer numberHalfYear = dateRangeValidation.halfYearValidator(createRankingDto.getDateRangeNumber());

        Thread thread = new Thread() {
            @SneakyThrows
            @Override
            public void run() {
                if (findByFirstRankingByType(RankingType.GENERAL_HALF_YEAR, year, numberHalfYear)) {
                    removeAllRankingType(RankingType.HALF_YEAR, year, numberHalfYear);
                }
            }
        };
        thread.start();
        thread.join();

        return createRanking(RankingType.HALF_YEAR, year, numberHalfYear);
    }

    //create year
    @Transactional
    public RankingDto createRankingYear(CreateRankingDto createRankingDto) throws OutRange, DataInvalid, NotFound, InterruptedException {
        Integer year = dateRangeValidation.yearValidator(createRankingDto.getYear());
        Integer numberYear = dateRangeValidation.yearValidator(createRankingDto.getDateRangeNumber());


        Thread thread = new Thread() {
            @SneakyThrows
            @Override
            public void run() {
                if (findByFirstRankingByType(RankingType.GENERAL_YEAR, year, numberYear)) {
                    removeAllRankingType(RankingType.YEAR, year, numberYear);
                }
            }
        };
        thread.start();
        thread.join();
        return createRanking(RankingType.YEAR, year, numberYear);
    }


    //get week
    public RankingDto getRankingWeek(Integer year, Integer week) throws DataInvalid, OutRange, NotFound {
        Integer yearValidated = dateRangeValidation.yearValidator(year);
        Integer weekValidated = dateRangeValidation.weekValidator(week);
        return getRanking(yearValidated, weekValidated, RankingType.WEEK);
    }

    //get month
    public RankingDto getRankingMonth(Integer year, Integer month) throws DataInvalid, OutRange, NotFound {
        Integer yearValidated = dateRangeValidation.yearValidator(year);
        Integer monthValidated = dateRangeValidation.monthValidator(month);
        return getRanking(yearValidated, monthValidated, RankingType.MONTH);
    }

    //get 3 month
    public RankingDto getRankingQuarter(Integer year, Integer rankingTypeCounter) throws DataInvalid, OutRange, NotFound {
        Integer yearValidated = dateRangeValidation.yearValidator(year);
        Integer quarterValidated = dateRangeValidation.quarterValidator(rankingTypeCounter);
        return getRanking(yearValidated, quarterValidated, RankingType.QUARTER);
    }

    //get half a year
    public RankingDto getRankingHalfYear(Integer year, Integer rankingTypeCounter) throws DataInvalid, OutRange, NotFound {
        Integer yearValidated = dateRangeValidation.yearValidator(year);
        Integer halfYearValidated = dateRangeValidation.halfYearValidator(rankingTypeCounter);
        return getRanking(yearValidated, halfYearValidated, RankingType.HALF_YEAR);
    }

    //get year
    public RankingDto getRankingYear(Integer year) throws DataInvalid, OutRange, NotFound {
        Integer yearValidated = dateRangeValidation.yearValidator(year);
        return getRanking(yearValidated, year, RankingType.YEAR);
    }


    public boolean findByFirstRankingByType(RankingType rankingType,
                                            Integer year,
                                            Integer rankingTypeCounter) {

        Optional<EmployeeRanking> first = rankingRepository
                .findFirstByYearAndRankingTypeCounterAndRankingType(year, rankingTypeCounter, rankingType);
        return first.isPresent();
    }

    @Transactional
    synchronized RankingDto createRanking(RankingType rankingType,
                                          Integer year,
                                          Integer rankingTypeCounter) throws DataInvalid, NotFound, OutRange {

        long start = System.currentTimeMillis();

        RankingDateRange dateRange = dateRangeRankingFactory.setDateRangeRanking(rankingType, rankingTypeCounter, year);
        ListRankingTypes listRankingTypes = rankingTypeFactory.setRankingType(rankingType);

        List<EmployeeRanking> generalRanking = preparationRankingService.generalRanking(dateRange.getStartRanking(), dateRange.getEndRanking(), year, rankingTypeCounter, listRankingTypes.getGeneral());
        List<EmployeeRanking> firstWorkplaceRanking = preparationRankingService.workplaceRanking(dateRange.getStartRanking(), dateRange.getEndRanking(), year, rankingTypeCounter, listRankingTypes.getFirstWorkplace(), "firstWorkplace");
        List<EmployeeRanking> secondWorkplaceRanking = preparationRankingService.workplaceRanking(dateRange.getStartRanking(), dateRange.getEndRanking(), year, rankingTypeCounter, listRankingTypes.getSecondWorkplace(), "secondWorkplace");
        List<EmployeeRanking> thirdWorkplaceRanking = preparationRankingService.workplaceRanking(dateRange.getStartRanking(), dateRange.getEndRanking(), year, rankingTypeCounter, listRankingTypes.getThirdWorkplace(), "thirdWorkplace");

        List<EmployeeRanking> generalRankingSave = rankingRepository.saveAll(generalRanking);
        List<EmployeeRanking> firstWorkplaceRankingSave = rankingRepository.saveAll(firstWorkplaceRanking);
        List<EmployeeRanking> secondWorkplaceRankingSave = rankingRepository.saveAll(secondWorkplaceRanking);
        List<EmployeeRanking> thirdWorkplaceRankingSave = rankingRepository.saveAll(thirdWorkplaceRanking);

        long stop = System.currentTimeMillis();
        System.out.println("Czas wykonania:" + (stop - start));
        return RankingDto.builder()
                .year(Year.of(year))
                .counterForRankingType(rankingTypeCounter)
                .rankingName(rankingType)
                .startRanking(dateRange.getStartRanking())
                .endRanking(dateRange.getEndRanking())
                .generalRanking(rankingMapper.toListEmployeeGeneralRankingDto(generalRankingSave))
                .firstWorkplaceRanking(rankingMapper.toListEmployeeRankingForWorkplaceDto(firstWorkplaceRankingSave))
                .secondWorkplaceRanking(rankingMapper.toListEmployeeRankingForWorkplaceDto(secondWorkplaceRankingSave))
                .thirdWorkplaceRanking(rankingMapper.toListEmployeeRankingForWorkplaceDto(thirdWorkplaceRankingSave))
                .build();
    }

    private RankingDto getRanking(Integer year, Integer rankingTypeCounter, RankingType rankingType) throws DataInvalid, OutRange, NotFound {

        RankingDateRange dateRange = dateRangeRankingFactory.setDateRangeRanking(rankingType, rankingTypeCounter, year);
        ListRankingTypes listRankingTypes = rankingTypeFactory.setRankingType(rankingType);

        List<EmployeeGeneralRankingDto> generalRankingList = rankingRepository.findAllByYearAndRankingTypeCounterAndRankingType(year, rankingTypeCounter, listRankingTypes.getGeneral().name())
                .stream()
                .map(rankingMapper::toEmployeeGeneralRankingDto)
                .collect(Collectors.toList());

        List<EmployeeRankingForWorkplaceDto> firstWorkplaceRanking = rankingRepository
                .findAllByYearAndRankingTypeCounterAndRankingType(year, rankingTypeCounter, listRankingTypes.getFirstWorkplace().name())
                .stream()
                .map(rankingMapper::toEmployeeRankingForWorkplaceDto)
                .collect(Collectors.toList());

        List<EmployeeRankingForWorkplaceDto> secondWorkplaceRanking = rankingRepository
                .findAllByYearAndRankingTypeCounterAndRankingType(year, rankingTypeCounter, listRankingTypes.getSecondWorkplace().name())
                .stream()
                .map(rankingMapper::toEmployeeRankingForWorkplaceDto)
                .collect(Collectors.toList());

        List<EmployeeRankingForWorkplaceDto> thirdWorkplaceRanking = rankingRepository
                .findAllByYearAndRankingTypeCounterAndRankingType(year, rankingTypeCounter, listRankingTypes.getThirdWorkplace().name())
                .stream()
                .map(rankingMapper::toEmployeeRankingForWorkplaceDto)
                .collect(Collectors.toList());

        return RankingDto.builder()
                .year(Year.of(year))
                .counterForRankingType(rankingTypeCounter)
                .startRanking(dateRange.getStartRanking())
                .endRanking(dateRange.getEndRanking())
                .rankingName(rankingType)
                .generalRanking(generalRankingList)
                .firstWorkplaceRanking(firstWorkplaceRanking)
                .secondWorkplaceRanking(secondWorkplaceRanking)
                .thirdWorkplaceRanking(thirdWorkplaceRanking)
                .build();
    }

    @Transactional
    public synchronized void removeRanking(RankingType rankingType, Integer year, Integer rankingTypeCounter) throws OutRange, InterruptedException {
        Integer yearValidator = dateRangeValidation.yearValidator(year);
        List<EmployeeRanking> ranking = rankingRepository.findAllByYearAndRankingTypeCounterAndRankingType(yearValidator, rankingTypeCounter, rankingType.name());

        rankingRepository.deleteAll(ranking);
    }

    @SneakyThrows
    @Transactional
    public void removeAllRankingType(RankingType rankingType, Integer year, Integer rankingTypeCounter) throws DataInvalid, OutRange {
        RankingType general;
        RankingType firstWorkplace;
        RankingType secondWorkplace;
        RankingType thirdWorkplace;

        switch (rankingType) {
            case WEEK: {
                general = RankingType.GENERAL_WEEK;
                firstWorkplace = RankingType.WORKPLACE_FIRST_WEEK;
                secondWorkplace = RankingType.WORKPLACE_SECOND_WEEK;
                thirdWorkplace = RankingType.WORKPLACE_THIRD_WEEK;
                break;
            }
            case MONTH: {
                general = RankingType.GENERAL_MONTH;
                firstWorkplace = RankingType.WORKPLACE_FIRST_MONTH;
                secondWorkplace = RankingType.WORKPLACE_SECOND_MONTH;
                thirdWorkplace = RankingType.WORKPLACE_THIRD_MONTH;
                break;
            }
            case QUARTER: {
                general = RankingType.WORKPLACE_THIRD_QUARTER;
                firstWorkplace = RankingType.WORKPLACE_THIRD_QUARTER;
                secondWorkplace = RankingType.WORKPLACE_THIRD_QUARTER;
                thirdWorkplace = RankingType.WORKPLACE_THIRD_QUARTER;
                break;
            }
            case HALF_YEAR: {
                general = RankingType.GENERAL_HALF_YEAR;
                firstWorkplace = RankingType.WORKPLACE_FIRST_HALF_YEAR;
                secondWorkplace = RankingType.WORKPLACE_SECOND_HALF_YEAR;
                thirdWorkplace = RankingType.WORKPLACE_THIRD_HALF_YEAR;
                break;
            }
            case YEAR: {
                general = RankingType.GENERAL_YEAR;
                firstWorkplace = RankingType.WORKPLACE_FIRST_YEAR;
                secondWorkplace = RankingType.WORKPLACE_SECOND_YEAR;
                thirdWorkplace = RankingType.WORKPLACE_THIRD_YEAR;
                break;
            }
            default: {
                throw new DataInvalid("unknown type");
            }
        }
        removeRanking(general, year, rankingTypeCounter);
        removeRanking(firstWorkplace, year, rankingTypeCounter);
        removeRanking(secondWorkplace, year, rankingTypeCounter);
        removeRanking(thirdWorkplace, year, rankingTypeCounter);

    }
}
