package dev.polproject.hextl.service.user;

import dev.polproject.hextl.controller.dto.user.RoleDto;
import dev.polproject.hextl.model.user.Role;
import dev.polproject.hextl.repository.user.RoleRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.user.RoleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RoleService {

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final RoleMapper roleMapper;

    public List<RoleDto> getAllRole(){
        return roleRepository.findAll().stream().map(roleMapper::toDto).collect(Collectors.toList());
    }

    public RoleDto getRoleById(int id) throws NotFound {
        return roleRepository.findById(id).map(roleMapper::toDto).orElseThrow(()-> new NotFound(id));
    }
    public Role getRoleModelById(int id) throws NotFound {
        return roleRepository.findById(id).orElseThrow(()-> new NotFound(id));
    }
}
