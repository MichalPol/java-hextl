package dev.polproject.hextl.service.user;

import dev.polproject.hextl.controller.dto.mail.ResetPassword;
import dev.polproject.hextl.controller.dto.user.*;
import dev.polproject.hextl.model.user.Role;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.repository.user.UserRepository;
import dev.polproject.hextl.service.app.PasswordGenerator;
import dev.polproject.hextl.service.exeption.*;
import dev.polproject.hextl.service.mail.MailService;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import javax.validation.constraints.Email;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final PasswordGenerator passwordGenerator;
    private final MailService mailService;

    public List<UserDto> getAllUser(Boolean filterIsActive) {
        return userRepository
                .findAll()
                .stream()
                .filter(u -> u.getActive().equals(filterIsActive))
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public SessionUser getUserAuthentication(String login) throws NotFound {
        User user = getUserModelByLogin(login);
        return userMapper.toSessionUserDto(user);
    }

    public UserDto getUserById(Integer id, Boolean filterIsActive) throws NotFound {
        return userRepository
                .findById(id)
                .filter(u -> u.getActive().equals(filterIsActive))
                .map(userMapper::toDto)
                .orElseThrow(() -> new NotFound(id));
    }

    public User getUserModelById(Integer id) throws NotFound {
        return userRepository
                .findById(id)
                .filter(User::getActive)
                .orElseThrow(() -> new NotFound(id));
    }

    public User getUserModelByLogin(String login) throws NotFound {
        return userRepository
                .findAll()
                .stream()
                .filter(User::getActive).filter(user -> user.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new NotFound("Not found login: " + login));
    }

    public UserDto getUserByLogin(String login) throws NotFound {
        return userRepository
                .findByLogin(login)
                .map(userMapper::toDto)
                .orElseThrow(() -> new NotFound("Not found login: " + login));
    }

    public UserDto getUserByEmail(@Email String email) throws NotFound {
        return userRepository
                .findByEmail(email)
                .map(userMapper::toDto)
                .orElseThrow(() -> new NotFound("Not found e-mail: " + email));
    }

    @Transactional
    public UserDto createUser(CreateUserDto createUserDto) throws DataInvalid, NotFound, Conflict {
        User userLogin = userRepository
                .findAll()
                .stream()
                .filter(u -> u.getLogin().equals(createUserDto.getLogin()))
                .findFirst()
                .orElse(null);

        User userEmail = userRepository.findAll()
                .stream().filter(u -> u.getEmail()
                        .equals(createUserDto.getEmail()))
                .findFirst()
                .orElse(null);

        if (userLogin != null || userEmail != null) {
            throw new DataInvalid("Login or email are not provided");
        }
        if (createUserDto.getLogin().length() > 5 ||
                createUserDto.getPassword().length() > 5 ||
                createUserDto.getName().length() > 3 ||
                createUserDto.getSurname().length() > 2) {

            User user = new User();
            Role role = roleService.getRoleModelById(createUserDto.getRole());

            user.setId(null);
            user.setName(createUserDto.getName());
            user.setSurname(createUserDto.getSurname());
            user.setLogin(createUserDto.getLogin());
            user.setActive(true);
            user.setEmail(createUserDto.getEmail());
            user.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
            user.setCreateAt(OffsetDateTime.now());
            user.setPathAvatar("/avatar/default.jpg");
            user.setRole(role);

            User userCreatedSuccessfully = userRepository.save(user);
            return userMapper.toDto(userCreatedSuccessfully);
        }
        throw new DataInvalid("Character length error");
    }

    @Transactional
    public UserDto updateUser(Integer id, UpdateUserDto updateUserDto) throws NotFound, DataInvalid {

        User user = getUserModelById(id);
        if (updateUserDto.getPassword().length() > 5 ||
                updateUserDto.getName().length() > 3 ||
                updateUserDto.getSurname().length() > 2) {

            Role role = roleService.getRoleModelById(updateUserDto.getRoleId());

            user.setName(updateUserDto.getName());
            user.setSurname(updateUserDto.getSurname());
            user.setPassword(passwordEncoder.encode(updateUserDto.getPassword()));
            user.setPathAvatar(updateUserDto.getPathAvatar());
            user.setUpdateAt(OffsetDateTime.now());
            user.setRole(role);

            User userUpdate = userRepository.save(user);
            return userMapper.toDto(userUpdate);
        }
        throw new DataInvalid("Character length error");
    }


    @Transactional
    public UserDto addUserToRole(int idUser, int idRole) throws NotFound {
        User userFind = getUserModelById(idUser);
        Role role = roleService.getRoleModelById(idRole);
        userFind.setRole(role);
        userRepository.save(userFind);
        return userMapper.toDto(userFind);
    }

    @Transactional
    public UserDto deleteUserById(Integer id) throws NotFound {
        User user = getUserModelById(id);
        user.setActive(false);
        user.setUpdateAt(OffsetDateTime.now());
        userRepository.save(user);
        return userMapper.toDto(user);
    }

    @Transactional
    public UserDto changePasswordUser(Authentication authentication, ChangePasswordDto changePassword) throws NotFound, IncorrectPassword {

        User user = getUserModelByLogin(authentication.getName());
        if (changePassword.getNewPassword().equals(changePassword.getCheckPassword())) {

            if (passwordEncoder.matches(changePassword.getOldPassword(), user.getPassword())) {
                user.setPassword(passwordEncoder.encode(changePassword.getNewPassword()));
                User userChange = userRepository.save(user);
                return userMapper.toDto(userChange);

            }
        }
        throw new IncorrectPassword("Incorrect password");
    }

    @Transactional
    public void resetPasswordByAdmin(ResetPassword resetPassword, Authentication authentication) throws NotFound, Unauthorized, DataInvalid, MessagingException {

        if (null == authentication.getName()) {
            throw new Unauthorized("You must login");
        }

        User userAdmin = getUserModelByLogin(authentication.getName());

        if (!userAdmin.getRole().getNameRole().equals("ADMIN")) {
            throw new Unauthorized("You must have administrator rights ");
        }

        if (!passwordEncoder.matches(resetPassword.getPasswordUserAdmin(), userAdmin.getPassword())) {
            throw new DataInvalid("Password is invalid");
        }

        User userUpdatePassword = getUserModelById(resetPassword.getResetPasswordWithUserId());

        String newPassword = passwordGenerator.getNewPassword();
        userUpdatePassword.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(userUpdatePassword);
        mailService.sendNewPassword(userUpdatePassword.getEmail(), newPassword);
    }

}
