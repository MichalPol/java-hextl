package dev.polproject.hextl.service.timetable;

import dev.polproject.hextl.controller.dto.timetable.workShift.WorkShiftDto;
import dev.polproject.hextl.model.timetable.WorkLine;
import dev.polproject.hextl.model.timetable.WorkShift;
import dev.polproject.hextl.repository.timetable.workShift.ShiftRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.workPlan.WorkShiftMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class WorkShiftService {

    private final ShiftRepository shiftRepository;
    private final WorkShiftMapper workShiftMapper;

    public List<WorkShiftDto> getAllShift(){
        return shiftRepository
                .findAll()
                .stream()
                .map(workShiftMapper::toDto)
                .collect(Collectors.toList());
    }

    public WorkShiftDto getShiftById(Integer idShift) throws NotFound {
        return shiftRepository
                .findById(idShift)
                .map(workShiftMapper::toDto)
                .orElseThrow(()-> new NotFound(idShift));
    }

    public WorkShift getShiftModelById(Integer idShift) throws NotFound {
        return shiftRepository
                .findById(idShift)
                .orElseThrow(()-> new NotFound(idShift));
    }

    public WorkShift createWorkShift(List<WorkLine> workLines, Integer shiftNumber){
        return WorkShift.builder()
                .id(null)
                .shiftNumber(shiftNumber)
                .lines(workLines)
                .shiftsLeader(new ArrayList<>())
                .unskilledWorker(new ArrayList<>())
                .supervision(new ArrayList<>())
                .other(new ArrayList<>())
                .build();
    }
}
