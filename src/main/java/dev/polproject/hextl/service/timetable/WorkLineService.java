package dev.polproject.hextl.service.timetable;

import dev.polproject.hextl.controller.dto.timetable.workLine.WorkLineDto;
import dev.polproject.hextl.model.timetable.WorkLine;
import dev.polproject.hextl.model.timetable.Workplace;
import dev.polproject.hextl.repository.timetable.workLine.WorkLineRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.workPlan.WorkLineMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class WorkLineService {

    private final WorkLineRepository workLineRepository;
    private final WorkLineMapper workLineMapper;

    public List<WorkLineDto> getAllWorkLine(){
        return workLineRepository
                .findAll()
                .stream()
                .map(workLineMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<WorkLineDto> getListWorkLineDto(List<WorkLine> workLineList){
        return workLineList
                .stream()
                .map(workLineMapper::toDto)
                .collect(Collectors.toList());
    }

    public WorkLine getWorkLineModelById(Integer idWorkLine) throws NotFound {
        return workLineRepository.findById(idWorkLine).orElseThrow(() -> new NotFound(idWorkLine));
    }

    public WorkLine createWorkLine(List<Workplace> workplaces, Integer numberLine){
        return WorkLine.builder()
            .id(null)
            .lineNumber(numberLine)
            .workplaces(workplaces)
            .build();
    }
}
