package dev.polproject.hextl.service.timetable;

import dev.polproject.hextl.controller.config.app.AppConfigProperties;
import dev.polproject.hextl.controller.dto.timetable.workLine.UpdateWorkLineDto;
import dev.polproject.hextl.controller.dto.timetable.workPlace.UpdateWorkplaceDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.CreateWorkPlanDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.UpdateWorkPlanDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.WorkPlanDto;
import dev.polproject.hextl.controller.dto.timetable.workShift.UpdateWorkShiftDto;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.model.timetable.WorkLine;
import dev.polproject.hextl.model.timetable.WorkPlan;
import dev.polproject.hextl.model.timetable.WorkShift;
import dev.polproject.hextl.model.timetable.Workplace;
import dev.polproject.hextl.repository.timetable.workPlan.WorkPlanRepository;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.workPlan.WorkPlanMapper;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class WorkPlanService {

    private final WorkPlanRepository workPlanRepository;
    private final WorkPlanMapper workPlanMapper;
    private final UserService userService;
    private final EmployeeService employeeService;
    private final WorkShiftService workShiftService;
    private final WorkLineService workLineService;
    private final WorkplaceService workplaceService;
    private final AuditLogService auditLogService;
    private final AppConfigProperties appConfigProperties;

    public List<WorkPlanDto> getAllWorkPlan() {
        return workPlanRepository
                .findAll()
                .stream()
                .map(workPlanMapper::toDto).collect(Collectors.toList());
    }

    public WorkPlanDto getWorkPlanById(Integer id) throws NotFound {
        return workPlanRepository
                .findById(id)
                .map(workPlanMapper::toDto).orElseThrow(() -> new NotFound(id));
    }

    public WorkPlan getWorkPlanModelById(Integer id) throws NotFound {
        return workPlanRepository
                .findById(id)
                .orElseThrow(() -> new NotFound(id));
    }

    public WorkPlanDto getFirstWorkPlanBetweenDate(LocalDate start, LocalDate end) throws NotFound, DataInvalid {

        if (start.isAfter(end)){
            throw  new DataInvalid("Data is invalid");
        }
        List<WorkPlan> allByStartDayBetween = workPlanRepository.findAllByStartDayBetween(start, end);

        if (allByStartDayBetween.size() == 1) {
            return workPlanMapper.toDto(allByStartDayBetween.get(0));

        } else if (allByStartDayBetween.size() == 0) {
            throw new NotFound(start, end);
        }
        throw new DataInvalid("Range is invalid");
    }

    @Transactional
    public WorkPlanDto createWorkPlan(CreateWorkPlanDto createWorkPlan) throws NotFound, Conflict, DataInvalid {

        User user = userService.getUserModelById(createWorkPlan.getCreateByIdUser());
        try {
             getFirstWorkPlanBetweenDate(createWorkPlan.getStartDay(), createWorkPlan.getEndDay());

        } catch (NotFound notFound) {

            Integer numberJobShifts = appConfigProperties.getNumberJobShifts();
            Integer numberLine = appConfigProperties.getWorkPlanNumbersLine();
            Integer workPlanNumberWorkplaces = appConfigProperties.getWorkPlanNumberWorkplaces();

            List<WorkShift> workShifts = new ArrayList<>();
            for (int i = 0; i < numberJobShifts; i++) {

                List<WorkLine> workLines = new ArrayList<>();
                for (int j = 0; j < numberLine; j++) {

                    List<Workplace> workplaces = workplaceService.createWorkspaces(workPlanNumberWorkplaces);

                    WorkLine workLine = workLineService.createWorkLine(workplaces, j + 1);
                    workLines.add(workLine);
                }
                WorkShift workShift = workShiftService.createWorkShift(workLines, i + 1);
                workShifts.add(workShift);
            }

            WorkPlan workPlan = WorkPlan.builder()
                    .id(null)
                    .startDay(createWorkPlan.getStartDay())
                    .endDay(createWorkPlan.getEndDay())
                    .workShifts(workShifts)
                    .holidaysEmployee(new ArrayList<>())
                    .infoHolidays(null)
                    .absenceEmployee(new ArrayList<>())
                    .infoAbsence(null)
                    .createBy(user)
                    .updateBy(user)
                    .createAt(OffsetDateTime.now())
                    .updateAt(OffsetDateTime.now())
                    .build();

            WorkPlan save = workPlanRepository.save(workPlan);

            StringBuilder msg = new StringBuilder();
            msg
                    .append("Create new Work plan: ")
                    .append(createWorkPlan.getStartDay())
                    .append(" ")
                    .append(createWorkPlan.getEndDay())
                    .append(" ")
                    .append(createWorkPlan.getCreateByIdUser())
                    .append(" ")
                    .append("msg - Create successful");
            auditLogService.action(ControlCode.WORK_PLAN_CREATE, msg.toString(), createWorkPlan.getCreateByIdUser() );
            return workPlanMapper.toDto(save);

        } catch (DataInvalid dataInvalid) {
            StringBuilder msg = new StringBuilder();
            msg
                    .append("Error create new Work plan: ")
                    .append(createWorkPlan.getStartDay())
                    .append(" ")
                    .append(createWorkPlan.getEndDay())
                    .append(" ")
                    .append(createWorkPlan.getCreateByIdUser())
                    .append(" ")
                    .append("msg - Data Invalid, Range is invalid");

            auditLogService.action(ControlCode.ERROR_WORK_PLAN_NOT_CREATED_RANGE_INVALID, msg.toString(), createWorkPlan.getCreateByIdUser() );
            throw new DataInvalid("Data Invalid, Range is invalid");
        }
        StringBuilder msg = new StringBuilder();
        msg
                .append("Error create new Work plan: ")
                .append(createWorkPlan.getStartDay())
                .append(" ")
                .append(createWorkPlan.getEndDay())
                .append(" ")
                .append(createWorkPlan.getCreateByIdUser())
                .append(" ")
                .append("msg - work plan already exists");
        auditLogService.action(ControlCode.ERROR_WORK_PLAN_NOT_CREATED_ALREADY_EXISTS, msg.toString(), createWorkPlan.getCreateByIdUser() );
        throw new Conflict("work plan already exists");

    }

    @Transactional
    public WorkPlanDto updateWorkPlan(Integer idUser, Integer idWorkPlan, UpdateWorkPlanDto updateWorkPlanDto) throws NotFound, DataInvalid {

        User userModel = userService.getUserModelById(idUser);

        WorkPlanDto workPlanBetweenDate = getFirstWorkPlanBetweenDate(updateWorkPlanDto.getStartDay(), updateWorkPlanDto.getEndDay());
        if (!workPlanBetweenDate.getId().equals(idWorkPlan)){
            throw new DataInvalid("Error - work plan ID and scope are not the same");
        }
        WorkPlan workPlanModel = getWorkPlanModelById(idWorkPlan);
        List<UpdateWorkShiftDto> updateWorkShiftDto = updateWorkPlanDto.getWorkShifts();
        List<WorkShift> updateWorkShift = new ArrayList<>();

        for (int i = 0; i < updateWorkShiftDto.size(); i++) {
            WorkShift workShift = workShiftService.getShiftModelById(updateWorkShiftDto.get(i).getId());

            List<UpdateWorkLineDto> updateWorkLineDto = updateWorkPlanDto.getWorkShifts().get(i).getLines();
            List<WorkLine>  updateWorkLine = new ArrayList<>();

            for (int j = 0; j < updateWorkLineDto.size(); j++) {
                WorkLine workLine = workLineService.getWorkLineModelById(updateWorkLineDto.get(j).getId());
                List<UpdateWorkplaceDto> updateWorkplaceDto = updateWorkLineDto.get(j).getWorkplaces();
                List<Workplace> updateWorkplace = new ArrayList<>();

                for (int k = 0; k < updateWorkplaceDto.size(); k++) {
                    Workplace workplace = workplaceService.getWorkplaceModel(updateWorkplaceDto.get(k).getId());
                    workplace.setNameWorkplace(updateWorkplaceDto.get(k).getNameWorkplace());
                    workplace.setEmployeeListWorkplaces(employeeService.getAllEmployeeWithId(updateWorkplaceDto.get(k).getEmployeeListWorkplaces()));
                    updateWorkplace.add(workplace);
                }

                workLine.setLineNumber(updateWorkLineDto.get(j).getLineNumber());
                workLine.setWorkplaces(updateWorkplace);
                updateWorkLine.add(workLine);
            }

            workShift.setShiftNumber(updateWorkShiftDto.get(i).getShiftNumber());
            workShift.setLines(updateWorkLine);
            workShift.setShiftsLeader(employeeService.getAllEmployeeWithId(updateWorkShiftDto.get(i).getShiftsLeader()));
            workShift.setUnskilledWorker(employeeService.getAllEmployeeWithId(updateWorkShiftDto.get(i).getUnskilledWorker()));
            workShift.setSupervision(employeeService.getAllEmployeeWithId(updateWorkShiftDto.get(i).getSupervision()));
            workShift.setOther(employeeService.getAllEmployeeWithId(updateWorkShiftDto.get(i).getOther()));
            workShift.setComments(updateWorkShiftDto.get(i).getComments());

            updateWorkShift.add(workShift);

        }
        workPlanModel.setStartDay(updateWorkPlanDto.getStartDay());
        workPlanModel.setEndDay(updateWorkPlanDto.getEndDay());
        workPlanModel.setWorkShifts(updateWorkShift);
        workPlanModel.setHolidaysEmployee(employeeService.getAllEmployeeWithId(updateWorkPlanDto.getHolidaysEmployees()));
        workPlanModel.setInfoHolidays(updateWorkPlanDto.getInfoHolidays());
        workPlanModel.setAbsenceEmployee(employeeService.getAllEmployeeWithId(updateWorkPlanDto.getAbsenceEmployees()));
        workPlanModel.setInfoAbsence(updateWorkPlanDto.getInfoAbsence());
        workPlanModel.setUpdateBy(userModel);
        workPlanModel.setUpdateAt(OffsetDateTime.now());

        WorkPlan save = workPlanRepository.save(workPlanModel);
        return workPlanMapper.toDto(save);
    }
}
