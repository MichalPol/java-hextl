package dev.polproject.hextl.service.timetable;

import dev.polproject.hextl.controller.dto.timetable.workPlace.WorkplaceDto;
import dev.polproject.hextl.model.timetable.Workplace;
import dev.polproject.hextl.repository.timetable.workplace.WorkplaceRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.workPlan.WorkplaceMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class WorkplaceService {

    private final WorkplaceRepository workplaceRepository;
    private final WorkplaceMapper workplaceMapper;

    public List<WorkplaceDto> getListWorkplaceDto(List<Workplace> workplaceList){
        return workplaceList
                .stream()
                .map(workplaceMapper::toDto)
                .collect(Collectors.toList());
    }

    public Workplace getWorkplaceModel(Integer idWorkplace) throws NotFound {
        return workplaceRepository
                .findById(idWorkplace)
                .orElseThrow(()-> new NotFound(idWorkplace));
    }

    public List<Workplace> createWorkspaces(Integer seatReservation ){
        List<Workplace> workplaces = new ArrayList<>();
        for (int k = 0; k < seatReservation; k++) {
            Workplace workplace = Workplace.builder()
                    .id(null)
                    .nameWorkplace("workplace-" + (1 + k))
                    .employeeListWorkplaces(new ArrayList<>())
                    .build();
            workplaces.add(workplace);
        }
        return workplaces;

    }


}
