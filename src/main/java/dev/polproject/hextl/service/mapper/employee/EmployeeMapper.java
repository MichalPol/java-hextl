package dev.polproject.hextl.service.mapper.employee;

import dev.polproject.hextl.controller.dto.employee.EmployeeDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeProductionReportsDto;
import dev.polproject.hextl.model.employee.Employee;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EmployeeMapper {

    public EmployeeDto toDto(Employee employee){
        return EmployeeDto.builder()
                .id(employee.getId())
                .name(employee.getName())
                .lastName(employee.getLastName())
                .isActive(employee.getActive())
                .information(employee.getInformation())
                .createAt(employee.getCreateAt())
                .updateAt(employee.getUpdateAt())
                .build();
    }

    public EmployeeIdNameSurnameDto toEmployeeIdNameSurnameDto(Employee employee){
        return EmployeeIdNameSurnameDto.builder()
                .id(employee.getId())
                .name(employee.getName())
                .lastName(employee.getLastName())
                .build();
    }

    public EmployeeProductionReportsDto toEmployeeProductionReportsDto(Employee employee){
        List<Long> idProductionReports = employee.getEmployeeProductionReport()
                .stream()
                .map(r -> r.getId())
                .collect(Collectors.toList());
        return EmployeeProductionReportsDto.builder()
                .id(employee.getId())
                .name(employee.getName())
                .lastName(employee.getLastName())
                .isActive(employee.getActive())
                .information(employee.getInformation())
                .idProductionReports(idProductionReports)
                .build();
    }
}
