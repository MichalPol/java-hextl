package dev.polproject.hextl.service.mapper.product;

import dev.polproject.hextl.controller.dto.product.ProductDto;
import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.model.product.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public ProductDto toProductDto(Product product){
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .isSerialized(product.getIsSerialized())
                .isActive(product.getActive())
                .itemsPerCycle(product.getItemsPerCycle())
                .instructionId(product.getInstructionId())
                .description(product.getDescription())
                .createAt(product.getCreateAt())
                .updateAt(product.getUpdateAt())
                .build();
    }

    public ProductIdNameSerializedDto toProductIdNameSerializedDto(Product product){
        return ProductIdNameSerializedDto.builder()
                .id(product.getId())
                .name(product.getName())
                .isActive(product.getIsActive())
                .isSerialized(product.getIsSerialized())
                .build();
    }
}
