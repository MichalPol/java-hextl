package dev.polproject.hextl.service.mapper.ranking;

import dev.polproject.hextl.controller.dto.ranking.EmployeeGeneralRankingDto;
import dev.polproject.hextl.controller.dto.ranking.EmployeeRankingForWorkplaceDto;
import dev.polproject.hextl.model.ranking.EmployeeRanking;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.mapper.line.LineMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RankingMapper {

    @Autowired
    private  EmployeeMapper employeeMapper;
    @Autowired
    private LineMapper lineMapper;

    public EmployeeRankingForWorkplaceDto toEmployeeRankingForWorkplaceDto(EmployeeRanking employeeRanking){
        return EmployeeRankingForWorkplaceDto.builder()
                .line(lineMapper.toLineIdNameNumberDto(employeeRanking.getLine()))
                .employee(employeeMapper.toEmployeeIdNameSurnameDto(employeeRanking.getEmployee()))
                .averagePerHour(employeeRanking.getAveragePerHour())
                .percentage(employeeRanking.getPercentage())
                .averageSpeed(employeeRanking.getAverageSpeed())
                .totalProduced(employeeRanking.getTotalProduced())
                .build();
    }

    public EmployeeGeneralRankingDto toEmployeeGeneralRankingDto(EmployeeRanking employeeRanking){
        return EmployeeGeneralRankingDto.builder()
                .employee(employeeMapper.toEmployeeIdNameSurnameDto(employeeRanking.getEmployee()))
                .averagePerHour(employeeRanking.getAveragePerHour())
                .percentage(employeeRanking.getPercentage())
                .averageSpeed(employeeRanking.getAverageSpeed())
                .totalProduced(employeeRanking.getTotalProduced())
                .build();
    }

    public List <EmployeeRankingForWorkplaceDto> toListEmployeeRankingForWorkplaceDto(List <EmployeeRanking> employeesRanking){

        List<EmployeeRankingForWorkplaceDto> rankingDtoList = new ArrayList<>();

        for (EmployeeRanking employeeRanking : employeesRanking) {
            EmployeeRankingForWorkplaceDto build = EmployeeRankingForWorkplaceDto.builder()
                    .line(lineMapper.toLineIdNameNumberDto(employeeRanking.getLine()))
                    .employee(employeeMapper.toEmployeeIdNameSurnameDto(employeeRanking.getEmployee()))
                    .averagePerHour(employeeRanking.getAveragePerHour())
                    .percentage(employeeRanking.getPercentage())
                    .averageSpeed(employeeRanking.getAverageSpeed())
                    .totalProduced(employeeRanking.getTotalProduced())
                    .build();
            rankingDtoList.add(build);
        }
        return rankingDtoList;
    }

    public List <EmployeeGeneralRankingDto> toListEmployeeGeneralRankingDto(List<EmployeeRanking> employeesRanking){

        List<EmployeeGeneralRankingDto> generalRankingDtoList = new ArrayList<>();
        for(EmployeeRanking employeeRanking: employeesRanking){
            EmployeeGeneralRankingDto build = EmployeeGeneralRankingDto.builder()
                    .employee(employeeMapper.toEmployeeIdNameSurnameDto(employeeRanking.getEmployee()))
                    .averagePerHour(employeeRanking.getAveragePerHour())
                    .percentage(employeeRanking.getPercentage())
                    .averageSpeed(employeeRanking.getAverageSpeed())
                    .totalProduced(employeeRanking.getTotalProduced())
                    .build();
            generalRankingDtoList.add(build);
        }
        return generalRankingDtoList;
    }

}
