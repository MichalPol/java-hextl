package dev.polproject.hextl.service.mapper.productivity;

import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.mapper.product.ProductMapper;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import lombok.*;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProductionReportMapper {

    private final EmployeeMapper employeeMapper;
    private final ProductMapper productMapper;
    private final UserMapper userMapper;

    public ProductionReportDto toProductionReportDto(ProductionReport productionReport){

        return ProductionReportDto.builder()
                .id(productionReport.getId())
                .lineId(productionReport.getLine().getId())
                .productionStart(productionReport.getProductionStart())
                .productionEnd(productionReport.getProductionEnd())
                .productionTimeToHour(productionReport.getProductionTimeToHour())
                .product(productMapper
                        .toProductDto(productionReport.getProduct()))
                .series(productionReport.getSeries())
                .firstWorkplace(employeeMapper
                        .toEmployeeIdNameSurnameDto(productionReport.getFirstWorkplace()))
                .secondWorkplace(employeeMapper
                        .toEmployeeIdNameSurnameDto(productionReport.getSecondWorkplace()))
                .thirdWorkplace(employeeMapper
                        .toEmployeeIdNameSurnameDto(productionReport.getThirdWorkplace()))
                .speedMachinePerCycle(productionReport.getSpeedMachinePerCycle())
                .totalQuantityProduced(productionReport.getTotalQuantityProduced())
                .maxPossibleItems(productionReport.getMaxPossibleItems())
                .performancePerHour(productionReport.getPerformancePerHour())
                .percentagePerformance(productionReport.getPercentagePerformance())
                .description(productionReport.getDescription())
                .createAt(productionReport.getCreateAt())
                .updateAt(productionReport.getUpdateAt())
                .createdByUser(userMapper.toUserIdNameSurnameDto(productionReport.getCreatedByUser()))
                .updatedByUser(userMapper.toUserIdNameSurnameDto(productionReport.getUpdatedByUser()))
                .build();
    }
}
