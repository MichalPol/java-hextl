package dev.polproject.hextl.service.mapper.workPlan;

import dev.polproject.hextl.controller.dto.timetable.workPlace.WorkplaceDto;
import dev.polproject.hextl.model.timetable.Workplace;
import dev.polproject.hextl.service.employee.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class WorkplaceMapper {

    private final EmployeeService employeeService;


    public WorkplaceDto toDto(Workplace workPlace){
        return WorkplaceDto.builder()
                .id(workPlace.getId())
                .nameWorkplace(workPlace.getNameWorkplace())
                .employeeListWorkplaces(employeeService.getIdNameSurnameEmployee(workPlace.getEmployeeListWorkplaces()))
                .build();
    }
}
