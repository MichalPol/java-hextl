package dev.polproject.hextl.service.mapper.line;

import dev.polproject.hextl.controller.dto.line.LineDto;
import dev.polproject.hextl.controller.dto.line.LineIdNameNumberDto;
import dev.polproject.hextl.controller.dto.line.LineProductionReportsDto;
import dev.polproject.hextl.model.line.Line;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class LineMapper {

    public LineDto toLineDto(Line line){
        return LineDto.builder()
                .id(line.getId())
                .name(line.getName())
                .numberLine(line.getNumberLine())
                .isActive(line.getActive())
                .build();
    }


    public LineProductionReportsDto toLineProductionReportsDto(Line line){

        List<Long> listIdProductionReport = line.getLineProductionReports()
                .stream()
                .map(l -> l.getId())
                .collect(Collectors.toList());

       return LineProductionReportsDto.builder()
                .id(line.getId())
                .name(line.getName())
                .numberLine(line.getNumberLine())
                .isActive(line.getActive())
                .idProductionLineReports(listIdProductionReport)
                .build();
    }

    public LineIdNameNumberDto toLineIdNameNumberDto(Line line){
        return LineIdNameNumberDto.builder()
                .id(line.getId())
                .name(line.getName())
                .numberLine(line.getNumberLine())
                .build();
    }
}
