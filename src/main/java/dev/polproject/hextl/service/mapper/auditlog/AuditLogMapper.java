package dev.polproject.hextl.service.mapper.auditlog;

import dev.polproject.hextl.controller.dto.auditLog.AuditLogDto;
import dev.polproject.hextl.model.auditLog.AuditLog;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class AuditLogMapper {

    private final UserMapper userMapper;


    public AuditLogDto toDto (AuditLog auditLog){
        return AuditLogDto.builder()
                .id(auditLog.getId())
                .logControlCode(auditLog.getControlCode())
                .eventMessage(auditLog.getActionMessage())
                .createdByUser(userMapper.toUserIdNameSurnameDto(auditLog.getUser()))
                .createAt(auditLog.getCreateAt())
                .build();
    }
}
