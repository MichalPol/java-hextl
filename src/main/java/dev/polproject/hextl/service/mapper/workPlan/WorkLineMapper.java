package dev.polproject.hextl.service.mapper.workPlan;

import dev.polproject.hextl.controller.dto.timetable.workLine.WorkLineDto;
import dev.polproject.hextl.controller.dto.timetable.workPlace.WorkplaceDto;
import dev.polproject.hextl.model.timetable.WorkLine;
import dev.polproject.hextl.service.timetable.WorkplaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;


@RequiredArgsConstructor
@Component
public class WorkLineMapper {

    private final WorkplaceService workplaceService;

    public WorkLineDto toDto (WorkLine workLine){
        List<WorkplaceDto> listWorkplaceDto = workplaceService.getListWorkplaceDto(workLine.getWorkplaces());

        return WorkLineDto.builder()
                .id(workLine.getId())
                .lineNumber(workLine.getLineNumber())
                .workplaces(listWorkplaceDto)
                .build();
    }
}
