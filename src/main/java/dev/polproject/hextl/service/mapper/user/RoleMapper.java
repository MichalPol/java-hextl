package dev.polproject.hextl.service.mapper.user;

import dev.polproject.hextl.controller.dto.user.RoleDto;
import dev.polproject.hextl.model.user.Role;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {
    public RoleDto toDto(Role role){
        return RoleDto.builder()
                .id(role.getId())
                .nameRole(role.getNameRole())
                .description(role.getDescription())
                .build();
    }
    public Role toModel(RoleDto roleDto){
        return Role.builder()
                .id(roleDto.getId())
                .nameRole(roleDto.getNameRole())
                .description(roleDto.getDescription())
                .build();
    }
}
