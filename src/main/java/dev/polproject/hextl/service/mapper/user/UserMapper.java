package dev.polproject.hextl.service.mapper.user;

import dev.polproject.hextl.controller.dto.user.SessionUser;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.controller.dto.user.UserIdNameSurnameDto;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.repository.user.UserRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserMapper {

    @Autowired
    private UserRepository userRepository;

    public UserDto toDto(User user){
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .login(user.getLogin())
                .email(user.getEmail())
                .isActive(user.getActive())
                .pathAvatar(user.getEmail())
                .createAt(user.getCreateAt())
                .updateAt(user.getUpdateAt())
                .idRole(user.getRole().getId())
                .build();
    }

    public UserIdNameSurnameDto toUserIdNameSurnameDto(User user){
        return UserIdNameSurnameDto.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .build();
    }

    public SessionUser toSessionUserDto(User user){
        return SessionUser.builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .login(user.getLogin())
                .email(user.getEmail())
                .role(user.getRole().getNameRole())
                .build();
    }

    public User toUserModel(UserDto userDto)  {
        return userRepository.findById(userDto.getId()).orElse(null);
    }
}
