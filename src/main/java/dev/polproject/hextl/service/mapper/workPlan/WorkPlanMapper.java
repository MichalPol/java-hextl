package dev.polproject.hextl.service.mapper.workPlan;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.timetable.workShift.WorkShiftDto;
import dev.polproject.hextl.controller.dto.timetable.workPlan.WorkPlanDto;
import dev.polproject.hextl.model.timetable.WorkPlan;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class WorkPlanMapper {


    private final WorkShiftMapper workShiftMapper;
    private final EmployeeMapper employeeMapper;
    private final UserMapper userMapper;

    public WorkPlanDto toDto(WorkPlan workPlan){

        List<EmployeeIdNameSurnameDto> holidaysEmployees = workPlan.
                getHolidaysEmployee()
                .stream()
                .map(employeeMapper::toEmployeeIdNameSurnameDto)
                .collect(Collectors.toList());

        List<EmployeeIdNameSurnameDto> absenceEmployees = workPlan.getAbsenceEmployee()
                .stream()
                .map(employeeMapper::toEmployeeIdNameSurnameDto)
                .collect(Collectors.toList());

        List<WorkShiftDto> workShiftDtoList = workPlan.getWorkShifts()
                .stream()
                .map(workShiftMapper::toDto)
                .collect(Collectors.toList());


        return WorkPlanDto.builder()
                .id(workPlan.getId())
                .startDay(workPlan.getStartDay())
                .endDay(workPlan.getEndDay())
                .workShifts(workShiftDtoList)
                .holidaysEmployees(holidaysEmployees)
                .infoHolidays(workPlan.getInfoHolidays())
                .absenceEmployees(absenceEmployees)
                .infoAbsence(workPlan.getInfoAbsence())
                .createByIdUser(userMapper.toUserIdNameSurnameDto(workPlan.getCreateBy()))
                .updateByIdUser(userMapper.toUserIdNameSurnameDto(workPlan.getUpdateBy()))
                .updateAt(workPlan.getUpdateAt())
                .build();
    }
}
