package dev.polproject.hextl.service.mapper.workPlan;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.timetable.workLine.WorkLineDto;
import dev.polproject.hextl.controller.dto.timetable.workShift.WorkShiftDto;
import dev.polproject.hextl.model.timetable.WorkShift;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.timetable.WorkLineService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;


@RequiredArgsConstructor
@Component
public class WorkShiftMapper {

    private final EmployeeService employeeService;
    private final WorkLineService workLineService;


    public WorkShiftDto toDto(WorkShift workShift){

        List<EmployeeIdNameSurnameDto> leaderList = employeeService.getIdNameSurnameEmployee(workShift.getShiftsLeader());
        List<EmployeeIdNameSurnameDto> unskilledWorkerList = employeeService.getIdNameSurnameEmployee(workShift.getUnskilledWorker());
        List<EmployeeIdNameSurnameDto> supervisionList = employeeService.getIdNameSurnameEmployee(workShift.getSupervision());
        List<EmployeeIdNameSurnameDto> otherList = employeeService.getIdNameSurnameEmployee(workShift.getOther());
        List<WorkLineDto> workLineList = workLineService.getListWorkLineDto(workShift.getLines());

        return WorkShiftDto.builder()
                .id(workShift.getId())
                .shiftNumber(workShift.getShiftNumber())
                .lines(workLineList)
                .shiftsLeader(leaderList)
                .unskilledWorker(unskilledWorkerList)
                .supervision(supervisionList)
                .other(otherList)
                .comments(workShift.getComments())
                .build();
    }


}
