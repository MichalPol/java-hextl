package dev.polproject.hextl.service.mapper.productivity;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.statisticRaports.*;
import dev.polproject.hextl.controller.dto.line.LineIdNameNumberDto;
import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.mapper.line.LineMapper;
import dev.polproject.hextl.service.mapper.product.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StatisticReportMapper {
    private final LineMapper lineMapper;
    private final ProductMapper productMapper;
    private final EmployeeMapper employeeMapper;


    public DataReport toReportGeneratorData(ProductionReport report){
        LineIdNameNumberDto lineIdNameNumberDto = lineMapper.toLineIdNameNumberDto(report.getLine());
        ProductIdNameSerializedDto productIdNameSerializedDto = productMapper.toProductIdNameSerializedDto(report.getProduct());
        EmployeeIdNameSurnameDto firstEmployee = employeeMapper.toEmployeeIdNameSurnameDto(report.getFirstWorkplace());
        EmployeeIdNameSurnameDto secondEmployee = employeeMapper.toEmployeeIdNameSurnameDto(report.getSecondWorkplace());
        EmployeeIdNameSurnameDto thirdEmployee = employeeMapper.toEmployeeIdNameSurnameDto(report.getThirdWorkplace());
        return DataReport.builder()
                .idProductionReport(report.getId())
                .startProduction(report.getProductionStart())
                .endProduction(report.getProductionEnd())
                .line(lineIdNameNumberDto)
                .productionTimeToHour(report.getProductionTimeToHour())
                .product(productIdNameSerializedDto)
                .series(report.getSeries())
                .firstWorkplace(firstEmployee)
                .secondWorkplace(secondEmployee)
                .thirdWorkplace(thirdEmployee)
                .speedMachinePerCycle(report.getSpeedMachinePerCycle())
                .totalQuantityProduced(report.getTotalQuantityProduced())
                .maxPossibleItems(report.getMaxPossibleItems())
                .percentagePerformance(report.getPercentagePerformance())
                .performancePerHour(report.getPerformancePerHour())
                .build();
    }

    public StatisticReportCircleChart toStatisticReportCircleChart(StatisticReport report){
        return StatisticReportCircleChart.builder()
                .name(report.getName())
                .starRange(report.getStarRange())
                .endRange(report.getEndRange())
                .options(report.getOptions())
                .build();

    }

    public CreateNewReport toCreateNewReport(CreateReportEmployee createReportEmployee){
        return CreateNewReport.builder()
                .idItems(createReportEmployee.getIdItems())
                .start(createReportEmployee.getStart())
                .end(createReportEmployee.getEnd())
                .type(createReportEmployee.getType())
                .options(createReportEmployee.getOptions())
                .build();

    }
}
