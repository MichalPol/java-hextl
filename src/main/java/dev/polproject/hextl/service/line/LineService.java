package dev.polproject.hextl.service.line;

import dev.polproject.hextl.controller.dto.line.CreateLineDto;
import dev.polproject.hextl.controller.dto.line.LineDto;
import dev.polproject.hextl.controller.dto.line.LineProductionReportsDto;
import dev.polproject.hextl.controller.dto.line.UpdateLineDto;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.repository.line.LineRepository;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.line.LineMapper;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportAdditionRemoval;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class LineService implements ProductionReportAdditionRemoval {

    private final LineRepository lineRepository;
    private final LineMapper lineMapper;


    public List<LineDto> getAllLine(Boolean filterIsActive) {
        return lineRepository.findAll()
                .stream()
                .filter(line -> line.getActive() == filterIsActive)
                .map(lineMapper::toLineDto)
                .collect(Collectors.toList());
    }

    public List<Line> getAllLineModel() {
        return lineRepository.findAll();
    }

    public LineDto getLineDto(Long idLine, Boolean filterIsActive) throws NotFound {
        return lineRepository.findById(idLine)
                .filter(line -> line.getActive().equals(filterIsActive))
                .map(lineMapper::toLineDto)
                .orElseThrow(() -> new NotFound("Not found line id: " + idLine));
    }

    public Line getLineModel(Long idLine) throws NotFound {
        return lineRepository
                .findById(idLine)
                .orElseThrow(() -> new NotFound("Not found line id: " + idLine));
    }

    public Line getLineModelByIdFilterActive(Long idLine, Boolean activeFilter) throws NotFound {
        return lineRepository
                .findById(idLine)
                .filter(line -> line.getActive().equals(activeFilter))
                .orElseThrow(() -> new NotFound("Not found line id: " + idLine));
    }

    @Transactional
    public LineDto createLine(CreateLineDto createLineDto) throws Conflict {
        Optional<Line> numberLine = lineRepository.findByNumberLine(createLineDto.getNumberLine());
        if (numberLine.isPresent()) {
            log.warn("Conflict number line " + numberLine.get().getNumberLine());
            throw new Conflict("Error - Exist line about number: " + numberLine.get().getNumberLine());
        }

        Optional<Line> nameLine = lineRepository.findByName(createLineDto.getName());
        if (nameLine.isPresent()) {
            log.warn("Conflict name line" + nameLine.get().getName());
            throw new Conflict("Error - Exist name line: " + nameLine.get().getName());
        }
        Line newLine = Line.builder()
                .id(null)
                .name(createLineDto.getName())
                .numberLine(createLineDto.getNumberLine())
                .isActive(true)
                .build();
        Line save = lineRepository.save(newLine);
        log.info("A new production line was created with id " + save.getId());
        return lineMapper.toLineDto(save);

    }

    @Transactional
    public LineDto updateLine(Long idLine, UpdateLineDto updateLineDto) throws NotFound {

        Line updateLine = getLineModelByIdFilterActive(idLine,true);
            updateLine.setName(updateLineDto.getName());
            Line save = lineRepository.save(updateLine);
            log.info("production line update with id " + save.getId());
            return lineMapper.toLineDto(save);

    }

    @Transactional
    public LineDto deleteLine(Long idLine) throws NotFound {
        Line lineModel = getLineModel(idLine);
        lineModel.setActive(false);

        Line save = lineRepository.save(lineModel);
        log.info("deactivation of the production line with id " + save.getId());
        return lineMapper.toLineDto(save);
    }

    @Transactional
    @Override
    public void addProductionReport(Long id, ProductionReport productionReport) throws NotFound {
        Line line = getLineModel(id);
        line.getLineProductionReports().add(productionReport);
        Line save = lineRepository.save(line);
        log.info("a production report has been added to the line with id: " + save.getId());
    }

    @Transactional
    @Override
    public void removeProductionReport(Long id, ProductionReport productionReport) throws NotFound {
        Line line = getLineModel(id);
        line.getLineProductionReports().remove(productionReport);
        Line save = lineRepository.save(line);
        log.info("the production ID report has been removed from the line " + save.getId());
    }

    public LineProductionReportsDto getAllProductionReportsFromLine(Long idLine) throws NotFound {
        Line line = lineRepository
                .findById(idLine)
                .orElseThrow(() -> new NotFound("Not Found id " + idLine) );
        return lineMapper.toLineProductionReportsDto(line);
    }
}
