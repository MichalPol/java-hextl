package dev.polproject.hextl.service.employee;

import dev.polproject.hextl.controller.dto.employee.CreateUpdateEmployeeDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.employee.EmployeeProductionReportsDto;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.repository.employee.EmployeeRepository;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportAdditionRemoval;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeService implements ProductionReportAdditionRemoval {
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;


    public List<EmployeeDto> getAllEmployeeDto(Boolean filterIsActive) {
        return employeeRepository
                .findAll()
                .stream()
                .filter(employee -> employee.getActive() == filterIsActive)
                .map(employeeMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<Employee> getAllEmployeeModel() {
        return employeeRepository.findAll();
    }

    public List<Employee> getAllEmployeeWithId(List<Long> listIdEmployee) throws NotFound {

        List<Employee> employees = new ArrayList<>();
        for (Long employee : listIdEmployee) {
            Employee employeeModelById = getEmployeeModel(employee);
            employees.add(employeeModelById);
        }
        return employees;
    }

    public EmployeeDto getEmployeeDtoByIdFilterActive(Long idEmployee, Boolean filterIsActive) throws NotFound {
        return employeeRepository
                .findById(idEmployee)
                .filter(employee -> employee.getActive() == filterIsActive)
                .map(employeeMapper::toDto)
                .orElseThrow(() -> new NotFound(idEmployee));
    }

    public EmployeeDto getEmployeeDtoByIdNoFilterActive(Long idEmployee) throws NotFound {
        return employeeRepository
                .findById(idEmployee)
                .map(employeeMapper::toDto)
                .orElseThrow(() -> new NotFound(idEmployee));
    }

    public Employee getEmployeeModel(Long idEmployee) throws NotFound {
        return employeeRepository.findById(idEmployee)
                .orElseThrow(() -> new NotFound(idEmployee));
    }

    public Employee getEmployeeModelByIdUseFilterActive(Long idEmployee, Boolean filterIsActive) throws NotFound {
        return employeeRepository.findById(idEmployee)
                .filter(employee -> employee.getActive() == filterIsActive)
                .orElseThrow(() -> new NotFound(idEmployee));
    }

    @Transactional
    public EmployeeDto createEmployee(CreateUpdateEmployeeDto createEmployee) throws Conflict {

        Employee employee = Employee.builder()
                .id(null)
                .name(createEmployee.getName())
                .lastName(createEmployee.getLastName())
                .isActive(true)
                .information(createEmployee.getInformation())
                .createAt(OffsetDateTime.now())
                .build();

        List<Employee> employeeList = getAllEmployeeModel()
                .stream()
                .filter(e -> e.getName().equals(employee.getName()))
                .filter(e -> e.getLastName().equals(employee.getLastName()))
                .collect(Collectors.toList());

        if (employeeList.size() != 0) {
            log.warn("Such an employee already exists!: " + createEmployee.getName() + " " + createEmployee.getLastName());
            throw new Conflict("Such an employee already exists!");
        }
        Employee save = employeeRepository.save(employee);
        log.info("Create new employee " + save.getName() + " " + save.getLastName());
        return employeeMapper.toDto(save);
    }

    @Transactional
    public EmployeeDto updateEmployee(Long idEmployee, CreateUpdateEmployeeDto updateEmployee) throws NotFound, Conflict {

        Employee employee = getEmployeeModelByIdUseFilterActive(idEmployee, true);
        Optional<Employee> findEmployeeFromRepository = getAllEmployeeModel().stream()
                .filter(emp -> emp.hashCode() == updateEmployee.hashCode())
                .findFirst();

        if (findEmployeeFromRepository.isPresent()) {
            if (findEmployeeFromRepository.get().getId() != idEmployee) {
                throw new Conflict("Another such employee already exists");
            }
        }
        employee.setName(updateEmployee.getName());
        employee.setLastName(updateEmployee.getLastName());
        employee.setInformation(updateEmployee.getInformation());
        employee.setUpdateAt(OffsetDateTime.now());
        Employee save = employeeRepository.save(employee);
        log.info("Edit employee " + save.getId());
        return employeeMapper.toDto(save);
    }

    @Transactional
    public EmployeeDto removeEmployee(Long idEmployee) throws NotFound {
        Employee employee = getEmployeeModel(idEmployee);
        employee.setActive(false);
        employee.setUpdateAt(OffsetDateTime.now());

        Employee save = employeeRepository.save(employee);
        return employeeMapper.toDto(save);
    }

    public List<EmployeeIdNameSurnameDto> getIdNameSurnameEmployee(List<Employee> employeeList) {
        return employeeList
                .stream()
                .map(employeeMapper::toEmployeeIdNameSurnameDto)
                .collect(Collectors.toList());
    }

    public EmployeeProductionReportsDto getAllProductionReportsFromEmployee(Long idEmployee) throws NotFound {
        Employee employee = employeeRepository
                .findById(idEmployee)
                .orElseThrow(() -> new NotFound("Not Found id: " + idEmployee));
        return employeeMapper.toEmployeeProductionReportsDto(employee);
    }

    @Transactional
    @Override
    public void addProductionReport(Long id, ProductionReport productionReport) throws NotFound {
        Employee employee = getEmployeeModel(id);
        List<ProductionReport> report = employee.getEmployeeProductionReport();
        report.add(productionReport);
        employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public void removeProductionReport(Long id, ProductionReport productionReport) throws NotFound {
        Employee employee = getEmployeeModel(id);
        List<ProductionReport> employeeProductionReport = employee.getEmployeeProductionReport();
        employeeProductionReport.remove(productionReport);
        employeeRepository.save(employee);
    }
}
