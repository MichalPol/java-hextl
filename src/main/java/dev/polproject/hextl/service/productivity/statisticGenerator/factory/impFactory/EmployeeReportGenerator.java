package dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory;

import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.ReportGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@RequiredArgsConstructor
public class EmployeeReportGenerator extends ReportGenerator {

    private final EmployeeService employeeService;

    @Override
    public List<ProductionReport> generateReport(CreateNewReport newReport) throws NotFound {

        List<ProductionReport> productProductionReport = new ArrayList<>();

        for (int i = 0; i < newReport.getIdItems().size(); i++) {
            Employee employee = employeeService.getEmployeeModel(newReport.getIdItems().get(i));
            productProductionReport.addAll(employee.getEmployeeProductionReport());
        }

        return productProductionReport;


    }
}
