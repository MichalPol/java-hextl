package dev.polproject.hextl.service.productivity.productionReport;

import dev.polproject.hextl.service.exeption.OutRange;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class PerformanceCalculationsService {

    public double getProductionTimeInHours(@NotNull Integer hour, @NotNull Integer minutes) throws OutRange {

        if (hour < 0) {
            throw new OutRange("the hour cannot be less than zero");
        }

        if (minutes < 0 || minutes > 60) {
            throw new OutRange("minutes must not be less than zero and greater than 60");
        }

        double minutesToHours = 1. * minutes / 60;
        double result = minutesToHours + hour;
        result = (double) Math.round(result * 100) / 100;
        return result;
    }

    public int getMaxPossibleItems(@NotNull Integer itemsPerCycle, @NotNull Integer speedMachinePerCycle, @NotNull double productionTimeInHours) throws OutRange {

        if (itemsPerCycle < 2 || itemsPerCycle > 16) {
            throw new OutRange("Machine cycle out the range 10 - 16");
        }

        if (speedMachinePerCycle <= 4 || speedMachinePerCycle >= 51) {
            throw new OutRange("Machine speed out the range 5 - 50");
        }

        productionTimeInHours *= 60;
        double result = itemsPerCycle * speedMachinePerCycle * productionTimeInHours * 10;
        result = Math.round(result);
        result /= 10;
        return (int) result;
    }

    public double getPerformancePerHour(@NotNull Integer totalQuantityProduced, @NotNull double productionTimeInHours) throws OutRange {
        if (totalQuantityProduced < 0 || productionTimeInHours < 0.01) {
            throw new OutRange("The range is bad, it must be greater than 0");
        }

        productionTimeInHours *= 60;
        double result = totalQuantityProduced / productionTimeInHours * 60;
        result *= 100;
        result = Math.round(result);
        result /= 100;
        return result;
    }

    public double getPercentagePerformance(@NotNull Integer totalQuantityProduced, @NotNull double maxPossibleItems) throws OutRange {
        if (totalQuantityProduced < 0 || maxPossibleItems < 0) {
            throw new OutRange("The range is bad, it must be greater than 0");
        }

        double result = totalQuantityProduced / maxPossibleItems;
        result *= 1000;
        result = (double) Math.round(result) / 10;
        return result;

    }
}
