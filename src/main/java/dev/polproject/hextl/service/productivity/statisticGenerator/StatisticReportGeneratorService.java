package dev.polproject.hextl.service.productivity.statisticGenerator;

import dev.polproject.hextl.controller.dto.employee.EmployeeIdNameSurnameDto;
import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.controller.dto.statisticRaports.*;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.mapper.productivity.StatisticReportMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class StatisticReportGeneratorService {

    private final ReportGeneratorFactory reportGeneratorFactory;
    private final StatisticReportMapper statisticReportMapper;

    public StatisticReport getReportGenerator(CreateNewReport createNewReport) throws DataInvalid, NotFound, OutRange {

        if (createNewReport.getIdItems() == null || createNewReport.getIdItems().size() == 0){
            throw new NotFound("Not found id");
        }

        return reportGeneratorFactory.creatReportGenerator(createNewReport.getType()).getReport(createNewReport);

    }


    public StatisticReportCircleChart getReportGeneratorOnlyStatistic(CreateNewReport createNewReport) throws NotFound, DataInvalid, OutRange {
        if (createNewReport.getIdItems() == null || createNewReport.getIdItems().size() == 0){
            throw new NotFound("Not found id");
        }
        return statisticReportMapper
                .toStatisticReportCircleChart(
                        reportGeneratorFactory.creatReportGenerator(createNewReport.getType()).getReport(createNewReport));
    }

    public Set<ProductIdNameSerializedDto> getProductsOfReportGenerator(CreateNewReport createNewReport) throws OutRange, DataInvalid, NotFound {
        return getReportGenerator(createNewReport)
                .getDataReport()
                .stream()
                .map(DataReport::getProduct)
                .collect(Collectors.toSet());
    }

    public Set<EmployeeIdNameSurnameDto> getEmployeeOfReportProduction(CreateReportEmployee createReportEmployee) throws OutRange, DataInvalid, NotFound {

        CreateNewReport createNewReport = statisticReportMapper.toCreateNewReport(createReportEmployee);
        Set<EmployeeIdNameSurnameDto> employees = new HashSet<>();

        if (null == createReportEmployee.getFirstWorkplaceFilter()){
            createReportEmployee.setFirstWorkplaceFilter(true);
        }

        if (null == createReportEmployee.getSecondWorkplaceFilter()){
            createReportEmployee.setSecondWorkplaceFilter(true);
        }

        if (null == createReportEmployee.getThirdWorkplaceFilter()){
            createReportEmployee.setThirdWorkplaceFilter(true);
        }

        if (createReportEmployee.getFirstWorkplaceFilter()){
            Set<EmployeeIdNameSurnameDto> collect = getReportGenerator(createNewReport)
                    .getDataReport()
                    .stream()
                    .map(DataReport::getFirstWorkplace)
                    .collect(Collectors.toSet());
            employees.addAll(collect);
        }

        if (createReportEmployee.getSecondWorkplaceFilter()){
            Set<EmployeeIdNameSurnameDto> collect = getReportGenerator(createNewReport)
                    .getDataReport()
                    .stream()
                    .map(DataReport::getSecondWorkplace)
                    .collect(Collectors.toSet());
            employees.addAll(collect);
        }

        if (createReportEmployee.getThirdWorkplaceFilter()){
            Set<EmployeeIdNameSurnameDto> collect = getReportGenerator(createNewReport)
                    .getDataReport()
                    .stream()
                    .map(DataReport::getThirdWorkplace)
                    .collect(Collectors.toSet());
            employees.addAll(collect);
        }
        return employees;
    }
}
