package dev.polproject.hextl.service.productivity.statisticGenerator;

import dev.polproject.hextl.model.productivity.productionReport.ReportType;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.ReportGenerator;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory.EmployeeReportGenerator;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory.LineReportGenerator;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory.ProductReportGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ReportGeneratorFactory {
    private final LineReportGenerator lineReportGenerator;
    private final EmployeeReportGenerator employeeReportGenerator;
    private final ProductReportGenerator productReportGenerator;

    public ReportGenerator creatReportGenerator(ReportType reportType) throws DataInvalid {

        switch (reportType) {
            case LINE: {
                return lineReportGenerator;
            }
            case EMPLOYEE: {
                return employeeReportGenerator;
            }
            case PRODUCT: {
                return productReportGenerator;
            }

            default: {
                throw new DataInvalid("Not Found Type");
            }
        }
    }
}
