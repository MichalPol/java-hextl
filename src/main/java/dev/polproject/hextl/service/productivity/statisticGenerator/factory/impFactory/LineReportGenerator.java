package dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory;

import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.ReportGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@RequiredArgsConstructor
public class LineReportGenerator extends ReportGenerator {

    private final LineService lineService;

    @Override
    protected List<ProductionReport> generateReport(CreateNewReport newReport) throws NotFound {

        List<ProductionReport> lineProductionReports = new ArrayList<>();
        for (int i = 0; i < newReport.getIdItems().size(); i++) {
            Line line = lineService.getLineModel(newReport.getIdItems().get(i));
            lineProductionReports.addAll(line.getLineProductionReports());
        }
        return lineProductionReports;
    }
}
