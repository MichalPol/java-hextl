package dev.polproject.hextl.service.productivity.productionReport;

import dev.polproject.hextl.controller.dto.product.ProductIdNameSerializedDto;
import dev.polproject.hextl.controller.dto.productionReport.CreateProductionReportDto;
import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.controller.dto.productionReport.UpdateProductionReportDto;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.repository.productivity.ProductionReportRepository;
import dev.polproject.hextl.service.app.DateAppFormatter;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.mapper.product.ProductMapper;
import dev.polproject.hextl.service.mapper.productivity.ProductionReportMapper;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import dev.polproject.hextl.service.product.ProductService;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
@RequiredArgsConstructor
@Service
public class ProductionReportService {

    private final ProductionReportRepository productionReportRepository;
    private final ProductionReportMapper productionReportMapper;
    private final LineService lineService;
    private final ProductService productService;
    private final ProductMapper productMapper;
    private final EmployeeService employeeService;
    private final PerformanceCalculationsService performanceCalculationsService;
    private final UserService userService;
    private final AuditLogService auditLogService;



    public List<ProductionReportDto> getAllProductReportDto() {
        return productionReportRepository
                .findAll()
                .stream()
                .map(productionReportMapper::toProductionReportDto)
                .collect(Collectors.toList());
    }

    public ProductionReportDto getProductionReportDto(Long idProductionReport) throws NotFound {
        return productionReportRepository
                .findById(idProductionReport)
                .map(productionReportMapper::toProductionReportDto)
                .orElseThrow(() -> new NotFound("Not found ProductionReport with id: " + idProductionReport));
    }

    public ProductionReport getProductionReportModel(Long idProductionReport) throws NotFound {
        return productionReportRepository
                .findById(idProductionReport)
                .orElseThrow(() -> new NotFound("Not found ProductionReport with id: " + idProductionReport));
    }

    public List<ProductionReportDto> getProductionReportDtoByBetweenTime(OffsetDateTime start, OffsetDateTime end) {

        return productionReportRepository.findAllByProductionEndBetween(start, end)
                .stream()
                .map(productionReportMapper::toProductionReportDto)
                .collect(Collectors.toList());

    }


    //todo do ustalenia czy front da mi id czy mam brac z sesji

    @Transactional
    public ProductionReportDto createProductionReport(@Valid CreateProductionReportDto createProductionReportDto, User createdByUser) throws NotFound, DataInvalid, OutRange, Conflict {


        OffsetDateTime startOffsetDateTime = DateAppFormatter.parseStringToOffsetDateTimeFormat(createProductionReportDto.getProductionStart());
        OffsetDateTime endOffsetDateTime = DateAppFormatter.parseStringToOffsetDateTimeFormat(createProductionReportDto.getProductionEnd());
        Line line = lineService.getLineModel(createProductionReportDto.getLineId());

        if (startOffsetDateTime.isAfter(endOffsetDateTime)) {
            auditLogService.action(ControlCode.ERROR_DATA_INVALID, "Start production is after End Production", createdByUser.getId());
            throw new DataInvalid("Start production is after End Production");
        }

        List<ProductionReport> reportFound = productionReportRepository.findAllByLineAndProductionStartAfterAndProductionEndBefore(line, startOffsetDateTime, endOffsetDateTime);

        System.out.println(reportFound.size());
        if (reportFound.size() != 0) {
            auditLogService.action(ControlCode.ERROR_CONFLICT, "Exists Production Report", createdByUser.getId());
            throw new Conflict("Exists Production Report");
        }

        if (createdByUser == null) {
            auditLogService.action(ControlCode.ERROR_DATA_INVALID, "User id is required", createdByUser.getId());
            throw new DataInvalid("User id is required");
        }
        Product product = productService.getProductModel(createProductionReportDto.getProductId());
        Employee firstEmployee = employeeService.getEmployeeModelByIdUseFilterActive(createProductionReportDto.getFirstWorkplaceIdEmployee(), true);
        Employee secondEmployee = employeeService.getEmployeeModelByIdUseFilterActive(createProductionReportDto.getSecondWorkplaceIdEmployee(), true);
        Employee thirdEmployee = employeeService.getEmployeeModelByIdUseFilterActive(createProductionReportDto.getThirdWorkplaceIdEmployee(), true);

        double productionTimeToHour = performanceCalculationsService
                .getProductionTimeInHours(
                        createProductionReportDto.getProductionHours(),
                        createProductionReportDto.getProductionMinutes());

        Integer speedMachinePerCycle = createProductionReportDto.getSpeedMachinePerMinute();
        Double percentagePerformance;
        int maxPossibleItems;
        Double performancePerHour;

        //preventing fraud consisting in lowering the production speed
        //we increase the speed until it drops below 100% of the machine's efficiency
        do {
            maxPossibleItems = performanceCalculationsService.getMaxPossibleItems(product.getItemsPerCycle(), speedMachinePerCycle, productionTimeToHour);

            percentagePerformance = performanceCalculationsService
                    .getPercentagePerformance(
                            createProductionReportDto.getTotalQuantityProduced(), maxPossibleItems);

            if (percentagePerformance > 100.0) {
                speedMachinePerCycle += 5;
                log.info(percentagePerformance + " jest powyżej 100%");
            }
        } while (percentagePerformance > 100.0);

        performancePerHour = performanceCalculationsService
                .getPerformancePerHour(createProductionReportDto.getTotalQuantityProduced(),
                        productionTimeToHour);

        ProductionReport productionReport = ProductionReport.builder()
                .id(null)
                .line(line)
                .productionStart(startOffsetDateTime)
                .productionEnd(endOffsetDateTime)
                .productionTimeToHour(productionTimeToHour)
                .product(product)
                .series(createProductionReportDto.getSeries())
                .firstWorkplace(firstEmployee)
                .secondWorkplace(secondEmployee)
                .thirdWorkplace(thirdEmployee)
                .speedMachinePerCycle(speedMachinePerCycle)
                .totalQuantityProduced(createProductionReportDto.getTotalQuantityProduced())
                .maxPossibleItems(maxPossibleItems)
                .performancePerHour(performancePerHour)
                .percentagePerformance(percentagePerformance)
                .description(createProductionReportDto.getDescription())
                .createAt(OffsetDateTime.now())
                .updateAt(OffsetDateTime.now())
                .createdByUser(createdByUser)
                .updatedByUser(createdByUser)
                .build();

        ProductionReport saveProductionReport = productionReportRepository.save(productionReport);
        lineService.addProductionReport(line.getId(), saveProductionReport);
        employeeService.addProductionReport(firstEmployee.getId(), saveProductionReport);
        employeeService.addProductionReport(secondEmployee.getId(), saveProductionReport);
        employeeService.addProductionReport(thirdEmployee.getId(), saveProductionReport);
        productService.addProductionReport(product.getId(), saveProductionReport);

        return productionReportMapper.toProductionReportDto(saveProductionReport);
    }

    @Transactional
    public ProductionReportDto updateProductionReport(Long idProductionReport, UpdateProductionReportDto updateProductionReportDto, User userUpdate) throws NotFound, DataInvalid {

        ProductionReport reportUpdate = getProductionReportModel(idProductionReport);

        Line lineUpdate = lineService.getLineModel(updateProductionReportDto.getLineId());
        OffsetDateTime startDateTime = DateAppFormatter.parseStringToOffsetDateTimeFormat(updateProductionReportDto.getProductionStart());
        OffsetDateTime endDateTime = DateAppFormatter.parseStringToOffsetDateTimeFormat(updateProductionReportDto.getProductionEnd());
        Product product = productService.getProductModel(updateProductionReportDto.getProductId());

        Employee firstEmployee = employeeService.getEmployeeModel(updateProductionReportDto.getFirstWorkplaceIdEmployee());
        Employee secondEmployee = employeeService.getEmployeeModel(updateProductionReportDto.getSecondWorkplaceIdEmployee());
        Employee thirdEmployee = employeeService.getEmployeeModel(updateProductionReportDto.getThirdWorkplaceIdEmployee());

        reportUpdate.setLine(lineUpdate);
        reportUpdate.setProductionStart(startDateTime);
        reportUpdate.setProductionEnd(endDateTime);
        reportUpdate.setProductionTimeToHour(updateProductionReportDto.getProductionTimeToHour());
        reportUpdate.setProduct(product);
        reportUpdate.setSeries(updateProductionReportDto.getSeries());
        reportUpdate.setFirstWorkplace(firstEmployee);
        reportUpdate.setSecondWorkplace(secondEmployee);
        reportUpdate.setThirdWorkplace(thirdEmployee);
        reportUpdate.setSpeedMachinePerCycle(updateProductionReportDto.getSpeedMachinePerCycle());
        reportUpdate.setTotalQuantityProduced(updateProductionReportDto.getTotalQuantityProduced());
        reportUpdate.setPercentagePerformance(updateProductionReportDto.getPercentagePerformance());
        reportUpdate.setPercentagePerformance(updateProductionReportDto.getPercentagePerformance());
        reportUpdate.setDescription(updateProductionReportDto.getDescription());
        reportUpdate.setUpdateAt(OffsetDateTime.now());
        reportUpdate.setUpdatedByUser(userUpdate);
        ProductionReport save = productionReportRepository.save(reportUpdate);

        return productionReportMapper.toProductionReportDto(save);
    }

    @Transactional
    public ProductionReportDto deleteProductionReport(Long idProductionReport) throws NotFound {
        ProductionReport productionReport = getProductionReportModel(idProductionReport);

        lineService.removeProductionReport(productionReport.getLine().getId(), productionReport);
        employeeService.removeProductionReport(productionReport.getFirstWorkplace().getId(), productionReport);
        employeeService.removeProductionReport(productionReport.getSecondWorkplace().getId(), productionReport);
        employeeService.removeProductionReport(productionReport.getThirdWorkplace().getId(), productionReport);
        productService.removeProductionReport(productionReport.getProduct().getId(), productionReport);
        productionReportRepository.delete(productionReport);
        return productionReportMapper.toProductionReportDto(productionReport);
    }

    public Set<ProductIdNameSerializedDto> getProductFromProductionReport(OffsetDateTime startDate, OffsetDateTime endDate) throws NotFound {

        return productionReportRepository.findAllByProductionEndBetween(startDate, endDate)
                .stream()
                .map(productionReport -> productMapper.toProductIdNameSerializedDto(productionReport.getProduct()))
                .collect(Collectors.toSet());
    }
}
