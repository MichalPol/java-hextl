package dev.polproject.hextl.service.productivity.statisticGenerator.factory;

import dev.polproject.hextl.controller.dto.statisticRaports.ParseReport;
import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.mapper.employee.EmployeeMapper;
import dev.polproject.hextl.service.mapper.productivity.StatisticReportMapper;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static dev.polproject.hextl.model.productivity.productionReport.StatisticalOptionType.*;
import static dev.polproject.hextl.model.productivity.productionReport.StatisticalOptionType.TOTAL_PRODUCED;

@Service
@RequiredArgsConstructor
public class ParseReportGenerator {

    private final LineService lineService;
    private final ProductService productService;
    private final EmployeeService employeeService;
    private final StatisticReportMapper statisticReportMapper;
    private final OptionGenerator optionGenerator;
    private final EmployeeMapper employeeMapper;

    public ParseReport parseOptions(Map<String, Object> optionsParse, List<ProductionReport> productionReports) throws NotFound, OutRange {

        Map<String, Object> options = new HashMap<>();
        List<ProductionReport> reports = productionReports;

        if (optionsParse.get(PRODUCT.getType()) != null) {
            Long productId = (long) (Integer) optionsParse.get(PRODUCT.getType());
            Product product = productService.getProductModel(productId);
            reports = optionGenerator.setProductFilter(product, reports);
            options.put(PRODUCT.getType(), product.getName());
        }

        if (optionsParse.get(LINE.getType()) != null) {
            Long lineId = (long) (Integer) optionsParse.get(LINE.getType());
            Line line = lineService.getLineModel(lineId);
            reports = optionGenerator.setLineFilter(line, reports);
            options.put(LINE.getType(), line.getName());
        }

        if (optionsParse.get(FIRST_WORKPLACE.getType()) != null) {
            Employee firstWorkplace = employeeService.getEmployeeModel((long) (Integer) optionsParse.get(FIRST_WORKPLACE.getType()));
            reports = optionGenerator.setFirstWorkplaceFilter(firstWorkplace, reports);
            options.put(FIRST_WORKPLACE.getType(), employeeMapper.toEmployeeIdNameSurnameDto(firstWorkplace));
        }

        if (optionsParse.get(SECOND_WORKPLACE.getType()) != null) {
            Employee secondWorkplace = employeeService.getEmployeeModel((long) (Integer) optionsParse.get(SECOND_WORKPLACE.getType()));
            reports = optionGenerator.setSecondWorkplaceFilter(secondWorkplace, reports);
            options.put(SECOND_WORKPLACE.getType(), employeeMapper.toEmployeeIdNameSurnameDto(secondWorkplace));
        }

        if (optionsParse.get(THIRD_WORKPLACE.getType()) != null) {
            Employee thirdWorkplace = employeeService.getEmployeeModel((long) (Integer) optionsParse.get(THIRD_WORKPLACE.getType()));
            reports = optionGenerator.setThirdWorkplaceFilter(thirdWorkplace, reports);
            options.put(THIRD_WORKPLACE.getType(), employeeMapper.toEmployeeIdNameSurnameDto(thirdWorkplace));
        }

        if (optionsParse.get(SERIES.getType()) != null) {
            String series = optionsParse.get(SERIES.getType()).toString();
            reports = optionGenerator.setSeriesFilter(series, reports);
            options.put(SERIES.getType(), series);
        }

        if (optionsParse.get(AVENGER_SPEED.getType()) != null && (boolean) optionsParse.get(AVENGER_SPEED.getType())) {
            Double averageSpeed = optionGenerator.setAverageSpeed(reports);
            options.put(AVENGER_SPEED.getType(), averageSpeed);
        }

        if (optionsParse.get(AVENGER_PER_HOUR.getType()) != null && (boolean) optionsParse.get(AVENGER_PER_HOUR.getType())) {
            Double avengerPerHour = optionGenerator.setAveragePerHour(reports);
            options.put(AVENGER_PER_HOUR.getType(), avengerPerHour);
        }

        if (optionsParse.get(PERCENTAGE.getType()) != null && (boolean) optionsParse.get(PERCENTAGE.getType())) {
            Double percentage = optionGenerator.setPercentage(reports);
            options.put(PERCENTAGE.getType(), percentage);
        }

        if (optionsParse.get(TOTAL_PRODUCED.getType()) != null && (boolean) optionsParse.get(TOTAL_PRODUCED.getType())) {
            Integer sumProduced = optionGenerator.setTotalProduced(reports);
            options.put(TOTAL_PRODUCED.getType(), sumProduced);
        }

        return new ParseReport(options, reports);
    }

}
