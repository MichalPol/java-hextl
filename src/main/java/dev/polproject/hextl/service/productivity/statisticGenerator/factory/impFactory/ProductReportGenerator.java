package dev.polproject.hextl.service.productivity.statisticGenerator.factory.impFactory;

import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.product.ProductService;
import dev.polproject.hextl.service.productivity.statisticGenerator.factory.ReportGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductReportGenerator extends ReportGenerator {
    private final ProductService productService;

    @Override
    public List<ProductionReport> generateReport(CreateNewReport newReport) throws NotFound {

        List<ProductionReport> productProductionReport = new ArrayList<>();

        for (int i = 0; i < newReport.getIdItems().size(); i++) {
            Product product = productService.getProductModel(newReport.getIdItems().get(i));
            productProductionReport.addAll(product.getProductProductionReports());
        }
        return productProductionReport;
    }
}
