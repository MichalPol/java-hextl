package dev.polproject.hextl.service.productivity.statisticGenerator.factory;

import dev.polproject.hextl.controller.dto.statisticRaports.CreateNewReport;
import dev.polproject.hextl.controller.dto.statisticRaports.ParseReport;
import dev.polproject.hextl.controller.dto.statisticRaports.DataReport;
import dev.polproject.hextl.controller.dto.statisticRaports.StatisticReport;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.app.DateAppFormatter;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.mapper.productivity.StatisticReportMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;


public abstract class ReportGenerator {

    @Autowired
    private StatisticReportMapper statisticReportMapper;
    @Autowired
    private OptionGenerator optionGenerator;
    @Autowired
    private  ParseReportGenerator parseReportGenerator;

    public StatisticReport getReport(CreateNewReport createNewReport) throws NotFound, DataInvalid, OutRange {
        StatisticReport report = new StatisticReport();
        OffsetDateTime start = DateAppFormatter.parseStringToOffsetDateTimeFormat(createNewReport.getStart());
        OffsetDateTime end = DateAppFormatter.parseStringToOffsetDateTimeFormat(createNewReport.getEnd());

        report.setName(createNewReport.getType().name());
        report.setStarRange(start);
        report.setEndRange(end);

        List<ProductionReport> productionReports = optionGenerator.setTimePeriod(
                start,
                end,
                generateReport(createNewReport));

        ParseReport reportOptions = parseReportGenerator.parseOptions(createNewReport.getOptions(), productionReports);

        List<DataReport> generatorTables = reportOptions.getProductionReports().stream()
                .map(statisticReportMapper::toReportGeneratorData)
                .collect(Collectors.toList());

        report.setDataReport(generatorTables);
        report.setOptions(reportOptions.getOptions());
        return report;
    }
     protected abstract List<ProductionReport> generateReport(CreateNewReport createNewReport) throws NotFound;

}
