package dev.polproject.hextl.service.productivity.productionReport;


import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.service.exeption.NotFound;


public interface ProductionReportAdditionRemoval {

    void addProductionReport(Long id, ProductionReport productionReport) throws NotFound;
    void removeProductionReport(Long id, ProductionReport productionReport) throws NotFound;
}
