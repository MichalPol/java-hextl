package dev.polproject.hextl.service.productivity.statisticGenerator.factory;

import dev.polproject.hextl.model.employee.Employee;
import dev.polproject.hextl.model.line.Line;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.productivity.productionReport.PerformanceCalculationsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class OptionGenerator {

    private final PerformanceCalculationsService performanceCalculationsService;

    public List<ProductionReport> setTimePeriod(OffsetDateTime start, OffsetDateTime end, List<ProductionReport> productionReports) {
        return productionReports
                .stream()
                .filter(pr -> pr.getProductionStart().isAfter(start))
                .filter(productionReport -> productionReport.getProductionEnd().isBefore(end))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setProductFilter(Product product, List<ProductionReport> productionReports) throws NotFound {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getProduct().equals(product))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setLineFilter(Line line, List<ProductionReport> productionReports) throws NotFound {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getLine().equals(line))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setSeriesFilter(String series, List<ProductionReport> productionReports) {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getSeries().equals(series))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setFirstWorkplaceFilter(Employee employee, List<ProductionReport> productionReports) {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getFirstWorkplace().equals(employee))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setSecondWorkplaceFilter(Employee employee, List<ProductionReport> productionReports) throws NotFound {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getSecondWorkplace().equals(employee))
                .collect(Collectors.toList());
    }

    public List<ProductionReport> setThirdWorkplaceFilter(Employee employee, List<ProductionReport> productionReports) throws NotFound {
        return productionReports.stream()
                .filter(productionReport -> productionReport.getThirdWorkplace().equals(employee))
                .collect(Collectors.toList());
    }

    public Double setAveragePerHour(List<ProductionReport> productionReports) throws OutRange {
        Integer items = 0;
        Double hour = 0.;
        for (ProductionReport productionReport : productionReports) {
            hour += productionReport.getProductionTimeToHour();
            items += productionReport.getTotalQuantityProduced();
        }
        if (items != 0 && hour != 0){
            return performanceCalculationsService.getPerformancePerHour(items, hour);
        }
        return 0.0;
    }

    public Double setPercentage(List<ProductionReport> productionReports) throws OutRange {
        Integer totalProduced = 0;
        Double maxPossibleItem = 0.;
        for (ProductionReport productionReport : productionReports) {
            totalProduced += productionReport.getTotalQuantityProduced();
            maxPossibleItem += productionReport.getMaxPossibleItems();
        }
        if (totalProduced != 0 && maxPossibleItem != 0.){
            return performanceCalculationsService.getPercentagePerformance(totalProduced, maxPossibleItem);
        }
        return 0.0;
    }

    public Integer setTotalProduced(List<ProductionReport> productionReports) throws NotFound {
        if (productionReports.size() != 0) {
            Integer sumProduced = 0;
            for (ProductionReport productionReport : productionReports) {
                sumProduced += productionReport.getTotalQuantityProduced();
            }
            return sumProduced;
        }
        return 0;
    }

    public Double setAverageSpeed(List<ProductionReport> productionReports) throws NotFound {
        if (productionReports.size() != 0) {
            Double sumSpeed = 0.;
            for (ProductionReport report : productionReports) {
                sumSpeed += report.getSpeedMachinePerCycle();
            }
            sumSpeed = sumSpeed / productionReports.size();
            sumSpeed *= 10;
            sumSpeed = (double)Math.round(sumSpeed) /10;
            return sumSpeed;
        }
        return 0.0;
    }
}
