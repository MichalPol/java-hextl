package dev.polproject.hextl.service.auditLog;

import dev.polproject.hextl.controller.dto.auditLog.AuditLogDto;
import dev.polproject.hextl.model.auditLog.AuditLog;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.repository.auditLog.AuditLogRepository;
import dev.polproject.hextl.service.app.AppConfigService;
import dev.polproject.hextl.service.app.DateAppFormatter;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.auditlog.AuditLogMapper;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class AuditLogService {

    private final AuditLogRepository auditLogRepository;
    private final UserService userService;
    private final AuditLogMapper auditLogMapper;
    private final AppConfigService appConfigService;


    public List<AuditLogDto> getAllLog(){
        return auditLogRepository
                .findAll()
                .stream()
                .map(auditLogMapper::toDto)
                .collect(Collectors.toList());
    }

    public void action(ControlCode controlCode, String actionMessage, int userId) throws NotFound {
        AuditLog auditLog = AuditLog.builder()
                .id(null)
                .controlCode(controlCode)
                .actionMessage(actionMessage)
                .user(userService.getUserModelById(userId))
                .createAt(OffsetDateTime.now())
                .build();

        log.info("Code: " + auditLog.getControlCode().getCode() +  " message: " + auditLog.getActionMessage() + " by the user login: " + auditLog.getUser().getLogin());
        auditLogRepository.save(auditLog);
    }

    public List<AuditLogDto> getLogBetweenTime(String startDate, String endDate, ControlCode code) throws DataInvalid {

        OffsetDateTime start = DateAppFormatter.parseStringToOffsetDateTimeFormat(startDate);
        OffsetDateTime end =DateAppFormatter.parseStringToOffsetDateTimeFormat(endDate);

        if (code.getCode() == ControlCode.ALL.getCode()){
            return auditLogRepository.findAllByCreateAtBetween(start, end).stream()
                    .map(auditLogMapper::toDto).collect(Collectors.toList());
        } else {
            return auditLogRepository.findAllByCreateAtBetweenAndControlCode(code,start,end)
                    .stream()
                    .map(auditLogMapper::toDto)
                    .collect(Collectors.toList());
        }
    }

    public List<AuditLogDto> getLogByCode(ControlCode controlCode,int page ){
        Pageable pageable = PageRequest.of(page,appConfigService.getPageSize());
        return auditLogRepository.findAllByControlCode(controlCode, pageable)
                .stream()
                .map(auditLogMapper::toDto)
                .collect(Collectors.toList());

    }
}
