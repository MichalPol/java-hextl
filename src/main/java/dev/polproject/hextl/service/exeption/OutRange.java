package dev.polproject.hextl.service.exeption;

public class OutRange extends Exception{
    public OutRange(String msg) {
        super(msg);
    }
}
