package dev.polproject.hextl.service.exeption;

public class Unauthorized extends Exception{
    public Unauthorized(String message) {
        super(message);
    }
}
