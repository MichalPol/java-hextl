package dev.polproject.hextl.service.exeption;

public class DataInvalid extends Exception {
    public DataInvalid(String message) {
        super(message);
    }
}
