package dev.polproject.hextl.service.exeption;


import java.time.LocalDate;

public class NotFound extends Exception {

    public NotFound(Integer id) {
        super("Could not find id " + id);
    }
    public NotFound(Long id) {
        super("Could not find id " + id);
    }
    public NotFound(LocalDate start, LocalDate end) {
        super("There is no plan between "+ start + " and "+ end);
    }
    public NotFound(String msg) {
        super(msg);
    }
}
