package dev.polproject.hextl.service.exeption;

public class IncorrectPassword extends Exception {

    public IncorrectPassword(String message){
        super(message);
    }
}
