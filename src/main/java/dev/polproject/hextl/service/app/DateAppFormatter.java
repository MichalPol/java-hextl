package dev.polproject.hextl.service.app;

import dev.polproject.hextl.service.exeption.DataInvalid;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class DateAppFormatter {

    public static OffsetDateTime parseStringToOffsetDateTimeFormat(String dateParse) throws DataInvalid {

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm");
            LocalDateTime localDateTime = LocalDateTime.parse(dateParse,formatter);
            return OffsetDateTime.of(localDateTime,OffsetDateTime.now().getOffset());

        }catch (Exception e){
            throw new DataInvalid("format date yyyy-MM-dd-HH:mm");
        }
    }
}
