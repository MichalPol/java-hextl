package dev.polproject.hextl.service.app;

import org.springframework.stereotype.Service;

@Service
public class PaginationService {

    public Boolean filterIsActiveValidation(Boolean filterIsActive){
        if (filterIsActive == null) {
            filterIsActive = true;
        }
        return filterIsActive;
    }
}
