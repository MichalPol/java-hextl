package dev.polproject.hextl.service.app;

import dev.polproject.hextl.controller.config.app.AppConfigProperties;
import dev.polproject.hextl.repository.app.AppRepository;
import dev.polproject.hextl.service.exeption.NotFound;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class AppConfigService {

    private final AppConfigProperties appConfigProperties;
    private final AppRepository appRepository;

    public String getAppName() {
        return appConfigProperties.getAppName();
    }

    public String getAddressServer() {
        return appConfigProperties.getAddressServer();
    }

    public Integer getNumberJobShifts() {
        return appConfigProperties.getNumberJobShifts();
    }

    public Integer getWorkPlanNumbersLine() {
        return appConfigProperties.getWorkPlanNumbersLine();
    }

    public Integer getWorkPlanNumberWorkplaces() {
        return appConfigProperties.getWorkPlanNumberWorkplaces();
    }

    public String getAppVersion() {
        return appConfigProperties.getAppVersion();
    }

    public Integer getRankingWeekStartDay(){
        return appConfigProperties.getRankingWeekStartDay();
    }

    public String getRankingWeekStartHour(){
       return appConfigProperties.getRankingWeekStartHour();
    }

    public Integer getRankingWeekEndDay(){
       return appConfigProperties.getRankingWeekEndDay();
    }

    public String getRankingWeekEndHour(){
        return appConfigProperties.getRankingWeekEndHour();
    }

    public String getRankingMonthStartHour(){
        return appConfigProperties.getRankingMonthStartHour();
    }

    public String getRankingMonthEndHour(){
        return appConfigProperties.getRankingMonthEndHour();
    }

    public String getAppAdminEmail(){
        return appConfigProperties.getAdminEmail();
    }

    public Integer getPageSize() {
        return appConfigProperties.getPageSize();
    }

}
