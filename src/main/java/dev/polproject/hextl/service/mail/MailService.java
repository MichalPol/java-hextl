package dev.polproject.hextl.service.mail;

import dev.polproject.hextl.controller.dto.mail.SendMessageContactEmail;
import dev.polproject.hextl.controller.dto.mail.SendMessageEmailToUser;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.service.app.AppConfigService;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.user.UserService;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailService {

    private final JavaMailSender javaMailSender;
    private final AppConfigService appConfigService;
    private final UserService userService;

    public MailService(JavaMailSender javaMailSender, AppConfigService appConfigService, @Lazy UserService userService) {
        this.javaMailSender = javaMailSender;
        this.appConfigService = appConfigService;
        this.userService = userService;
    }

    //sending contact message  to app admin
    public void sendMessageEmailContact(SendMessageContactEmail sendMessageContactEmail) throws MessagingException {
        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setTo(appConfigService.getAppAdminEmail());
        helper.setReplyTo(sendMessageContactEmail.getAddressEmail());
        helper.setFrom("Message from H E X T L ERP <from@email.com>");
        helper.setSubject(sendMessageContactEmail.getTitleMessage());
        helper.setText(sendMessageContactEmail.getNameSender() + " " + sendMessageContactEmail.getContent(), true);
        javaMailSender.send(mail);
    }

    //sending an e-mail request to reset the account password
    public void sendRequestPasswordChange(String email) throws MessagingException, NotFound {

        UserDto user = userService.getUserByEmail(email);

        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setTo(appConfigService.getAppAdminEmail());
        helper.setReplyTo(email);
        helper.setFrom("Password Change from " + appConfigService.getAppName() + " <from@email.com>");
        helper.setSubject("request to change the password");
        helper.setText(" Prośba o zmianę hasła dla adresu email: " + user.getEmail() + " " + "i loginie " + user.getLogin(), true);
        javaMailSender.send(mail);
    }

    //sending new password to account to the email address
    public void sendNewPassword(String email, String newPassword) throws NotFound, MessagingException {
        UserDto user = userService.getUserByEmail(email);

        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setTo(user.getEmail());
        helper.setFrom(appConfigService.getAppName() + " <from@email.com>");
        helper.setSubject("Reset password");
        helper.setText(messageNewPassword(newPassword), true);
        javaMailSender.send(mail);
    }


    //message text for the user sent via email
    private String messageNewPassword(String newPassword) {
        return " <div id=\":fo\" class=\"Ar Au\" style=\"display: block;\"><div id=\":fk\" class=\"Am Al editable LW-avf tS-tW\" hidefocus=\"true\" aria-label=\"Treść wiadomości\" g_editable=\"true\" role=\"textbox\" aria-multiline=\"true\" contenteditable=\"true\" tabindex=\"1\" style=\"direction: ltr; min-height: 285px;\" itacorner=\"6,7:1,1,0,0\" spellcheck=\"false\"><div><br></div><div><br></div>Zostało wygenerowane nowe hasło dla twojego konta w aplikacji:<div><br></div><div>hasło: " + newPassword + "</div></div></div>";
    }

    //sending contact message  to app admin
    public void sendMessageEmailToUser(SendMessageEmailToUser message) throws MessagingException, NotFound {
        UserDto sender = userService.getUserById(message.getIdSender(), true);
        UserDto recipient = userService.getUserById(message.getIdRecipient(), true);

        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setTo(recipient.getEmail());
        helper.setReplyTo(sender.getEmail());
        helper.setFrom("Message from H E X T L ERP <from@email.com>");
        helper.setSubject(message.getTitleMessage());
        helper.setText(message.getContent(), true);
        javaMailSender.send(mail);
    }

    //sending event - generate automatic  new employee ranking to the administrator
    public void sendMessageEmailGenerateAutomaticEmployeeRanking(String message) throws MessagingException {
        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setTo(appConfigService.getAppAdminEmail());
        helper.setFrom("Message from H E X T L ERP <from@email.com>");
        helper.setSubject("Report Generate Ranking");
        helper.setText(message, true);
        javaMailSender.send(mail);

    }
}
