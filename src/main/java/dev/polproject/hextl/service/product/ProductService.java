package dev.polproject.hextl.service.product;

import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.controller.dto.product.CreateUpdateProductDto;
import dev.polproject.hextl.controller.dto.product.ProductDto;
import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import dev.polproject.hextl.model.product.Product;
import dev.polproject.hextl.repository.product.ProductRepository;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.mapper.product.ProductMapper;

import dev.polproject.hextl.service.mapper.productivity.ProductionReportMapper;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportAdditionRemoval;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductService implements ProductionReportAdditionRemoval {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ProductionReportMapper productionReportMapper;

    public List<ProductDto> getAllProductDto(Boolean filterIsActive){

            return  productRepository.findAll()
                    .stream()
                    .filter(p -> p.getActive().equals(filterIsActive))
                    .map(productMapper::toProductDto)
                    .collect(Collectors.toList());
    }

    public ProductDto getProductDto(Long idProduct, Boolean filterIsActive) throws NotFound {
        return productRepository.findById(idProduct)
                .filter(p -> p.getActive().equals(filterIsActive))
                .map(productMapper::toProductDto)
                .orElseThrow(() -> new NotFound("Not found product id: " + idProduct));
    }

    public Product getProductModel(Long idProduct) throws NotFound {
        return productRepository
                .findById(idProduct)
                .orElseThrow(() -> new NotFound("Not found product id: " + idProduct));
    }

    @Transactional
    public ProductDto createProduct(CreateUpdateProductDto createProductDto) throws Conflict {
        Optional<Product> byInstructionIdProduct = productRepository.findByInstructionId(createProductDto.getInstructionId());
        if (byInstructionIdProduct.isPresent()) {
            log.warn("Conflict Instruction " + byInstructionIdProduct.get().getInstructionId());
            throw new Conflict("Product is exists instruction id: " + byInstructionIdProduct.get().getInstructionId());
        }

        Optional<Product> nameProductOptional = productRepository.findByName(createProductDto.getName());
        if (nameProductOptional.isPresent()){

            log.warn("Conflict Name product " + nameProductOptional.get().getName());
            throw new Conflict("Product is exists  name product: " + nameProductOptional.get().getName());
        }

            Product newProduct = Product.builder()
                    .id(null)
                    .name(createProductDto.getName())
                    .isSerialized(createProductDto.getIsSerialized())
                    .isActive(true)
                    .instructionId(createProductDto.getInstructionId())
                    .itemsPerCycle(createProductDto.getItemsPerCycle())
                    .description(createProductDto.getDescription())
                    .createAt(OffsetDateTime.now())
                    .updateAt(OffsetDateTime.now())
                    .build();
            Product save = productRepository.save(newProduct);
            log.info("Create new product name " + save.getName() + " id: " + save.getId());
            return productMapper.toProductDto(save);

    }

    @Transactional
    public ProductDto updateProduct(Long idProduct, CreateUpdateProductDto updateProductDto) throws NotFound, Conflict {

        Product updateProduct = getProductModel(idProduct);
        Optional<Product> productOptional = productRepository.findByInstructionId(updateProductDto.getInstructionId());

        if (productOptional.isPresent()){
            if (productOptional.get().getId() != updateProduct.getId()){
                throw new Conflict("Conflict - instruction Id exists");
            }
        }
        updateProduct.setName(updateProductDto.getName());
        updateProduct.setIsSerialized(updateProductDto.getIsSerialized());
        updateProduct.setItemsPerCycle(updateProductDto.getItemsPerCycle());
        updateProduct.setInstructionId(updateProductDto.getInstructionId());
        updateProduct.setDescription(updateProductDto.getDescription());
        updateProduct.setUpdateAt(OffsetDateTime.now());
        Product save = productRepository.save(updateProduct);
        log.info("Update product name: " + save.getName() + " id: " + save.getId());
        return productMapper.toProductDto(save);
    }

    @Transactional
    public ProductDto deleteProduct(Long idProduct) throws NotFound {
        Product productModel = getProductModel(idProduct);
        productModel.setActive(false);
        Product save = productRepository.save(productModel);
        log.info("remove product id: " + save.getId());
        return productMapper.toProductDto(save);
    }

    public List<ProductionReportDto> getAllProductionReportsFromProduct(Long idProduct) throws NotFound {
        Product product = getProductModel(idProduct);
        List<ProductionReport> productProductionReports = product.getProductProductionReports();
        return productProductionReports
                .stream()
                .map(productionReportMapper::toProductionReportDto)
                .collect(Collectors.toList());
    }

    @Override
    public void addProductionReport(Long id, ProductionReport productionReport) throws NotFound {

        Product product = getProductModel(id);
        product.getProductProductionReports().add(productionReport);
        Product save = productRepository.save(product);
        log.info("a production report has been added to the product with id: " + save.getId());
    }

    @Override
    public void removeProductionReport(Long id, ProductionReport productionReport) throws NotFound {
        Product product = getProductModel(id);
        product.getProductProductionReports().remove(productionReport);
        Product save = productRepository.save(product);
        log.info("the production ID report has been removed from the product " + save.getId());
    }
}
