package dev.polproject.hextl.service.schedulings;

import dev.polproject.hextl.controller.dto.employee.EmployeeDto;
import dev.polproject.hextl.controller.dto.line.LineDto;
import dev.polproject.hextl.controller.dto.product.ProductDto;
import dev.polproject.hextl.controller.dto.productionReport.CreateProductionReportDto;
import dev.polproject.hextl.controller.dto.user.UserDto;
import dev.polproject.hextl.model.ranking.RankingType;
import dev.polproject.hextl.model.user.User;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.Conflict;
import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.mapper.user.UserMapper;
import dev.polproject.hextl.service.product.ProductService;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportService;
import dev.polproject.hextl.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Configuration
@EnableScheduling
@Profile(value = "dev")
@RequiredArgsConstructor
public class ReportScheduleService {
    private final ProductionReportService productionReportService;
    private final ProductService productService;
    private final EmployeeService employeeService;
    private final LineService lineService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final String createReport = "0/2 0 0 ? * * ";

    @Scheduled(cron = createReport)
    @Transactional
    public void automaticCreatingReport() throws DataInvalid, Conflict, NotFound, OutRange {
        List<ProductDto> allProductDto = productService.getAllProductDto(true);
        List<EmployeeDto> allEmployeeDto = employeeService.getAllEmployeeDto(true);
        List<LineDto> allLine = lineService.getAllLine(true);
        List<UserDto> allUser = userService.getAllUser(true);
        User user = allUser.stream().findFirst().map(userMapper::toUserModel).get();

        LocalDateTime localDateTimeStart = LocalDateTime.now();
        LocalDateTime localDateTimeEnd = localDateTimeStart.plusHours(3);

        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm");
        String start = formatDate.format(localDateTimeStart);
        String end = formatDate.format(localDateTimeEnd);


        CreateProductionReportDto productionReport = CreateProductionReportDto.builder()
                .productId(allProductDto.stream().findFirst().get().getId())
                .firstWorkplaceIdEmployee(allEmployeeDto.stream().findAny().get().getId())
                .secondWorkplaceIdEmployee(allEmployeeDto.stream().findAny().get().getId())
                .thirdWorkplaceIdEmployee(allEmployeeDto.stream().findAny().get().getId())
                .lineId(allLine.stream().findAny().get().getId())
                .productionStart(start)
                .productionEnd(end)
                .productionHours(2)
                .productionMinutes(30)
                .series("test010120")
                .speedMachinePerMinute(30)
                .totalQuantityProduced(5000)
                .description("Raport generowany automatycznie na potrzeby testów")
                .build();

        productionReportService.createProductionReport(productionReport, user );
        log.info("create new report from schedule");

    }
}
