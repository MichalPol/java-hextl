-- example of role
INSERT INTO user_roles (id, description, name_role) VALUES (1, 'Administrator', 'ADMIN');
INSERT INTO user_roles (id, description, name_role) VALUES (2, 'Developer', 'DEV');
INSERT INTO user_roles (id, description, name_role) VALUES (3, 'Manager', 'MANAGER');
INSERT INTO user_roles (id, description, name_role) VALUES (4, 'Standard User', 'USER');
INSERT INTO user_roles (id, description, name_role) VALUES (5, 'Demo user', 'DEMO');

-- example of user

INSERT INTO users (id, create_at, email, is_active, login, name, password, path_avatar, surname, update_at, role_id) VALUES (1, '2020-04-29 19:03:38', 'admin@admin.com', true, 'admin', 'admin', '$2y$12$aPpA7lgufT2Ap2xH0G7hbOXxjRDduj6soeyOLlMH3kJbEf2ksJ/bG', '"Admin"', 'Admin', '2020-04-29 19:04:06', 1);
