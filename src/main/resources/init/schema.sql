create table employee_ranking
(
    id                   bigint       not null
        primary key,
    average_per_hour     double       not null,
    average_speed        double       not null,
    create_at            datetime     not null,
    percentage           double       not null,
    ranking_type         varchar(255) not null,
    ranking_type_counter int          not null,
    total_produced       int          not null,
    update_at            datetime     null,
    year                 int          not null,
    employee_id          bigint       not null,
    line_id              bigint       null
);

create table employees
(
    id          bigint       not null
        primary key,
    create_at   datetime     not null,
    information varchar(255) null,
    is_active   bit          not null,
    last_name   varchar(20)  not null,
    name        varchar(20)  not null,
    update_at   datetime     null
);

create table employees_employee_production_report
(
    employees_id                  bigint not null,
    employee_production_report_id bigint not null
);

create table hibernate_sequence
(
    next_val bigint null
);

create table logs
(
    id             bigint       not null
        primary key,
    action_message varchar(255) not null,
    control_code   varchar(255) not null,
    create_at      datetime     not null,
    user_id        int          null
);

create table production_lines
(
    id          bigint       not null
        primary key,
    is_active   bit          not null,
    name        varchar(255) not null,
    number_line int          not null
);

create table production_lines_line_production_reports
(
    production_lines_id        bigint not null,
    line_production_reports_id bigint not null
);

create table production_reports
(
    id                      bigint       not null
        primary key,
    create_at               datetime     not null,
    description             varchar(255) null,
    max_possible_items      int          not null,
    percentage_performance  double       not null,
    performance_per_hour    double       not null,
    production_end          datetime     not null,
    production_start        datetime     not null,
    production_time_to_hour double       not null,
    series                  varchar(10)  not null,
    speed_machine_per_cycle int          not null,
    total_quantity_produced int          not null,
    update_at               datetime     not null,
    created_by_user_id      int          not null,
    first_workplace_id      bigint       not null,
    line_id                 bigint       not null,
    product_id              bigint       not null,
    second_workplace_id     bigint       not null,
    third_workplace_id      bigint       not null,
    updated_by_user_id      int          not null
);

create table products
(
    id              bigint       not null
        primary key,
    create_at       datetime     not null,
    description     varchar(255) null,
    instruction_id  varchar(255) not null,
    is_active       bit          not null,
    is_serialized   bit          not null,
    items_per_cycle int          not null,
    name            varchar(255) not null,
    update_at       datetime     not null
);

create table products_product_production_reports
(
    products_id                   bigint not null,
    product_production_reports_id bigint not null
);

create table user_roles
(
    id          int          not null
        primary key,
    description varchar(255) null,
    name_role   varchar(255) null
);

create table users
(
    id          int          not null
        primary key,
    create_at   datetime     not null,
    email       varchar(255) null,
    is_active   bit          not null,
    login       varchar(20)  not null,
    name        varchar(20)  not null,
    password    varchar(255) not null,
    path_avatar varchar(255) not null,
    surname     varchar(20)  not null,
    update_at   datetime     null,
    role_id     int          not null
);

create table work_lines
(
    id          int not null
        primary key,
    line_number int not null
);

create table work_lines_workplaces
(
    work_lines_id int not null,
    workplaces_id int not null
);

create table work_places
(
    id             int          not null
        primary key,
    name_workplace varchar(255) not null
);

create table work_places_employee_list_workplaces
(
    work_places_id              int    not null,
    employee_list_workplaces_id bigint not null
);

create table work_plans
(
    id            int          not null
        primary key,
    create_at     datetime     not null,
    end_day       date         not null,
    info_absence  varchar(255) null,
    info_holidays varchar(255) null,
    start_day     date         not null,
    update_at     datetime     null,
    create_by_id  int          not null,
    update_by_id  int          null
);

create table work_plans_absence_employee
(
    work_plans_id       int    not null,
    absence_employee_id bigint not null
);

create table work_plans_holidays_employee
(
    work_plans_id        int    not null,
    holidays_employee_id bigint not null
);

create table work_plans_work_shifts
(
    work_plans_id  int not null,
    work_shifts_id int not null
);

create table work_shifts
(
    id           int          not null
        primary key,
    comments     varchar(255) null,
    shift_number int          not null
);

create table work_shifts_lines
(
    work_shifts_id int not null,
    lines_id       int not null
);

create table work_shifts_other
(
    work_shifts_id int    not null,
    other_id       bigint not null
);

create table work_shifts_shifts_leader
(
    work_shifts_id   int    not null,
    shifts_leader_id bigint not null
);

create table work_shifts_supervision
(
    work_shifts_id int    not null,
    supervision_id bigint not null
);

create table work_shifts_unskilled_worker
(
    work_shifts_id      int    not null,
    unskilled_worker_id bigint not null
);
