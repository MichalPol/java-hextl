package dev.polproject.hextl.controller.restApi.productivity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductionReportApiTest {

    @Test
    void getAllProductionReport() {
    }

    @Test
    void getProductionReportById() {
    }

    @Test
    void getProductionReportBetweenDateTime() {
    }

    @Test
    void createProductionReport() {
    }

    @Test
    void updateProductionReport() {
    }

    @Test
    void deleteProductionReport() {
    }

    @Test
    void getProductsFromProductionReport() {
    }
}