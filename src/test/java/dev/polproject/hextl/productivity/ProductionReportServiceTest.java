package dev.polproject.hextl.productivity;

import dev.polproject.hextl.controller.dto.productionReport.ProductionReportDto;
import dev.polproject.hextl.repository.productivity.ProductionReportRepository;
import dev.polproject.hextl.service.auditLog.AuditLogService;
import dev.polproject.hextl.service.employee.EmployeeService;
import dev.polproject.hextl.service.exeption.NotFound;
import dev.polproject.hextl.service.line.LineService;
import dev.polproject.hextl.service.mapper.product.ProductMapper;
import dev.polproject.hextl.service.mapper.productivity.ProductionReportMapper;
import dev.polproject.hextl.service.product.ProductService;
import dev.polproject.hextl.service.productivity.productionReport.PerformanceCalculationsService;
import dev.polproject.hextl.service.productivity.productionReport.ProductionReportService;
import dev.polproject.hextl.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class ProductionReportServiceTest {

    private ProductionReportService setMockProductionReportService() {
        ProductionReportRepository repository = Mockito.mock(ProductionReportRepository.class);
        ProductionReportMapper mapper = Mockito.mock(ProductionReportMapper.class);
        LineService lineService = Mockito.mock(LineService.class);
        ProductService productService = Mockito.mock(ProductService.class);
        EmployeeService employeeService = Mockito.mock(EmployeeService.class);
        PerformanceCalculationsService performanceCalculationsService = Mockito.mock(PerformanceCalculationsService.class);
        UserService userService = Mockito.mock(UserService.class);
        ProductMapper productMapper = Mockito.mock(ProductMapper.class);
        AuditLogService auditLogService = Mockito.mock(AuditLogService.class);

        return new ProductionReportService(repository, mapper, lineService, productService, productMapper, employeeService, performanceCalculationsService, userService, auditLogService);
    }

    @Test
    public void getAllProductReportDto_EmptyList() {
//        given
        ProductionReportService reportService = setMockProductionReportService();
//        when
        List<ProductionReportDto> allProductReportDto = reportService.getAllProductReportDto();
//        then
        Assertions.assertEquals(0, allProductReportDto.size());
    }

    @Test
    public void getProductionReportDto_InvalidId_throwsNotFound() {
        //given
        ProductionReportService reportService = setMockProductionReportService();
        //when
        //then
        Assertions.assertThrows(NotFound.class, () -> reportService.getProductionReportDto(1l));
    }
}
