package dev.polproject.hextl.productivity;


import dev.polproject.hextl.service.exeption.DataInvalid;
import dev.polproject.hextl.service.exeption.OutRange;
import dev.polproject.hextl.service.productivity.productionReport.PerformanceCalculationsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotNull;


public class PerformanceCalculationServiceTest {

    @Test
    public void getProductionTimeInHours_Hour_minus_throwsOutRange() {

        //given
        Integer hour = -1;
        Integer minutes = 59;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        //double productionTimeInHours = calculationsService.getProductionTimeInHours(hour, minutes);

        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getProductionTimeInHours(hour, minutes));
    }

    @Test
    public void getProductionTimeInHours_Minute_minus_throwsOutRange() {

        //given
        Integer hour = 1;
        Integer minutes = -1;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getProductionTimeInHours(hour, minutes));
    }

    @Test
    public void getProductionTimeInHours_minutes_out_range_minutes_throwsOutRange() {

        //given
        Integer hour = 1;
        Integer minutes = 61;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getProductionTimeInHours(hour, minutes));
    }

    @Test
    public void getProductionTimeInHours_allValueOK_resultDouble() throws OutRange {
        //given
        Integer hour = 5;
        Integer minutes = 20;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        double productionTimeInHours = calculationsService.getProductionTimeInHours(hour, minutes);
        //then
        Assertions.assertEquals(5.33, productionTimeInHours);
    }

    @Test
    public void getMaxPossibleItems__allValueOk_resultInt() throws OutRange {
        //given
        Integer itemsPerCycle = 6;
        Integer speedMachinePerCycle = 30;
        double productionTimeInHours = 2.5;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();

        //when
        int items = calculationsService.getMaxPossibleItems(itemsPerCycle, speedMachinePerCycle, productionTimeInHours);

        //then
        Assertions.assertEquals(27000, items);
    }

    @Test
    public void getMaxPossibleItems_outRange_cycleMachine_minOutRange_throwsOutRange() {
        //given
        Integer itemsPerCycle = 1;
        Integer speedMachinePerCycle = 30;
        double productionTimeInHours = 2.5;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getMaxPossibleItems(itemsPerCycle, speedMachinePerCycle, productionTimeInHours));
    }

    @Test
    public void getMaxPossibleItems_outRange_cycleMachine_maxOutRange_throwsOutRange() {
        //given
        Integer itemsPerCycle = 17;
        Integer speedMachinePerCycle = 30;
        double productionTimeInHours = 2.5;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getMaxPossibleItems(itemsPerCycle, speedMachinePerCycle, productionTimeInHours));
    }

    @Test
    public void getMaxPossibleItems_outRange_speedMachine_minOutRange_throwsOutRange() {
        //given
        Integer itemsPerCycle = 6;
        Integer speedMachinePerCycle = 4;
        double productionTimeInHours = 4;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getMaxPossibleItems(itemsPerCycle, speedMachinePerCycle, productionTimeInHours));
    }

    @Test
    public void getMaxPossibleItems_outRange_speedMachine_maxOutRange_throwsOutRange() {
        //given
        Integer itemsPerCycle = 6;
        Integer speedMachinePerCycle = 51;
        double productionTimeInHours = 4;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getMaxPossibleItems(itemsPerCycle, speedMachinePerCycle, productionTimeInHours));
    }

    @Test
    public void getPerformancePerHour_allValueOk_resultDouble() throws OutRange {

        //given
        Integer items1 = 10000;
        Integer items2 = 33333;
        Integer items3 = 12345;

        double time1 = 3;
        double time2 = 0.33;
        double time3 = 0.67;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();

        //when
        double result1 = calculationsService.getPerformancePerHour(items1, time1);
        double result2 = calculationsService.getPerformancePerHour(items1, time2);
        double result3 = calculationsService.getPerformancePerHour(items1, time3);

        double result4 = calculationsService.getPerformancePerHour(items2, time1);
        double result5 = calculationsService.getPerformancePerHour(items2, time2);
        double result6 = calculationsService.getPerformancePerHour(items2, time3);

        double result7 = calculationsService.getPerformancePerHour(items3, time1);
        double result8 = calculationsService.getPerformancePerHour(items3, time2);
        double result9 = calculationsService.getPerformancePerHour(items3, time3);
        //then
        Assertions.assertEquals(3333.33, result1);
        Assertions.assertEquals(30303.03, result2);
        Assertions.assertEquals(14925.37, result3);

        Assertions.assertEquals(11111, result4);
        Assertions.assertEquals(101009.09, result5);
        Assertions.assertEquals(49750.75, result6);

        Assertions.assertEquals(4115, result7);
        Assertions.assertEquals(37409.09, result8);
        Assertions.assertEquals(18425.37, result9);
    }

    @Test
    public void getPerformancePerHour_minItemOutRange_throwsOutRange() {

        //given
        Integer items = -1;
        double time = 2;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getPerformancePerHour(items,time));
    }

    @Test
    public void getPerformancePerHour_minHourOutRange_throwsOutRange() {

        //given
        Integer items = 1000;
        double time = 0;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        //then
        Assertions.assertThrows(OutRange.class, () -> calculationsService.getPerformancePerHour(items,time));
    }

    @Test
    public void getPercentagePerformance_allValueOk_resultDouble() throws OutRange {
//        given
        Integer totalItems = 20000;
        double maxPossibleItems = 30000;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when
        double percentagePerformance = calculationsService.getPercentagePerformance(totalItems, maxPossibleItems);
        //then
        Assertions.assertEquals(66.7,percentagePerformance);
    }

    @Test
    public void getPercentagePerformance_minItem_throwsOutRange(){
//        given
        Integer totalItems = -1;
        double maxPossibleItems = 30000;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class,() -> calculationsService.getPercentagePerformance(totalItems,maxPossibleItems));
    }

    @Test
    public void getPercentagePerformance_minMaxPossibleItems_throwsOutRange(){
//        given
        Integer totalItems = 10000;
        double maxPossibleItems = -1;
        PerformanceCalculationsService calculationsService = new PerformanceCalculationsService();
        //when

        //then
        Assertions.assertThrows(OutRange.class,() -> calculationsService.getPercentagePerformance(totalItems,maxPossibleItems));
    }



}
