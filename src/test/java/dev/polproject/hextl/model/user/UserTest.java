package dev.polproject.hextl.model.user;

import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private final OffsetDateTime createAt =  OffsetDateTime.of(2020,1,1,1,1,1,1, ZoneOffset.UTC);
    private final OffsetDateTime updateAt =  OffsetDateTime.of(2020,2,1,1,1,1,1, ZoneOffset.UTC);

    private User userTest = new User(1,
            "TestName",
            "TestSurname",
            "loginTest",
            "secretPassword",
            true,
            "test",
            "test.email@gmail.com",
            createAt,
            updateAt,
            new Role());

    @Test
    void getActive() {

    }

    @Test
    void setActive() {
    }

    @Test
    void getId() {
        //then
        assertEquals(1,userTest.getId());
    }

    @Test
    void getName() {
        //then
        assertEquals("TestName", userTest.getName());
    }

    @Test
    void getSurname() {
        //then
        assertEquals("TestSurname", userTest.getSurname());
    }

    @Test
    void getLogin() {
        //then
        assertEquals("loginTest", userTest.getLogin());
    }

    @Test
    void getPassword() {
    }

    @Test
    void getIsActive() {
    }

    @Test
    void getPathAvatar() {
    }

    @Test
    void getEmail() {
        //then
        assertEquals("test.email@gmail.com", userTest.getEmail());
    }

    @Test
    void getCreateAt() {
    }

    @Test
    void getUpdateAt() {
    }

    @Test
    void getRole() {
    }

    @Test
    void setId() {
    }

    @Test
    void setName() {
    }

    @Test
    void setSurname() {
    }

    @Test
    void setLogin() {
    }

    @Test
    void setPassword() {
    }

    @Test
    void setIsActive() {
    }

    @Test
    void setPathAvatar() {
    }

    @Test
    void setEmail() {
    }

    @Test
    void setCreateAt() {
    }

    @Test
    void setUpdateAt() {
    }

    @Test
    void setRole() {
    }
}
