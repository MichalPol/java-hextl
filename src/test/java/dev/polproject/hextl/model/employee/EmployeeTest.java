package dev.polproject.hextl.model.employee;

import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


class EmployeeTest {

    private final Employee testEmployee = Employee.builder()
            .id(null)
            .name("nameTest")
            .lastName("lastNameTest")
            .isActive(true)
            .information("test information")
            .createAt(OffsetDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))
            .updateAt(null)
            .employeeProductionReport(new ArrayList<>())
            .build();

    @BeforeEach
    void setUp() {

    }

    @Test
    void getActive() {
        //then
        assertTrue(testEmployee.getActive());
    }

    @Test
    void setActive() {
        //when
        testEmployee.setActive(false);
        //then
        assertFalse(testEmployee.getActive());
    }

    @Test
    void getId() {
        //then
        assertNull(testEmployee.getId());
    }

    @Test
    void setId() {
        //when
        testEmployee.setId(1L);
        //then
        assertEquals(1L, testEmployee.getId());
    }

    @Test
    void getName() {
        //then
        assertEquals("nameTest", testEmployee.getName());
    }

    @Test
    void setName() {
        //when
        testEmployee.setName("newName");
        //then
        assertEquals("newName", testEmployee.getName());
    }

    @Test
    void getLastName() {
        //then
        assertEquals("lastNameTest", testEmployee.getLastName());
    }

    @Test
    void setLastName() {
        //when
        testEmployee.setLastName("newLastName");
        //then
        assertEquals("newLastName", testEmployee.getLastName());
    }

    @Test
    void getInformation() {
        //then
        assertEquals("test information", testEmployee.getInformation());
    }

    @Test
    void setInformation() {
        //when
        testEmployee.setInformation("Testing");
        //then
        assertEquals("Testing", testEmployee.getInformation());
    }

    @Test
    void getCreateAt() {
        //when
        OffsetDateTime testingTime = OffsetDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC);

        //then
        assertEquals(testingTime, testEmployee.getCreateAt());
    }

    @Test
    void setCreateAt() {
        //when
        OffsetDateTime testingTime = OffsetDateTime.of(2020, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC);
        testEmployee.setCreateAt(testingTime);

        //then
        assertEquals(testingTime, testEmployee.getCreateAt());
    }

    @Test
    void getUpdateAt() {

        //then
        assertNull(testEmployee.getUpdateAt());

    }

    @Test
    void setUpdateAt() {
        //when
        OffsetDateTime testingTime = OffsetDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC);
        testEmployee.setUpdateAt(testingTime);
        //then
        assertEquals(testingTime, testEmployee.getUpdateAt());
    }

    @Test
    void getEmployeeProductionReport() {
        //then
        assertEquals(0,testEmployee.getEmployeeProductionReport().size());
    }

    @Test
    void setEmployeeProductionReport() {
        //when
        List<ProductionReport> reportList = new ArrayList<>();
        ProductionReport productionReport = new ProductionReport();
        reportList.add(productionReport);
        testEmployee.setEmployeeProductionReport(reportList);
        //then
        assertEquals(1, testEmployee.getEmployeeProductionReport().size());
    }
}
