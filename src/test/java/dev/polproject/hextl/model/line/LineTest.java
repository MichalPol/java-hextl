package dev.polproject.hextl.model.line;

import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class LineTest {

    private final Line testLine = new Line(null,"Test Line",1,true,new ArrayList<>());

    @Test
    void getActive() {
        //then
        assertTrue(testLine.getActive());
    }

    @Test
    void setActive() {
        //when
        testLine.setActive(false);
        //then
        assertFalse(testLine.getActive());
    }

    @Test
    void getId() {
        //then
        assertNull(testLine.getId());
    }

    @Test
    void getName() {
        //then
        assertEquals("Test Line", testLine.getName());
    }

    @Test
    void getNumberLine() {
        //then
        assertEquals(1, testLine.getNumberLine());
    }

    @Test
    void getIsActive() {
        //then
        assertTrue(testLine.getIsActive());
    }

    @Test
    void getLineProductionReports() {
        assertEquals(0, testLine.getLineProductionReports().size());
    }

    @Test
    void setId() {
        //when
        testLine.setId(2L);
        //then
        assertEquals(2L, testLine.getId());
    }

    @Test
    void setName() {
        //when
        testLine.setName("New Test Line 2");
        //then
        assertEquals("New Test Line 2", testLine.getName());
    }

    @Test
    void setNumberLine() {
        //when
        testLine.setNumberLine(5);
        //then
        assertEquals(5, testLine.getNumberLine());
    }

    @Test
    void setIsActive() {
        //when
        testLine.setIsActive(false);
        //then
        assertFalse(testLine.getIsActive());
    }

    @Test
    void setLineProductionReports() {
        //given
        ProductionReport productionReport1 = new ProductionReport();
        ProductionReport productionReport2 = new ProductionReport();
        ArrayList<ProductionReport> list = new ArrayList<>();
        //when
        list.add(productionReport1);
        list.add(productionReport2);
        testLine.setLineProductionReports(list);
        //then
        assertEquals(2, testLine.getLineProductionReports().size());
    }
}
