package dev.polproject.hextl.model;

import dev.polproject.hextl.model.auditLog.AuditLog;
import dev.polproject.hextl.model.auditLog.ControlCode;
import dev.polproject.hextl.model.user.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class AuditLogTest {

    private AuditLog auditLog;

    @Before
    public void  setAuditLog(){
        auditLog = new AuditLog(1l, ControlCode.ALL, "test", new User(), OffsetDateTime.of(2020,1,1,1,1,1,1, ZoneOffset.UTC));
    }

    @Test
    public void auditLogArgumentsAllIsGood(){
//        given
        AuditLog auditLog = this.auditLog;
//        when
//        then
        Assert.assertEquals(1L, (long) auditLog.getId());
        Assert.assertEquals(1, auditLog.getControlCode().getCode());
        Assert.assertEquals("test", auditLog.getActionMessage());
        Assert.assertEquals(new User(), auditLog.getUser());
    }

}
