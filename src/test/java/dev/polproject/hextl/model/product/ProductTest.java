package dev.polproject.hextl.model.product;

import dev.polproject.hextl.model.productivity.productionReport.ProductionReport;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private final OffsetDateTime createProduct = OffsetDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC);
    private final OffsetDateTime updateProduct = OffsetDateTime.of(2020, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC);

    private final Product testProduct = new Product(
            null,
            "Product A",
            true,
            6,
            "testIDInstructions",
            "description",
            true,
            createProduct,
            updateProduct,
            new ArrayList<>());


    @Test
    void getActive() {
        //then
        assertTrue(testProduct.getActive());
    }

    @Test
    void setActive() {
        //when
        testProduct.setActive(false);
        //then
        assertFalse(testProduct.getActive());
    }

    @Test
    void getId() {
        //then
        assertNull(testProduct.getId());
    }

    @Test
    void getName() {
        //then
        assertEquals("Product A", testProduct.getName());
    }

    @Test
    void getIsSerialized() {
        //then
        assertTrue(testProduct.getIsSerialized());
    }

    @Test
    void getItemsPerCycle() {
        //then
        assertEquals(6, testProduct.getItemsPerCycle());
    }


    @Test
    void getInstructionId() {
        //then
        assertEquals("testIDInstructions", testProduct.getInstructionId());
    }

    @Test
    void getDescription() {
        //then
        assertEquals("description", testProduct.getDescription());
    }

    @Test
    void getIsActive() {
        //then
        assertTrue(testProduct.getIsActive());
    }

    @Test
    void getCreateAt() {
        //when
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC);
        //then
        assertEquals(offsetDateTime, testProduct.getCreateAt());
    }

    @Test
    void getUpdateAt() {
        //when
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2020, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC);
        //then
        assertEquals(offsetDateTime, testProduct.getUpdateAt());
    }

    @Test
    void getProductProductionReports() {
        //then
        assertEquals(0, testProduct.getProductProductionReports().size());
    }

    @Test
    void setId() {
        //when
        testProduct.setId(2L);
        //then
        assertEquals(2l, testProduct.getId());
    }

    @Test
    void setName() {
        //when
        testProduct.setName("test2");
        //then
        assertEquals("test2", testProduct.getName());
    }

    @Test
    void setIsSerialized() {
        //when
        testProduct.setIsSerialized(false);
        //then
        assertFalse(testProduct.getIsSerialized());
    }

    @Test
    void setItemsPerCycle() {
        //when
        testProduct.setItemsPerCycle(9);
        //then
        assertEquals(9, testProduct.getItemsPerCycle());
    }

    @Test
    void setInstructionId() {
        //when
        testProduct.setInstructionId("new Test 1/2/20");
        //then
        assertEquals("new Test 1/2/20", testProduct.getInstructionId());
    }

    @Test
    void setDescription() {
        //when
        testProduct.setDescription("test d");
        //then
        assertEquals("test d", testProduct.getDescription());
    }

    @Test
    void setIsActive() {
        //when
        testProduct.setIsActive(false);
        //then
        assertFalse(testProduct.getIsActive());
    }

    @Test
    void setCreateAt() {
        //given
        OffsetDateTime createOft = OffsetDateTime.of(2010, 10, 10, 10, 1, 1, 1, ZoneOffset.UTC);
        //when
        testProduct.setCreateAt(createOft);
        //then
        assertEquals(createOft, testProduct.getCreateAt());
    }

    @Test
    void setUpdateAt() {
        //given
        OffsetDateTime updateOft = OffsetDateTime.of(2012, 11, 10, 10, 1, 1, 1, ZoneOffset.UTC);
        //when
        testProduct.setUpdateAt(updateOft);
        //then
        assertEquals(updateOft, testProduct.getUpdateAt());
    }

    @Test
    void setProductProductionReports() {
        //given
        ProductionReport productionReport1 = new ProductionReport();
        ProductionReport productionReport2 = new ProductionReport();
        ArrayList<ProductionReport> list = new ArrayList<>();
        //when
        list.add(productionReport1);
        list.add(productionReport2);
        testProduct.setProductProductionReports(list);
        //then
        assertEquals(2, testProduct.getProductProductionReports().size());
    }
}
