package dev.polproject.hextl.service.userTest;
import dev.polproject.hextl.controller.dto.user.RoleDto;
import dev.polproject.hextl.repository.user.RoleRepository;
import dev.polproject.hextl.service.mapper.user.RoleMapper;
import dev.polproject.hextl.service.user.RoleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {

    @Mock
    private RoleRepository roleRepository;
    @Mock
    private RoleMapper roleMapper;

    private RoleService roleService;


    @Before
    public void init(){

        roleService = new RoleService(roleRepository, roleMapper);
    }

    @Test
    public void get_List_role() throws Exception{

        //given
        when(roleRepository.findAll()).thenReturn(new ArrayList<>());

        //when
        List<RoleDto> allRole = roleService.getAllRole();

        //then
        assertThat(allRole.size()).isEqualTo(0);
    }




}
