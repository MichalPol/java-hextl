# System ERP do wsparcia zarządzania pracownikami i oceną wydajności napisane w Spring Fremwork oraz w React 
>wersja Beta
>
> System ERP bazuje na standardowej autoryzacji za pomocą plików sesji.

## Spis treści

- [Ogólny opis](#ogólny-opis)
- [Technologie](#technologie)
- [Jak uruchomić](#jak-uruchomic)
- [W przygotowaniu](#w-przygotowaniu)
- [Status](#status)
- [Inspiracja](#inspiracja)
- [Kontakt](#contact)
- [Prawa Autorskie](#prawa)

## Ogólny opis

Aplikacja napisana w Javie i w React, wspierajca zarzdzanie pracownikami w obszarze produkcyjnym na wybranym dziale produkcyjnym. 
Zarzdza grafikiem pracowników na poszczególnych zmianach, liniach produkcyjnych oraz stanowiskach w przedziale tygodniowym.
Realizuje też zadanie przechowywania raportów z produkcji oraz generowania na ich podstawie statysyk: 
- linii produkcyjnych
- pracowników
- produktów

Jest możliwość wtgenerowania statystyk czasie rzeczywistym. Aplikacja również posiada autoamtyczne genreowanie rankingu pracowników z podziałem na:
 - tydzień
 - miesiac
 - kwartał
 - pół roku
 - rok


## Technologie
Frontend:
- React
- Material UI
- MUI-database
- ---redux---
- typescript
- axios
- react-dnd
- react-router-dom
- react-circular-progressbar
- react-move
- d3-ease

Backend:
- java 1.8
- hibernate 
- MySql 
- spring-boot-starter-security
- spring-boot-starter-web
- spring-boot-starter-mai
- springfox-swagger2
- spring-boot-starter-test
- spring-boot-starter-data-jpa
- spring-security-test
- spring-boot-starter-validation
- lombok
- junit
- REST API


## Jak uruchomic

- `postępować zgodnie z install.md`
- `skopiować repozytorium`
- `uzupełnić danymi plik properties`
- `mvn spring-boot:run` to run application
- `mvn test` to run all tests


## W przyszłości

- generator statysyk na wybrane parametry 
- dodanie możliwości zgłaszania awarii
- dodanie czatu miedzy uzytkownikami aplikacji
- dodanie dynamicznie działów produkcyjnych


## Status

Demo aplikacji pod adresem:

Wersja Beta:
[Aplikacja ERP HEXTL](https://hextl.magfi.pl)

## Inspiracja

Potrzeba rozwizania problemów rzetelnej oceny pracowników w obszarze produkcyjnym.


## Kontakt

Created by Java [@MichalPol]  kontakt : michal.p.pol@gmail.com. www 

Created by React [@KrzysztofZiemski]  kontakt : k.b.ziemski@gmail.com. [www](https://krzysztofziemski.com/)

## Prawa autorskie
`Wszelkie prawa zastrzeżone - zrealizowane koncepcje są całkowitą własnościa autorów bez pozwolenia nie zezwala sie na używanie komercyjnie projektu`

v 0.4

