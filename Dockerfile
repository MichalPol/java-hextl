FROM openjdk:8u191-jdk-alpine3.9
ADD target/hextl-0.0.1-SNAPSHOT.jar .
EXPOSE 8443
CMD java -jar -Dspring.profiles.active=dev hextl-0.0.1-SNAPSHOT.jar --envname=prod
